import json
import os
from LitePagesExecutioner import LitePagesExecutioner
from VirtualHost import VirtualHost

class LPFileManager:

    def __init__(self, data):
        self.data = data
        pass

    def returnPathEnclosed(self, path):
        return "'" + path + "'"

    def listForTable(self):
        try:
            lpe = LitePagesExecutioner('')

            lpe.command = "sudo -u %s ls -la --group-directories-first %s" % (self.data['VHUser'], self.returnPathEnclosed(self.data['path']))
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                output = stdout.splitlines()

                json_data = "["
                counter = 1
                checker = 0

                for items in output:
                    try:

                        currentFile = items.split(' ')
                        currentFile = [a for a in currentFile if a != '']
                        if currentFile[-1] == '.' or currentFile[-1] == '..' or currentFile[0] == 'total':
                            continue

                        if len(currentFile) > 9:
                            fileName = currentFile[8:]
                            currentFile[-1] = " ".join(fileName)

                        dirCheck = 0
                        if currentFile[0][0] == 'd':
                            dirCheck = 1

                        size = str(int(int(currentFile[4]) / float(1024)))
                        lastModified = currentFile[5] + ' ' + currentFile[6] + ' ' + currentFile[7]

                        dic = {'name': currentFile[-1], 'FullPath': '%s/%s' % (self.data['path'], currentFile[-1]),
                               'size': size, 'lastModified': lastModified, 'dirCheck': dirCheck, 'permissions': currentFile[0]}

                        if checker == 0:
                            json_data = json_data + json.dumps(dic)
                            checker = 1
                        else:
                            json_data = json_data + ',' + json.dumps(dic)
                        counter = counter + 1
                    except BaseException as msg:
                        pass

                json_data = json_data + ']'

                return 1, json_data

            return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def CompressNow(self):
        try:

            self.cwd = os.getcwd()

            os.chdir(self.data['basePath'])

            lpe = LitePagesExecutioner('')

            if self.data['compressionType'] == 'zip':
                compressedFileName = self.returnPathEnclosed(self.data['basePath'] + '/' + self.data['compressedFileName'] + '.zip')
                command = 'zip -r ' + compressedFileName + ' '
            else:
                compressedFileName = self.returnPathEnclosed(self.data['basePath'] + '/' + self.data['compressedFileName'] + '.tar.gz')
                command = 'tar -czvf ' + compressedFileName + ' '

            for item in self.data['listOfFiles']:
                command = '%s%s ' % (command, self.returnPathEnclosed(item))

            lpe.command = 'sudo -u %s %s' % (self.data['VHUser'], command)
            ReturnCode, stdout, stderr = lpe.Execute()

            os.chdir(self.cwd)

            if ReturnCode:
                return 1, None
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def ExtractNow(self):
        try:

            lpe = LitePagesExecutioner('')
            if self.data['fileToExtract'].endswith('.zip'):
                command = 'unzip -o ' + self.returnPathEnclosed(self.data['fileToExtract']) + ' -d ' + self.returnPathEnclosed(self.data['extractionLocation'])
            else:
                command = 'tar -xf ' + self.returnPathEnclosed(self.data['fileToExtract']) + ' -C ' + self.returnPathEnclosed(self.data['extractionLocation'])

            lpe.command = 'sudo -u %s %s' % (self.data['VHUser'], command)

            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, None
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def deleteFolderOrFile(self):
        try:
            VirtualHost.FixPermissions(self.data['basePath'], self.data['VHUser'])
            lpe = LitePagesExecutioner('')

            for item in self.data['fileAndFolders']:
                lpe.command = 'sudo -u %s rm -rf %s' % (self.data['VHUser'], self.returnPathEnclosed(self.data['basePath'] + '/' + item))
                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode:
                    pass
                else:
                    return 0, stderr

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def createNewFolder(self):
        try:
            VirtualHost.FixPermissions(self.data['basePath'], self.data['VHUser'])
            lpe = LitePagesExecutioner('')

            FinalPath = '%s/%s' % (self.returnPathEnclosed(self.data['basePath']), self.data['name'])

            lpe.command = "sudo -u %s mkdir %s" % (self.data['VHUser'], FinalPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, None
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def createNewFile(self):
        try:
            #VirtualHost.FixPermissions(self.data['basePath'], self.data['VHUser'])
            lpe = LitePagesExecutioner('')

            FinalPath = '%s/%s' % (self.returnPathEnclosed(self.data['basePath']), self.data['name'])

            lpe.command = "sudo -u %s touch %s" % (self.data['VHUser'], FinalPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, None
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def readFileContents(self):
        try:
            lpe = LitePagesExecutioner('')

            lpe.command = "sudo -u %s cat %s" % (self.data['VHUser'], self.returnPathEnclosed(self.data['file']))
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, stdout
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def putFileContents(self):
        try:
            writeToFile = open(self.data['file'], 'w')
            writeToFile.write(self.data['fileContent'])
            writeToFile.close()
            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def fixPermissions(self):

        path = LitePagesExecutioner.remove_suffix(self.data['path'], '/')
        VHUser = self.data['VHUser']

        lpe = LitePagesExecutioner('')

        groupName = 'nogroup'

        lpe.command = 'chown -P %s:%s %s' % (VHUser, VHUser, path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        lpe.shell = True
        lpe.command = 'chown -R -P %s:%s %s/public_html/*' % (VHUser, VHUser, path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        lpe.command = 'chown -R -P %s:%s %s/public_html/.[^.]*' % (VHUser, VHUser, path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            pass


        lpe.command = "find %s/public_html -type d -exec chmod 0755 {} \;" % (path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        lpe.command = "find %s/public_html -type f -exec chmod 0644 {} \;" % (path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        lpe.command = 'chown -P %s:%s %s/public_html' % (VHUser, groupName, path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        lpe.command = 'chmod 750 %s/public_html' % (path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        return 1, 'Permissions fixed.'

