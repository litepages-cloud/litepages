import json
import socket
import os
import threading as multi
import argparse

from cryptography.fernet import Fernet

from Logger import LitePagesLogger
limitThreads = multi.BoundedSemaphore(10)

class HandleRequest(multi.Thread):

    def __init__(self, conn):
        multi.Thread.__init__(self)
        self.connection = conn
        self.config = json.loads(open(LitePagesLogger.LitePagesConfigPathFile, 'r').read())
        self.fernet = Fernet(self.config['key'].encode())

    def __del__(self):
        try:
            self.connection.close()
        except BaseException as msg:
            LitePagesLogger.writeToFile(str(msg), LitePagesLogger.ERROR, 1)

    def run(self):
        limitThreads.acquire()
        self.dataComplete = "".encode()

        try:
            while True:
                #Data = self.connection.recv(2048).decode("utf-8")
                Data = self.connection.recv(80000)
                if Data:
                    if len(Data) != 0:
                        self.dataComplete = self.dataComplete + Data
                        if os.path.exists('/var/log/LPDebug'):
                            LitePagesLogger.writeforCLI(self.dataComplete, LitePagesLogger.INFO, 'HandleRequest.run')
                    else:
                        self.dataComplete = self.fernet.decrypt(self.dataComplete).decode("utf-8")
                        self.RunJob()
                        break
                else:
                    self.dataComplete = self.fernet.decrypt(self.dataComplete).decode("utf-8")
                    self.RunJob()
                    break

        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'HandleRequest.run')
        finally:
            limitThreads.release()

    def RunJob(self):
        try:
            data = json.loads(self.dataComplete)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeforCLI('Received data via socket: %s' % str(data), LitePagesLogger.DEBUG, 'HandleRequest.RunJob')
                LitePagesLogger.writeforCLI('Received in local config: %s' % str(self.config), LitePagesLogger.DEBUG,'HandleRequest.RunJob')
                LitePagesLogger.writeforCLI('Function to execute %s' % (data['function']), LitePagesLogger.DEBUG, 'HandleRequest.RunJob')

            if data['token'] == self.config['token']:

                if data['function'] == 'createVirtualHost':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    vh.CreateVirtualHost()

                elif data['function'] == 'FetchServerDetails':
                    from LPServices import LPServices
                    status, message = LPServices.FetchServerDetails(self.config)
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteVirtualHost':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.DeleteVirtualHost()
                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'CreateDatabase':
                    from MySQLUtils import MySQLUtils
                    status, message = MySQLUtils.CreateDatabaseFull(data['database'], data['user'], data['PasswordByPass'], data['host'])

                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteDatabase':
                    from MySQLUtils import MySQLUtils
                    status, message = MySQLUtils.DeleteDatabaseFull(data)

                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'CreateDatabaseUser':
                    from MySQLUtils import MySQLUtils
                    status, message = MySQLUtils.CreateDatabaseUser(data['database'], data['user'], data['PasswordByPass'], data['host'])

                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteDatabaseUser':
                    from MySQLUtils import MySQLUtils
                    status, message = MySQLUtils.DeleteUser(data['user'])

                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeployWordpressNow':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    vh = WPManager(data)
                    vh.DeployWordPress()
                elif data['function'] == 'DeleteWordPress':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.DeleteWordPress()
                elif data['function'] == 'ChangeWebsitePHP':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.ChangeWebsitePHP()

                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchPublicKey':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.FetchPublicKey()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'AttachGitRepo':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from GitManager import GitManager
                    gm = GitManager(data)
                    gm.AttachGitRepo()
                elif data['function'] == 'ProcessWebHook':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from GitManager import GitManager
                    gm = GitManager(data)
                    gm.ProcessWebHook()

                elif data['function'] == 'FetchWordPressDetails':

                    from WPManager import WPManager
                    wm = WPManager(data)
                    status, message = wm.FetchWordPressDetails()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchPHPDetails':

                    from VirtualHost import VirtualHost
                    wm = VirtualHost(data)
                    status, message = wm.FetchPHPDetails()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'savePHPConfigBasic':

                    from VirtualHost import VirtualHost
                    wm = VirtualHost(data)
                    status, message = wm.savePHPConfigBasic()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchPHPDetailsAdvanced':

                    from VirtualHost import VirtualHost
                    wm = VirtualHost(data)
                    status, message = wm.FetchPHPDetailsAdvanced()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'savePHPConfigAdvanced':

                    from VirtualHost import VirtualHost
                    wm = VirtualHost(data)
                    status, message = wm.savePHPConfigAdvanced()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'UpdateWPSettings':

                    from WPManager import WPManager
                    wm = WPManager(data)
                    status, message = wm.UpdateWPSettings()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'AutoLogin':

                    from WPManager import WPManager
                    wm = WPManager(data)
                    status, message = wm.AutoLogin()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'GetCurrentPlugins':

                    from WPManager import WPManager
                    wm = WPManager(data)
                    status, message = wm.GetCurrentPlugins()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'UpdatePlugins':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.UpdatePlugins()

                elif data['function'] == 'DeletePlugins':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.DeletePlugins()

                elif data['function'] == 'ChangeStatus':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.ChangeStatus()

                elif data['function'] == 'GetCurrentThemes':
                    from WPManager import WPManager
                    wm = WPManager(data)
                    status, message = wm.GetCurrentThemes()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'UpdateThemes':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.UpdateThemes()

                elif data['function'] == 'DeleteThemes':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.DeleteThemes()
                elif data['function'] == 'ChangeStatusThemes':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    wm = WPManager(data)
                    wm.ChangeStatusThemes()

                elif data['function'] == 'CreateStagingFinal':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from WPManager import WPManager
                    vh = WPManager(data)
                    vh.CreateStagingFinal()

                elif data['function'] == 'DeployToProduction':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###

                    from WPManager import WPManager
                    vh = WPManager(data)
                    vh.DeployToProduction()

                elif data['function'] == 'listForTable':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.listForTable()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'CompressNow':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.CompressNow()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ExtractNow':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.ExtractNow()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'deleteFolderOrFile':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.deleteFolderOrFile()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'createNewFolder':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.createNewFolder()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'createNewFile':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.createNewFile()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'readFileContents':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.readFileContents()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'putFileContents':

                    from LPFileManager import LPFileManager
                    lpfm = LPFileManager(data)
                    status, message = lpfm.putFileContents()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchServicesStatus':

                    from LPServices import LPServices
                    lps = LPServices(data)
                    status, message = lps.FetchServicesStatus()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)

                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ChangeUserPasswordSSH':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.ChangeUserPasswordSSH()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchUserSSHKeys':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.FetchUserSSHKeys()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FixPermissions':

                    from LPFileManager import LPFileManager

                    lpfm = LPFileManager(data)
                    status, message = lpfm.fixPermissions()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'AddUserPublicKey':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.AddUserPublicKey()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteUserPublicKey':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.DeleteUserPublicKey()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'showStatus':

                    from MySQLUtils import MySQLUtils
                    msu = MySQLUtils()
                    status, message = msu.showStatus()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchMySQLConfigs':

                    from MySQLUtils import MySQLUtils
                    msu = MySQLUtils()
                    status, message = msu.FetchMySQLConfigs()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ApplyChanges':

                    from MySQLUtils import MySQLUtils
                    msu = MySQLUtils(data)
                    status, message = msu.ApplyChanges(self.config['MySQLRootPass'])

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'getDataFromLogFile':

                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.getDataFromLogFile()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'BubbleWrapStatus':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.BubbleWrapStatus()
                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'reCAPTCHASettings':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.reCAPTCHASettings()
                    if status:
                        RetData = {'status': 1, 'message': 'None'}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchCurrentLSConfig':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.FetchCurrentLSConfig()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())

                    self.connection.close()
                elif data['function'] == 'SaveLSConfig':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.SaveLSConfig()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchCurrentRewriteRules':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.FetchCurrentRewriteRules()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'SaveRewriteRules':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.SaveRewriteRules()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchSSLDetails':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.FetchSSLDetails()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchUserSSHKeysGlobal':
                    from UFWFirewall import UFWFirewall
                    vh = UFWFirewall(data)
                    status, message = vh.FetchUserSSHKeysGlobal()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteUserPublicKeyGlobal':
                    from UFWFirewall import UFWFirewall
                    vh = UFWFirewall(data)
                    status, message = vh.DeleteUserPublicKeyGlobal()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'AddUserPublicKeyGlobal':
                    from UFWFirewall import UFWFirewall
                    vh = UFWFirewall(data)
                    status, message = vh.AddUserPublicKeyGlobal()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'SaveCustomSSL':
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    status, message = vh.SaveCustomSSL()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                if data['function'] == 'IssueFreeSSL':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from VirtualHost import VirtualHost
                    vh = VirtualHost(data)
                    vh.IssueFreeSSL()
                elif data['function'] == 'FetchRulesJson':
                    from UFWFirewall import UFWFirewall
                    status, message = UFWFirewall.FetchRulesJson()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'getSSHConfigs':
                    from UFWFirewall import UFWFirewall
                    status, message = UFWFirewall.getSSHConfigs()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'saveSSHConfigs':
                    from UFWFirewall import UFWFirewall
                    ufw = UFWFirewall(data)
                    status, message = ufw.saveSSHConfigs()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FinalDeleteFirewallRule':
                    from UFWFirewall import UFWFirewall
                    status, message = UFWFirewall.FinalDeleteFirewallRule(data)
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'AddNewRule':
                    from UFWFirewall import UFWFirewall
                    status, message = UFWFirewall.AddFirewallRule(data['protocol'], data['IPAddress'], data['port'])
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FirewallStatus':
                    from UFWFirewall import UFWFirewall
                    status, message = UFWFirewall.FirewallStatus(data)
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchLiteSpeedDetails':

                    from LPServices import LPServices
                    wm = LPServices(data)
                    status, message = wm.FetchLiteSpeedDetails()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                if data['function'] == 'InstallModSecurity':
                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###
                    from UFWFirewall import UFWFirewall
                    vh = UFWFirewall(data)
                    vh.InstallModSecurity()
                elif data['function'] == 'FetchModSecConfigs':

                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.FetchModSecConfigs()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'saveModSecConfigs':

                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.saveModSecConfigs()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'installOWASP':

                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.installOWASP()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'disableOWASP':

                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.disableOWASP()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchModSecRules':
                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.FetchModSecRules()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'disableRuleFile':
                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.disableRuleFile()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'enableRuleFile':
                    from UFWFirewall import UFWFirewall
                    wm = UFWFirewall(data)
                    status, message = wm.enableRuleFile()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'UpgradeLP':

                    from LPVersionManager import LPVersion
                    lpfm = LPVersion(data)
                    status, message = lpfm.UpgradeLP()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'addNewCron':
                    from CronManager import CronManager
                    vh = CronManager(data)
                    status, message = vh.addNewCron()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchCronJobs':
                    from CronManager import CronManager
                    vh = CronManager(data)
                    status, message = vh.FetchCronJobs()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteCronFinal':
                    from CronManager import CronManager
                    vh = CronManager(data)
                    status, message = vh.DeleteCronFinal()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'CreateLocalBackup':

                    ### Close the connection before doing the long job
                    self.connection.close()
                    ###

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    lpfm.CreateLocalBackup()

                elif data['function'] == 'Getdatabasename':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.Getdatabasename()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'CheckServiceStaus':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.CheckServiceStaus()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchRedisdata':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.FetchRedisdata()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'InstallService':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.InstallService()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'RestartCurrentServer':
                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.RestartCurrentServer()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ClearRedisCache':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.ClearRedisCache()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeployPhpmyadmin':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.DeployPhpmyadmin()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'RestoreLocalBackup':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.RestoreLocalBackup()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ExRestoreLocalBackup':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.ExRestoreLocalBackup()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'CopyFile':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.CopyFile()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    self.connection.close()
                elif data['function'] == 'RestoreCopy':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.RestoreCopy()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    self.connection.close()
                elif data['function'] == 'WPSiteMove':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.WPSiteMove()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'NRestoreLocalBackup':

                    from LPBackup import LPBackups
                    lpfm = LPBackups(data)
                    status, message = lpfm.NRestoreLocalBackup()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'FetchLiteSpeedSettingsSL':

                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.FetchLiteSpeedSettingsSL()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'RestartLSWS':

                    from LPBackup import LPBackups
                    lswse = LPBackups(data)
                    status, message = lswse.RestartLSWS()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'SaveLSSettingsSL':

                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.SaveLSSettingsSL()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ShowRealVisitorIP':

                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.ShowRealVisitorIP()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'IssueSSLv2':
                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.IssueSSLv2()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'switchServer':
                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.switchServer()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'GetApacheStatus':
                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.GetApacheStatus()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'saveApacheConfig':
                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.saveApacheConfig()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'tuneApacheSettings':
                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.tuneApacheSettings()
                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'CloudflareForSSL':

                    from VirtualHost import VirtualHost
                    lpfm = VirtualHost(data)
                    status, message = lpfm.CloudflareForSSL()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'getExtensionsInformation':
                    from phpManager import PHPManager
                    lpfm = PHPManager(data)
                    status, message = lpfm.getExtensionsInformation()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'installPHPExtension':
                    from phpManager import PHPManager
                    lpfm = PHPManager(data)
                    status, message = lpfm.installPHPExtension()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'unInstallPHPExtension':
                    from phpManager import PHPManager
                    lpfm = PHPManager(data)
                    status, message = lpfm.unInstallPHPExtension()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'findPHPVersions':
                    from phpManager import PHPManager
                    lpfm = PHPManager(data)
                    status, message = lpfm.findPHPVersions()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()

                elif data['function'] == 'DeployWPContainer':
                    from DockerSites import Docker_Sites
                    lpfm = Docker_Sites('DeployWPContainer', data)
                    lpfm.start()

                    RetData = {'status': 1, 'message': ''}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeleteDockerApp':
                    from DockerSites import Docker_Sites
                    lpfm = Docker_Sites('', data)
                    status, message = lpfm.DeleteDockerApp()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ListContainers':
                    from DockerSites import Docker_Sites
                    lpfm = Docker_Sites('', data)
                    status, message = lpfm.ListContainers()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ContainerLogs':
                    from DockerSites import Docker_Sites
                    lpfm = Docker_Sites('', data)
                    status, message = lpfm.ContainerLogs()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'ContainerInfo':
                    from DockerSites import Docker_Sites
                    lpfm = Docker_Sites('', data)
                    status, message = lpfm.ContainerInfo()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'DeployN8NContainer':
                    from DockerSites import Docker_Sites
                    lpfm = Docker_Sites('', data)
                    status, message = lpfm.DeployN8NContainer()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}


                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                elif data['function'] == 'RunGivenCommand':

                    from LPVersionManager import LPVersion
                    lpfm = LPVersion(data)
                    status, message = lpfm.RunGivenCommand()

                    if status:
                        RetData = {'status': 1, 'message': message}
                    else:
                        RetData = {'status': 0, 'message': message}

                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
                else:
                    RetData = {'status': 0, 'message': 'This function is not available.'}
                    FinalDataToSend = json.dumps(RetData)
                    encMessage = self.fernet.encrypt(FinalDataToSend.encode())
                    self.connection.sendall(encMessage)
                    # self.connection.sendall(RetData.encode())
                    self.connection.close()
        except BaseException as msg:
            LitePagesLogger.writeforCLI('%s.' % (str(msg)), LitePagesLogger.ERROR, 'HandleRequest.RunJob')


class SetupConn:

    LitePagesConfigPathFile = '/etc/LitePages/config'

    def __init__(self, serv_addr, port):
        self.server_addr = serv_addr
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def setup_conn(self):
        try:
            self.sock.bind(('0.0.0.0', self.port))
        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'SetupConn.setup_conn')

    def start_listening(self):
        try:
            self.sock.listen(5)
            while True:
                connection, client_address = self.sock.accept()
                background = HandleRequest(connection)
                background.start()
        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'SetupConn.start_listening')

    def __del__(self):
        self.sock.close()


def Main():


    parser = argparse.ArgumentParser(description='LitePages server side daemon')
    parser.add_argument('function', help='Specify a operation to perform!')

    args = parser.parse_args()

    if args.function == "start":
        try:
            from LitePagesExecutioner import LitePagesExecutioner
            pid = open('/etc/LitePages/ServerPID', 'r').read().rstrip('\n')
            command = 'kill -9 %s' % (pid)

            lpe = LitePagesExecutioner(command)
            lpe.Execute()
        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'Main.start')

        writeToFile = open('/etc/LitePages/ServerPID', 'w')
        writeToFile.write(str(os.getpid()))
        writeToFile.close()

        ##
        import json
        listenConn = SetupConn(json.loads(open(SetupConn.LitePagesConfigPathFile, 'r').read())['ip'], 2083)
        listenConn.setup_conn()
        listenConn.start_listening()
    elif args.function == "stop":
        try:
            from LitePagesExecutioner import LitePagesExecutioner
            pid = open('/etc/LitePages/ServerPID', 'r').read().rstrip('\n')
            command = 'kill -9 %s' % (pid)

            lpe = LitePagesExecutioner(command)
            lpe.Execute()
        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'Main.stop')
    elif args.function == "restart":
        try:
            from LitePagesExecutioner import LitePagesExecutioner
            pid = open('/etc/LitePages/ServerPID', 'r').read().rstrip('\n')
            command = 'kill -9 %s' % (pid)

            lpe = LitePagesExecutioner(command)
            lpe.Execute()
        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'Main.restart')

        writeToFile = open('/etc/LitePages/ServerPID', 'w')
        writeToFile.write(str(os.getpid()))
        writeToFile.close()

        ##
        import json
        listenConn = SetupConn(json.loads(open(SetupConn.LitePagesConfigPathFile, 'r').read())['ip'], 2083)
        listenConn.setup_conn()
        listenConn.start_listening()

if __name__ == "__main__":
    Main()

