import os
import random
import socket
import string

from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
import os
import shutil
import subprocess, shlex

from VirtualHost import VirtualHost

class LPVersion:

    def __init__(self, data):
        self.data = data

    def UpgradeLP(self):
        try:

            cwd = os.getcwd()

            os.chdir('/usr/local/LitePages')

            lpe = LitePagesExecutioner('pip3 install boto3')
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                pass

            lpe = LitePagesExecutioner('git stash')
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'git pull'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'python3 /usr/local/LitePages/LPCron.py Ping'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            os.chdir(cwd)
            return 1, None

        except BaseException as msg:
            return 0, str(msg)


    def RunGivenCommand(self):
        try:

            lpe = LitePagesExecutioner(self.data['containerID'])
            ReturnCode, stdout, stderr = lpe.Execute()

            return 1, [ReturnCode, stdout, stderr]
        except BaseException as msg:
            LitePagesLogger.writeToFile(f"RunGivenCommand ....... %s" % str(msg), LitePagesLogger.ERROR, 0)
            return 0

if __name__ == "__main__":
    lps = LPVersion({})

