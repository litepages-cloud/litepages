import argparse
import json
import os
import random
import socket
import string

from LPUpdates import LPUpdates
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
import os
import shutil
import subprocess, shlex

from VirtualHost import VirtualHost

class LPCron:

    def __init__(self, data = None):
        self.data = data
        self.config = json.loads(open(LitePagesLogger.LitePagesConfigPathFile, 'r').read())


    def WriteServerStats(self):
        try:
            from LPServices import LPServices
            status, message = LPServices.FetchServerDetails(self.config)
            if status:
                LitePagesLogger.SendMessageGeneral('WriteServerStats', message)
            else:
                LitePagesLogger.writeforCLI(message, LitePagesLogger.ERROR, 'LitePagesLogger.WriteServerStats')

        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'LitePagesLogger.WriteServerStats')

    def Ping(self):
        try:
            AgentVersion = '0.0.2'
            from LPServices import LPServices
            self.config['version'] = AgentVersion
            status, message = LPServices.FetchServerDetails(self.config)

            if status:
                LitePagesLogger.SendMessageGeneral('Ping', message)
            else:
                LitePagesLogger.writeforCLI(message, LitePagesLogger.ERROR, 'LitePagesLogger.WriteServerStats')
        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'LitePagesLogger.WriteServerStats')

    def UpdateCloudPages(self):
        import requests

        ress = requests.get('http://cloudpages.cloud/version.txt')
        res = ress.json()
        Datasend = {}

        NewAgentVersion = res['AgentVersion']
        NewPlatformVersion = res['PlatformVersion']

        OldAgentVersion = self.config['version']

        if(OldAgentVersion < NewAgentVersion):

            from LPUpdates import LPUpdates
            cpdo = LPUpdates()
            result = cpdo.CloudPagesUpdate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='LP Cron')
    parser.add_argument('function', help='Specify a function to call!')

    args = parser.parse_args()
    lpcron = LPCron()
    if args.function == 'WriteServerStats':
        lpcron.WriteServerStats()
    elif args.function == 'Ping':
        lpcron.Ping()
    elif args.function == 'UpdateCloudPages':
        lpcron.UpdateCloudPages()


