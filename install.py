import subprocess
import shutil
import argparse
import os
import shlex
import string
import random
import time

import requests
import json
from stat import *

class LitePagesInstall:



    FetchPublicIPUrl = 'https://cyberpanel.sh/?ip'
    AgentVersion = '0.0.3'
    
    ### These are level of info, for example when writing anything to log it will belong to any of 3 categories.
    ### If something is error, then it needs to be checked

    INFO = 0
    WARNING = 1
    ERROR = 3
    
    LSWSEntVersion = '6.0'
    CPVersion = 'v2.3.3'
    WhiteListIP = '3.66.15.168'

    ## LSWS Conf Templates

    olsMasterMainConf = """virtualHost {virtualHostName} {
  vhRoot                  {path}
  configFile              $SERVER_ROOT/conf/vhosts/$VH_NAME.conf
  allowSymbolLink         1
  enableScript            1
  restrained              1
}
"""

    olsMasterConf = """docRoot                   $VH_ROOT/public_html
vhDomain                  $VH_NAME
vhAliases                 www.$VH_NAME
adminEmails               {adminEmails}
enableGzip                1
enableIpGeo               1

index  {
  useServer               0
  indexFiles              index.php, index.html
}

errorlog $SERVER_ROOT/logs/$VH_NAME/$VH_NAME.error_log {
  useServer               0
  logLevel                ERROR
  rollingSize             10M
}

accesslog $SERVER_ROOT/logs/$VH_NAME/$VH_NAME.access_log {
  useServer               0
  logFormat               "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\""
  logHeaders              5
  rollingSize             10M
  keepDays                10  
  compressArchive         1
}

scripthandler  {
  add                     lsapi:{virtualHostUser} php
}

extprocessor {virtualHostUser} {
  type                    lsapi
  address                 UDS://tmp/lshttpd/{virtualHostUser}.sock
  maxConns                40
  env                     PHP_LSAPI_MAX_REQUESTS=5000
  env                     PHP_LSAPI_CHILDREN=40
  initTimeout             60
  retryTimeout            0
  persistConn             1
  respBuffer              0
  autoStart               2
  backlog                 100
  instances               1
  path                    /usr/local/lsws/lsphp{php}/bin/lsphp
  extUser                 {virtualHostUser}
  extGroup                {virtualHostUser}
  runOnStartUp            3
  priority                0
  memSoftLimit            2047M
  memHardLimit            2047M
  procSoftLimit           400
  procHardLimit           500
}

phpIniOverride  {
}

rewrite  {
  enable                  1
  autoLoadHtaccess        1
}

module cache {
 storagePath /usr/local/lsws/cachedata/$VH_NAME
}

expires {
  enableExpires           1
  expiresByType           text/css=A43200,image/*=A43200,application/x-javascript=A43200,application/javascript=A43200,application/x-font-ttf=A43200,font/*=A43200
}

context /.well-known/acme-challenge {
    location              /usr/local/lsws/Example/html/.well-known/acme-challenge
    allowBrowse           1
}

"""

    lswsMasterConf = """<VirtualHost *:80>

    ServerName {virtualHostName}
    ServerAlias www.{virtualHostName}
    ServerAdmin {administratorEmail}
    SuexecUserGroup {externalApp} {externalApp}
    DocumentRoot {path}/public_html
    Alias /.well-known/acme-challenge /usr/local/lsws/Example/html/.well-known/acme-challenge
    CustomLog /usr/local/lsws/logs/{virtualHostName}/{virtualHostName}.access_log combined
    AddHandler application/x-httpd-php{php} .php .php7 .phtml
    <IfModule LiteSpeed>
        CacheRoot lscache
        CacheLookup on
    </IfModule>

</VirtualHost>
"""

    MySQLInitialConfig = """[client]
port            = 3306
socket          = /var/run/mysqld/mysqld.sock

# Here is entries for some specific programs
# The following values assume you have at least 32M ram

# This was formally known as [safe_mysqld]. Both versions are currently parsed.
"""

    UnitConfig = """[Unit]
Description = LitePages Server Daemon

[Service]
Type=simple
ExecStart = python3 /usr/local/LitePages/LPServer.py start
ExecStop = python3 /usr/local/LitePages/LPServer.py stop
Restart = python3 /usr/local/LitePages/LPServer.py restart
Restart=on-abnormal

[Install]
WantedBy=default.target
"""

    def __init__(self, install, remove, LSWSEnterpriseInstallation, LSWSLicenseKey, token, company, job, id, key):

        ## Detect OS Version

        OSVersion = open('/etc/os-release').read()

        if OSVersion.find('Ubuntu 22.04') > -1:
            self.OSVersion = 'Ubuntu 22'
        else:
            self.OSVersion = 'Ubuntu 20'



        ### Set some important variables to be used later.

        self.cwd = os.getcwd()
        self.ServerRootPath = '/usr/local/lsws/'
        self.LitePagesPath = '/usr/local/LitePages'
        self.LitePagesConfigPathDirectory = '/etc/LitePages'
        self.olsMasterMainConf = '%s/olsMasterMainConf' % (self.LitePagesConfigPathDirectory)
        self.olsMasterConf = '%s/olsMasterConf' % (self.LitePagesConfigPathDirectory)
        self.lswsMasterConf = '%s/lswsMasterConf' % (self.LitePagesConfigPathDirectory)
        self.LitePagesConfigPathFile = '/etc/LitePages/config'
        self.LiteSpeedRepoFilePath = '/etc/apt/sources.list.d/lst_debian_repo.list'
        self.LitePagesServerUnitFile = '/etc/systemd/system/litepages.service'
        self.MySQLPath = '/etc/mysql/my.cnf'
        self.ServerID = id

        ## Some decision variables

        self.install = install
        self.remove = remove
        self.token = token
        self.company = company
        self.job = job
        self.StatusURL = 'https://%s/Base/GlobalAjax' % (self.company)

        ## Check if already installed and exit

        if self.install:
            if os.path.exists(self.LitePagesPath):
                self.Log('LitePages is already installed', LitePagesInstall.ERROR, 1, 1, 0)

            ### Installation root check

            import getpass
            if getpass.getuser() != 'root':
                self.Log('Installation should run as root user. You can switch to root using "sudo su -"', LitePagesInstall.ERROR, 1, 1, 0)

        ##

        if self.install:

            ## Remove some not needed files

            command = 'rm -f enable_lst_debain_repo.sh'
            status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 0)

            command = 'rm -rf lsws-%s' % (LitePagesInstall.LSWSEntVersion)
            status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 0)

            command = 'rm -f lsws-%s-ent-x86_64-linux.tar.gz' % (LitePagesInstall.LSWSEntVersion)
            status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 0)

            ###

            self.MySQLRootPass = self.generate_pass(16)
            self.PublicIP = requests.get(LitePagesInstall.FetchPublicIPUrl).text.rstrip('\n')
            self.LSWSEnterpriseInstallation = LSWSEnterpriseInstallation

            ## Detect and set value of lsws license key

            if self.LSWSEnterpriseInstallation:
                self.LSWSLicenseKey = LSWSLicenseKey

            ### Create Directory to Store LitePages Configurations

            os.mkdir(self.LitePagesConfigPathDirectory)

            ### Config object

            ## { 'ip': '192.168.1.1', 'MySQLRootPass': 'pass', 'token': token. 'company': company.com, 'id': 1, 'key': key }

            config = {'ip': self.PublicIP, 'MySQLRootPass': self.MySQLRootPass, 'token': self.token, 'company': company, 'version': LitePagesInstall.AgentVersion, 'id': self.ServerID, 'key': key}
            self.WriteToFile(self.LitePagesConfigPathFile, json.dumps(config), 'w')
            os.chmod(self.LitePagesConfigPathFile, 0o600)



            ### Save Web server configuration templates

            self.WriteToFile(self.olsMasterMainConf, LitePagesInstall.olsMasterMainConf, 'w')
            self.WriteToFile(self.olsMasterConf, LitePagesInstall.olsMasterConf, 'w')
            self.WriteToFile(self.lswsMasterConf, LitePagesInstall.lswsMasterConf, 'w')

        else:
            try:
                shutil.rmtree(self.LitePagesConfigPathDirectory)
            except:
                pass

            try:
                shutil.rmtree(self.LitePagesPath)
            except:
                pass

    ## Set of common utils required by installer.

    def Execute(self, command, LogLevel, log, DoExit, shell=False):
        try:

            if shell == False:
                result = subprocess.run(shlex.split(command), capture_output=True, text=True)
            else:
                result = subprocess.run(command, capture_output=True, text=True, shell=shell)

            if result.returncode == 0:
                self.ReturnCode = 1
            else:
                self.ReturnCode = 0

            ### Output success, error and return code

            if self.ReturnCode == 0 and DoExit:
                self.Log(result.stdout, LogLevel, 1, 0)
                self.Log(result.stderr, LogLevel, 1, 0)
                self.Log('Return code for command "%s" is %s' % (command, str(self.ReturnCode)), LogLevel, 1, DoExit)
            else:
                self.Log(result.stdout, LogLevel, 0, 0)
                self.Log(result.stderr, LogLevel, 0, 0)
                self.Log('Return code for command "%s" is %s' % (command, str(self.ReturnCode)), LogLevel, 0, 0)

            return self.ReturnCode, result.stdout, result.stderr

        except BaseException as msg:
            message = 'Failed to execute "%s", error %s.' % (command, str(msg))
            self.Log(message, LogLevel, log, DoExit)

    def CheckIfFirewallActive(self):
        command = 'ufw status'
        status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 0, 0)

        if status:
            if success.find('inactive') > -1:
                return 0
            else:
                return 1
        else:
            return 0

    def EnableFireWall(self):
        if self.CheckIfFirewallActive() == 0:
            command = 'echo "y" | ufw enable'
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1, True)

    def AddFirewallRule(self, proto, IPAddress, port):
        command = 'ufw allow proto %s from %s to any port %s' % (proto, IPAddress, port)
        status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

    def WriteToFile(self, file, content, mode):
        writeToFile = open(file, mode)

        if type(content) == list:
            for line in content:
                writeToFile.writelines(line)
        else:
            writeToFile.write(content)

        writeToFile.close()

    def generate_pass(self, length=20):
        chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
        size = length
        return ''.join(random.choice(chars) for x in range(size))

    def Log(self, message, level, log = 0, DoExit = 0, percentage = 0):

        print(message)

        if message == 'Successfully installed.':
            abort = 1
            AbortStatus = 1
            percentage = 100
        elif DoExit:
            abort = 1
            AbortStatus = 0
            percentage = 0
        else:
            abort = 0
            AbortStatus = 0

        if self.install:
            if log:
                try:
                    import requests
                    finalData = json.dumps({'function': 'LogWriting', 'token': self.token, "job": self.job, "message": message,
                                            'level': level, 'abort': abort, 'AbortStatus': AbortStatus, 'Percentage': percentage})
                    requests.post(self.StatusURL, data=finalData)
                except BaseException as msg:
                    print(str(msg))
                    time.sleep(5)

        if DoExit:
            exit(1)

    ## Function to install LiteSpeed repo

    def SetupLiteSpeedRepo(self):
        try:
            if self.install:
                filename = "enable_lst_debain_repo.sh"
                command = "wget http://rpms.litespeedtech.com/debian/" + filename
                status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

                os.chmod(filename, S_IRWXU | S_IRWXG)

                command = "./" + filename
                status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)
            else:
                try:
                    os.remove(self.LiteSpeedRepoFilePath)
                except BaseException as msg:
                    message = 'Failed to remove LiteSpeed repo, error %s.' % (str(msg))
                    self.Log(message, LitePagesInstall.INFO, 1, 0)

        except BaseException as msg:
            message = 'Failed to set up LiteSpeed repo, error %s.' % (str(msg))
            self.Log(message, LitePagesInstall.ERROR, 1, 1)

    ## Function to install LiteSpeed Webserver

    def InstallLiteSpeed(self):
        if self.install:

            command = "apt-get -y install bubblewrap"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

            if self.LSWSEnterpriseInstallation == 0:

                command = "apt-get -y install openlitespeed"
                status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

                ### In OpenLiteSpeed Configurations, change port from 8088 to 80

                self.Log('Changing OpenLiteSpeed port..', LitePagesInstall.INFO, 1, 0)

                data = open(self.ServerRootPath + "conf/httpd_config.conf").readlines()

                writeDataToFile = open(self.ServerRootPath + "conf/httpd_config.conf", 'w')

                for items in data:
                    if (items.find("*:8088") > -1):
                        writeDataToFile.writelines(items.replace("*:8088", "*:80"))
                    else:
                        writeDataToFile.writelines(items)

                writeDataToFile.close()

                self.Log('OpenLiteSpeed port changed.', LitePagesInstall.INFO, 1, 0)

                ### Removing Example VirtualHost

                data = open(self.ServerRootPath + "conf/httpd_config.conf", 'r').readlines()

                writeDataToFile = open(self.ServerRootPath + "conf/httpd_config.conf", 'w')

                for items in data:
                    if items.find("map") > -1 and items.find("Example") > -1:
                        continue
                    else:
                        writeDataToFile.writelines(items)

                writeDataToFile.close()

                ## Example virtual host removed

                ### Add Bubblewrap Command

                WriteToFile = open(self.ServerRootPath + "conf/httpd_config.conf", 'a')
                WriteToFile.writelines('bubbleWrap                1\n')
                WriteToFile.writelines("bubbleWrapCmd             /bin/bwrap --ro-bind /usr /usr --ro-bind /lib /lib --ro-bind-try /lib64 /lib64 --ro-bind /bin /bin --ro-bind /sbin /sbin --dir /var --ro-bind-try /var/www /var/www --dir /tmp --proc /proc --symlink../tmp var/tmp --dev /dev --ro-bind-try /etc/localtime /etc/localtime --ro-bind-try /etc/ld.so.cache /etc/ld.so.cache --ro-bind-try /etc/resolv.conf /etc/resolv.conf --ro-bind-try /etc/ssl /etc/ssl --ro-bind-try /etc/pki /etc/pki --ro-bind-try /etc/man_db.conf /etc/man_db.conf --ro-bind-try /usr/local/bin/msmtp /etc/alternatives/mta --ro-bind-try /usr/local/bin/msmtp /usr/sbin/exim --bind-try $HOMEDIR $HOMEDIR --bind-try /var/lib/mysql/mysql.sock /var/lib/mysql/mysql.sock --bind-try /home/mysql/mysql.sock /home/mysql/mysql.sock --bind-try /tmp/mysql.sock /tmp/mysql.sock  --bind-try /run/mysqld/mysqld.sock /run/mysqld/mysqld.sock --bind-try /var/run/mysqld/mysqld.sock /var/run/mysqld/mysqld.sock '$COPY-TRY /etc/exim.jail/$USER.conf $HOMEDIR/.msmtprc' --unshare-all --share-net --die-with-parent --dir /run/user/$UID ‘$PASSWD 65534’ ‘$GROUP 65534’\n")
                WriteToFile.close()
            else:
                try:
                    command = 'groupadd nobody'
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 0)

                    command = 'usermod -a -G nobody nobody'
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 0)


                    command = 'wget https://www.litespeedtech.com/packages/%s/lsws-%s-ent-x86_64-linux.tar.gz' % (LitePagesInstall.LSWSEntVersion, LitePagesInstall.LSWSEntVersion)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)

                    command = 'tar zxf lsws-%s-ent-x86_64-linux.tar.gz' % (LitePagesInstall.LSWSEntVersion)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)

                    if str.lower(self.LSWSLicenseKey) == 'trial':
                        command = 'wget -q --output-document=lsws-%s/trial.key http://license.litespeedtech.com/reseller/trial.key' % (LitePagesInstall.LSWSEntVersion)
                        status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)
                    else:
                        writeSerial = open('lsws-%s/serial.no' % (LitePagesInstall.LSWSEntVersion), 'w')
                        writeSerial.writelines(self.LSWSLicenseKey)
                        writeSerial.close()

                    ### Curl Important Files

                    command = 'curl https://raw.githubusercontent.com/usmannasir/cyberpanel/%s/install/litespeed/install.sh -o lsws-%s/install.sh' % (LitePagesInstall.CPVersion, LitePagesInstall.LSWSEntVersion)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1, True)

                    command = 'curl https://raw.githubusercontent.com/usmannasir/cyberpanel/%s/install/litespeed/functions.sh -o lsws-%s/functions.sh' % (
                    LitePagesInstall.CPVersion, LitePagesInstall.LSWSEntVersion)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1, True)

                    os.chdir('lsws-%s' % (LitePagesInstall.LSWSEntVersion))

                    command = 'chmod +x install.sh'
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)

                    command = 'chmod +x functions.sh'
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)

                    command = './install.sh'
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)

                    confPath = '/usr/local/lsws/conf/'

                    command = 'curl https://raw.githubusercontent.com/usmannasir/cyberpanel/%s/install/litespeed/httpd_config.xml -o %s/httpd_config.xml' % (
                        LitePagesInstall.CPVersion, confPath)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1, True)

                    command = 'curl https://raw.githubusercontent.com/usmannasir/cyberpanel/%s/install/litespeed/modsec.conf -o %s/modsec.conf' % (
                        LitePagesInstall.CPVersion, confPath)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1, True)

                    command = 'curl https://raw.githubusercontent.com/usmannasir/cyberpanel/%s/install/litespeed/httpd.conf -o %s/httpd.conf' % (
                        LitePagesInstall.CPVersion, confPath)
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1, True)

                    command = 'chown -R lsadm:lsadm ' + confPath
                    status, success, failure = self.Execute(command, LitePagesInstall.INFO, 1, 1)

                except BaseException as msg:
                    message = 'Failed to set install LiteSpeed Enterprise, error %s.' % (str(msg))
                    self.Log(message, LitePagesInstall.ERROR, 1, 1)
        else:

            command = "apt-get -y purge bubblewrap"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

            command = "apt-get -y purge openlitespeed"
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0)
            
            command = 'rm -rf %s' % (self.ServerRootPath)
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0)

    ### Install LiteSpeed PHPs
            
    def InstallLiteSpeedPHPs(self):
        
        if self.install:
        
            ## Install All version of php 7.x

            if self.OSVersion == 'Ubuntu 22':
                command = 'DEBIAN_FRONTEND=noninteractive apt-get -y install ' \
                          'lsphp7? lsphp7?-common lsphp7?-curl lsphp7?-dev lsphp7?-imap lsphp7?-intl lsphp7?-json ' \
                          'lsphp7?-ldap lsphp7?-mysql lsphp7?-opcache lsphp7?-pspell ' \
                          'lsphp7?-sqlite3 lsphp7?-tidy'
            else:
                command = 'DEBIAN_FRONTEND=noninteractive apt-get -y install ' \
                          'lsphp7? lsphp7?-common lsphp7?-curl lsphp7?-dev lsphp7?-imap lsphp7?-intl lsphp7?-json ' \
                          'lsphp7?-ldap lsphp7?-mysql lsphp7?-opcache lsphp7?-pspell ' \
                          'lsphp7?-sqlite3 lsphp7?-tidy'
    
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1, True)
    
            ## Install PHP 8.x
    
            command = 'DEBIAN_FRONTEND=noninteractive apt-get -y install ' \
                      'lsphp8? lsphp8?-common lsphp8?-curl lsphp8?-dev lsphp8?-imap lsphp8?-intl ' \
                      'lsphp8?-ldap lsphp8?-mysql lsphp8?-opcache lsphp8?-pspell ' \
                      'lsphp8?-sqlite3 lsphp8?-tidy'
    
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1, True)
        else:
            command = 'DEBIAN_FRONTEND=noninteractive apt-get purge lsphp* -y'
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0, True)

    ### Install MySQL

    def InstallMySQL(self):

        if self.install:

            ### First Install MySQL

            command = "apt-get -y install mariadb-server"
            if self.OSVersion == 'Ubuntu 22':
                status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)
                if status == 0:
                    command = 'DEBIAN_FRONTEND=noninteractive apt --fix-broken install -y'
                    status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1, True)
            else:

                status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

            command = "systemctl enable mariadb"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

            command = "systemctl start mariadb"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

            ### Change MySQL Password

            if self.OSVersion == 'Ubuntu 22':
                passwordCMD = "use mysql;DROP DATABASE IF EXISTS test;DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%%';GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '%s';flush privileges;" % (self.MySQLRootPass)

            else:
                passwordCMD = "use mysql;DROP DATABASE IF EXISTS test;DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%%';GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY '%s';UPDATE user SET plugin='' WHERE User='root';flush privileges;" % (self.MySQLRootPass)

            command = 'mysql -u root -e "' + passwordCMD + '"'
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

            ### Finally restart MySQL

            command = "systemctl restart mariadb"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

            ## Write MySQL Root pass to /etc/mysql/my.cnf

            self.WriteToFile(self.MySQLPath, LitePagesInstall.MySQLInitialConfig, 'a')

        else:

            command = "apt-get -y purge mariadb-server"
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0)

            command = "rm -rf /var/lib/mysql"
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0)

            command = 'rm -rf /etc/mysql'
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0)

    ### Install zip,unzip,rsync,acme

    def InstallZipUnzipRsyncACME(self):

        if self.install:

            command = "apt-get -y install unzip zip rsync"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

            ## Install ACME

            command = 'wget -O -  https://get.acme.sh | sh'
            subprocess.call(command, shell=True)

            command = '/root/.acme.sh/acme.sh --upgrade --auto-upgrade'
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0, True)

        else:

            command = "apt-get -y purge unzip zip rsync"
            status, success, failure = self.Execute(command, LitePagesInstall.WARNING, 1, 0)

    ### Get LitePages Server Side Code

    def GetLitePagesCode(self):

        if self.install:

            command = 'rm -rf %s' % (self.LitePagesPath)
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

            command = "git clone https://gitlab.com/litepages-cloud/litepages.git %s" % (self.LitePagesPath)
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

        else:

            command = 'rm -rf %s' % (self.LitePagesPath)
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

    ### Set up unit file

    def SetupLitePagesDaemonUnit(self):
        if self.install:
            self.WriteToFile(self.LitePagesServerUnitFile, LitePagesInstall.UnitConfig, 'w')

            command = "systemctl daemon-reload"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

            command = "systemctl enable litepages"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)

            command = "systemctl start litepages"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 1)
        else:

            command = "systemctl stop litepages"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

            try:
                os.remove(self.LitePagesServerUnitFile)
            except:
                pass

            command = "systemctl daemon-reload"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

    def SetupCron(self):
        CronFile = '/var/spool/cron/crontabs/root'
        StatsCron = '*/5 * * * * python3 /usr/local/LitePages/LPCron.py WriteServerStats\n'
        PingCron = '0 0 * * * python3 /usr/local/LitePages/LPCron.py Ping\n'
        CloudflareUpdateIPsCron = '5 8 * * Sun python3 /usr/local/LitePages/LPCron.py UpdateCFIPs\n'
        RenewSSLCron = '0 1 * * * python3 /usr/local/LitePages/renew.py\n'

        if self.install:
            self.WriteToFile(CronFile, StatsCron, 'a')
            self.WriteToFile(CronFile, PingCron, 'a')
            self.WriteToFile(CronFile, CloudflareUpdateIPsCron, 'a')
            self.WriteToFile(CronFile, RenewSSLCron, 'a')
        else:
            try:
                data = open(CronFile, 'r').readlines()

                WriteToFile = open(CronFile, 'w')

                for items in data:
                    if items.find(StatsCron) > -1:
                        continue
                    elif items.find(PingCron) > -1:
                        continue
                    elif items.find(CloudflareUpdateIPsCron) > -1:
                        continue
                    else:
                        WriteToFile.writelines(items)
                WriteToFile.close()
            except:
                pass

    def SendInitialPing(self):
        if self.install:
            command = "python3 /usr/local/LitePages/LPCron.py Ping"
            status, success, failure = self.Execute(command, LitePagesInstall.ERROR, 1, 0)

def main():

    parser = argparse.ArgumentParser(description='LitePages Installer')
    parser.add_argument('--install', help='Weather to install or remove.')
    parser.add_argument('--LSWSEnterpriseInstallation', help='Install LiteSpeed Enterprise or OpenLiteSpeed')
    parser.add_argument('--LSWSLicenseKey', help='LiteSpeed Enterprise Web server License key')
    parser.add_argument('--token', help='Agent Authentication Token')
    parser.add_argument('--job', help='Job ID')
    parser.add_argument('--id', help='Server ID')
    parser.add_argument('--key', help='Fernet key')
    parser.add_argument('--company', help='Company URL to post to.')

    args = parser.parse_args()

    install = int(args.install)
    LSWSEnterpriseInstallation = int(args.LSWSEnterpriseInstallation)

    if LSWSEnterpriseInstallation:
        LSWSLicenseKey = args.LSWSLicenseKey
    else:
        LSWSLicenseKey = None

    if install:
        remove = 0
        message = 'Installation Started..'
        FinalMessage = 'Successfully installed.'
    else:
        message = 'Removing agent..'
        FinalMessage = 'Successfully removed.'
        remove = 1

    lpi = LitePagesInstall(install, remove, LSWSEnterpriseInstallation, LSWSLicenseKey, args.token, args.company, args.job, args.id, args.key)

    lpi.Log(message, LitePagesInstall.INFO, 1, 0)

    if install:
        lpi.Log('Setting up LiteSpeed repo..', LitePagesInstall.INFO, 1, 0, 5)

    lpi.SetupLiteSpeedRepo()

    if install:
        lpi.Log('Installing LiteSpeed..', LitePagesInstall.INFO, 1, 0, 10)

    lpi.InstallLiteSpeed()

    if install:
        lpi.Log('Installing Optimized PHPs..', LitePagesInstall.INFO, 1, 0, 25)

    lpi.InstallLiteSpeedPHPs()

    if install:
        lpi.Log('Installing MariaDB..', LitePagesInstall.INFO, 1, 0, 70)

    lpi.InstallMySQL()

    if install:
        lpi.Log('Installing some components..', LitePagesInstall.INFO, 1, 0, 85)

    lpi.InstallZipUnzipRsyncACME()
    lpi.GetLitePagesCode()
    lpi.SetupLitePagesDaemonUnit()
    lpi.SetupCron()
    lpi.SendInitialPing()

    ### Finall Add Some important firewall rules

    if install:
        lpi.EnableFireWall()
        lpi.AddFirewallRule('tcp', '0.0.0.0/0', '22')
        lpi.AddFirewallRule('tcp', '0.0.0.0/0', '80')
        lpi.AddFirewallRule('tcp', '0.0.0.0/0', '443')
        lpi.AddFirewallRule('udp', '0.0.0.0/0', '443')
        lpi.AddFirewallRule('tcp', LitePagesInstall.WhiteListIP, '2083')

    lpi.Log(FinalMessage, LitePagesInstall.INFO, 1, 0)

if __name__ == "__main__":
    main()