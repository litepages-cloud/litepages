import os
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
import os
import shutil
import subprocess, shlex

from VirtualHost import VirtualHost


class GitManager:

    def __init__(self, data):
        self.data = data

    def AttachGitRepo(self):
        try:

            lpe = LitePagesExecutioner('')
            self.JobID = self.data['JobID']

            ##

            LitePagesLogger.SendStatus('Attaching Git Repo..', self.JobID, 1, 0, 0, 20)


            ### Attaching Repo

            lpe.command = 'sudo -u %s git config --global core.sshCommand "ssh -i %s/.ssh/id_rsa -o "StrictHostKeyChecking=no""' % (self.data['VHUser'],self.data['WebsitePath'])

            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            lpe.command = 'sudo -u %s git clone git@%s:%s/%s.git %s' % (self.data['VHUser'], self.data['GitHost'], self.data['UserName'], self.data['RepoName'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [GitManager.AttachGitRepo:107]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def ProcessWebHook(self):
        try:

            lpe = LitePagesExecutioner('')
            self.JobID = self.data['JobID']

            ##

            lpe.command = "sudo -u %s git -C %s pull" % (self.data['VHUser'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Execute commands if any

            for command in self.data['commands'].split('\n'):

                lpe.command = "sudo -u %s %s" % (self.data['VHUser'], command)
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)


            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.ProcessWebHook:130]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0
