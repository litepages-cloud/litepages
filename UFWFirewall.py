import json
import os
import shutil
import time

from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger

class UFWFirewall:

    ### When ever new version of OWASP files are generated, these variables are updated
    pathTOOWASPFolder = '/usr/local/lsws/conf/modsec/owasp'
    pathToOWASFolderNew = '/usr/local/lsws/conf/modsec/owasp-modsecurity-crs-3.0-master'
    pathToOWASFolderNew332 = '/usr/local/lsws/conf/modsec/coreruleset-3.3.2'
    Default = pathToOWASFolderNew332
    FinalFolder = "coreruleset-3.3.2"
    RulesExclusions = """
SecRule REQUEST_URI "@streq /wp-admin/admin-ajax.php" \\
    "id:100001,\\
    phase:1,\\
    t:none,\\    
    pass,\\
    nolog,\\
    setvar:'tx.restricted_headers=/proxy/ /lock-token/ /translate/ /if/'"
"""

    def __init__(self, data):
        self.data = data

        ### packNaem update because OWASP is now moved to corerulesetrepo so folder name does not contain owasp

        self.data['packName'] = 'coreruleset'

    @staticmethod
    def CheckIfFirewallActive():
        lpe = LitePagesExecutioner('')
        lpe.command = 'ufw status'
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode:
            if stdout.find('inactive') > -1:
                return 0
            else:
                return 1
        else:
            return 0

    @staticmethod
    def EnableFireWall():
        if UFWFirewall.CheckIfFirewallActive() == 0:
            lpe = LitePagesExecutioner('', True, 1, True)
            lpe.command = 'echo "y" | ufw enable'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                if stdout.find('Firewall is active') == -1:
                    return 0, stdout
                else:
                    return 1, None
            else:
                return 0, stderr
        else:
            return 1, None

    @staticmethod
    def AddFirewallRule(proto, IPAddress, port):
        if UFWFirewall.CheckIfFirewallActive() == 1:
            lpe = LitePagesExecutioner('', True, 1, True)
            lpe.command = 'ufw allow proto %s from %s to any port %s' % (proto, IPAddress, port)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                if stdout.find('Rule added') == -1:
                    return 0, stdout
                else:
                    return 1, None
            else:
                return 0, stderr
        else:
            return 0, 'Firewall is not active.'

    @staticmethod
    def FetchRulesJson():
        if UFWFirewall.CheckIfFirewallActive() == 1:
            lpe = LitePagesExecutioner('', True, 1, True)
            lpe.command = 'ufw status numbered'
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                lines = stdout.split('\n')

                json_data = "["
                checker = 0

                for line in lines:
                    result = line.split(' ')
                    result = [string for string in result if string != ""]

                    if len(result) > 5:

                        dic = {
                                'id': result[1].rstrip(']'),
                               'description': ' '.join(result[2:]),
                               }

                        if checker == 0:
                            json_data = json_data + json.dumps(dic)
                            checker = 1
                        else:
                            json_data = json_data + ',' + json.dumps(dic)

                json_data = json_data + ']'
                print(str(json_data))
                return 1, json_data
            else:
                return 0, stderr

        else:
            return 0, 'Firewall is not active.'

    @staticmethod
    def FinalDeleteFirewallRule(data):
        if UFWFirewall.CheckIfFirewallActive() == 1:
            lpe = LitePagesExecutioner('', True, 1, True)
            lpe.command = 'echo y | ufw delete %s' % (str(data['id']))
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:

                return 1, None
            else:
                return 0, stderr
        else:
            return 0, 'Firewall is not active.'

    @staticmethod
    def FirewallStatus(data):
        lpe = LitePagesExecutioner('', True, 1, True)
        if data['status'] == 1:
            status, message = UFWFirewall.EnableFireWall()
            return status, message
        else:
            lpe.command = 'ufw disable'
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:

                return 1, None
            else:
                return 0, stderr

    def InstallModSecurity(self):
        try:
            self.JobID = self.data['JobID']
            LitePagesLogger.SendStatus('Installing ModSecurity..', self.JobID, 1, 0, 0, 10)
            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                lpe = LitePagesExecutioner('')
                lpe.shell = True
                lpe.command = 'sudo DEBIAN_FRONTEND=noninteractive apt-get install ols-modsecurity -y'

                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode:
                    # Setup initial default configs

                    ## Try making a dir that will store ModSec configurations
                    path = '/usr/local/lsws/conf/modsec'
                    try:
                        os.mkdir(path)
                    except:
                        pass

                    initialConfigs = """
module mod_security {
modsecurity  on
modsecurity_rules `
SecDebugLogLevel 0
SecDebugLog /usr/local/lsws/logs/modsec.log
SecAuditEngine on
SecAuditLogRelevantStatus "^(?:5|4(?!04))"
SecAuditLogParts AFH
SecAuditLogType Serial
SecAuditLog /usr/local/lsws/logs/auditmodsec.log
SecRuleEngine On
`
modsecurity_rules_file /usr/local/lsws/conf/modsec/rules.conf
}
"""

                    confFile = '/usr/local/lsws/conf/httpd_config.conf'

                    confData = open(confFile).readlines()
                    confData.reverse()

                    modSecConfigFlag = False

                    for items in confData:
                        if items.find('module mod_security') > -1:
                            modSecConfigFlag = True
                            break

                    if modSecConfigFlag == False:
                        conf = open(confFile, 'a+')
                        conf.write(initialConfigs)
                        conf.close()

                    rulesFilePath = '/usr/local/lsws/conf/modsec/rules.conf'

                    if not os.path.exists(rulesFilePath):
                        initialRules = """SecRule ARGS "\.\./" "t:normalisePathWin,id:99999,severity:4,msg:'Drive Access' ,log,auditlog,deny"""
                        rule = open(rulesFilePath, 'a+')
                        rule.write(initialRules)
                        rule.close()

                    LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)
                else:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
            else:
                LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus(str(msg), self.JobID, 1, 1, 0, 0)

    def FetchModSecConfigs(self):
        try:

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:

                modsecurity = 0
                SecAuditEngine = 0
                SecRuleEngine = 0
                SecDebugLogLevel = "9"
                SecAuditLogRelevantStatus = '^(?:5|4(?!04))'
                SecAuditLogParts = 'ABIJDEFHZ'
                SecAuditLogType = 'Serial'

                confPath = '/usr/local/lsws/conf/httpd_config.conf'
                lpe = LitePagesExecutioner('')
                lpe.command = "sudo cat " + confPath
                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode:

                    data = stdout.split('\n')

                    for items in data:

                        if items.find('modsecurity ') > -1:
                            if items.find('on') > -1 or items.find('On') > -1:
                                modsecurity = 1
                                continue
                        if items.find('SecAuditEngine ') > -1:
                            if items.find('on') > -1 or items.find('On') > -1:
                                SecAuditEngine = 1
                                continue

                        if items.find('SecRuleEngine ') > -1:
                            if items.find('on') > -1 or items.find('On') > -1:
                                SecRuleEngine = 1
                                continue

                        if items.find('SecDebugLogLevel') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecDebugLogLevel':
                                SecDebugLogLevel = result[1]
                                continue
                        if items.find('SecAuditLogRelevantStatus') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecAuditLogRelevantStatus':
                                SecAuditLogRelevantStatus = result[1]
                                continue
                        if items.find('SecAuditLogParts') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecAuditLogParts':
                                SecAuditLogParts = result[1]
                                continue
                        if items.find('SecAuditLogType') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecAuditLogType':
                                SecAuditLogType = result[1]
                                continue

                    final_dic = {'fetchStatus': 1,
                                 'installed': 1,
                                 'SecRuleEngine': SecRuleEngine,
                                 'modsecurity': modsecurity,
                                 'SecAuditEngine': SecAuditEngine,
                                 'SecDebugLogLevel': SecDebugLogLevel,
                                 'SecAuditLogParts': SecAuditLogParts,
                                 'SecAuditLogRelevantStatus': SecAuditLogRelevantStatus,
                                 'SecAuditLogType': SecAuditLogType,
                                 }
                else:
                    return 0, stderr
            else:
                SecAuditEngine = 0
                SecRuleEngine = 0
                SecDebugLogLevel = "9"
                SecAuditLogRelevantStatus = '^(?:5|4(?!04))'
                SecAuditLogParts = 'ABIJDEFHZ'
                SecAuditLogType = 'Serial'

                confPath = '/usr/local/lsws/conf/modsec.conf'

                lpe = LitePagesExecutioner('')
                lpe.command = "sudo cat " + confPath
                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode:
                    data = stdout.split('\n')
                    for items in data:
                        if items.find('SecAuditEngine ') > -1:
                            if items.find('on') > -1 or items.find('On') > -1:
                                SecAuditEngine = 1
                                continue

                        if items.find('SecRuleEngine ') > -1:
                            if items.find('on') > -1 or items.find('On') > -1:
                                SecRuleEngine = 1
                                continue

                        if items.find('SecDebugLogLevel') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecDebugLogLevel':
                                SecDebugLogLevel = result[1]
                                continue
                        if items.find('SecAuditLogRelevantStatus') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecAuditLogRelevantStatus':
                                SecAuditLogRelevantStatus = result[1]
                                continue
                        if items.find('SecAuditLogParts') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecAuditLogParts':
                                SecAuditLogParts = result[1]
                                continue
                        if items.find('SecAuditLogType') > -1:
                            result = items.split(' ')
                            if result[0] == 'SecAuditLogType':
                                SecAuditLogType = result[1]
                                continue

                    final_dic = {'fetchStatus': 1,
                                 'installed': 1,
                                 'SecRuleEngine': SecRuleEngine,
                                 'SecAuditEngine': SecAuditEngine,
                                 'SecDebugLogLevel': SecDebugLogLevel,
                                 'SecAuditLogParts': SecAuditLogParts,
                                 'SecAuditLogRelevantStatus': SecAuditLogRelevantStatus,
                                 'SecAuditLogType': SecAuditLogType,
                                 }
            return 1, final_dic

        except BaseException as msg:
            return 0, str(msg)

    def saveModSecConfigs(self):
        try:

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:

                confFile = os.path.join('/usr/local/lsws/conf/httpd_config.conf')
                confData = open(confFile).readlines()
                conf = open(confFile, 'w')

                for items in confData:
                    if items.find('SecAuditEngine ') > -1:
                        if self.data['SecAuditEngine']:
                            conf.writelines('SecAuditEngine on\n')
                        else:
                            conf.writelines('SecAuditEngine off\n')
                        continue
                    elif items.find('SecRuleEngine ') > -1:
                        if self.data['SecRuleEngine']:
                            conf.writelines('SecRuleEngine on\n')
                        else:
                            conf.writelines('SecRuleEngine off\n')
                        continue
                    elif items.find('SecDebugLogLevel') > -1:
                        conf.writelines('SecDebugLogLevel %s\n' % (self.data['SecDebugLogLevel']))
                        continue
                    elif items.find('SecAuditLogRelevantStatus ') > -1:
                        conf.writelines('SecAuditLogRelevantStatus %s\n' % (self.data['SecAuditLogRelevantStatus']))
                        continue
                    elif items.find('SecAuditLogParts ') > -1:
                        conf.writelines('SecAuditLogParts %s\n' % (self.data['SecAuditLogParts']))
                        continue
                    elif items.find('SecAuditLogType ') > -1:
                        conf.writelines('SecAuditLogType %s\n' % (self.data['SecAuditLogType']))
                        continue
                    else:
                        conf.writelines(items)

                conf.close()

                LitePagesExecutioner.RestartServer()

                return 1, None
            else:
                confFile = '/usr/local/lsws/conf/modsec.conf'
                confData = open(confFile).readlines()
                conf = open(confFile, 'w')

                for items in confData:
                    if items.find('SecAuditEngine ') > -1:
                        if self.data['SecAuditEngine']:
                            conf.writelines('SecAuditEngine on\n')
                        else:
                            conf.writelines('SecAuditEngine off\n')
                        continue
                    elif items.find('SecRuleEngine ') > -1:
                        if self.data['SecRuleEngine']:
                            conf.writelines('SecRuleEngine on\n')
                        else:
                            conf.writelines('SecRuleEngine off\n')
                        continue
                    elif items.find('SecDebugLogLevel') > -1:
                        conf.writelines('SecDebugLogLevel %s\n' % (self.data['SecDebugLogLevel']))
                        continue
                    elif items.find('SecAuditLogRelevantStatus ') > -1:
                        conf.writelines('SecAuditLogRelevantStatus %s\n' % (self.data['SecAuditLogRelevantStatus']))
                        continue
                    elif items.find('SecAuditLogParts ') > -1:
                        conf.writelines('SecAuditLogParts %s\n' % (self.data['SecAuditLogParts']))
                        continue
                    elif items.find('SecAuditLogType ') > -1:
                        conf.writelines('SecAuditLogType %s\n' % (self.data['SecAuditLogType']))
                        continue
                    else:
                        conf.writelines(items)

                conf.close()

                LitePagesExecutioner.RestartServer()

                return 1, None

        except BaseException as msg:
            return 1, str(msg)

    ####

    def setupOWASPRules(self):
        try:

            lpe = LitePagesExecutioner('')
            lpe.shell = True
            lpe.command = 'mkdir -p /usr/local/lsws/conf/modsec'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            ### Remove old rules folders if any

            if os.path.exists(UFWFirewall.pathToOWASFolderNew):
                shutil.rmtree(UFWFirewall.pathToOWASFolderNew)

            if os.path.exists(UFWFirewall.pathTOOWASPFolder):
                shutil.rmtree(UFWFirewall.pathTOOWASPFolder)

            if os.path.exists(UFWFirewall.pathToOWASFolderNew332):
                shutil.rmtree(UFWFirewall.pathToOWASFolderNew332)

            if os.path.exists(UFWFirewall.Default):
                shutil.rmtree(UFWFirewall.Default)

            if os.path.exists('owasp.tar.gz'):
                os.remove('owasp.tar.gz')

            lpe.command = "wget https://github.com/coreruleset/coreruleset/archive/refs/tags/v3.3.2.zip -O /usr/local/lsws/conf/modsec/owasp.zip"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = "unzip -o /usr/local/lsws/conf/modsec/owasp.zip -d /usr/local/lsws/conf/modsec/"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'mv %s/crs-setup.conf.example %s/crs-setup.conf' % (UFWFirewall.pathToOWASFolderNew332, UFWFirewall.pathToOWASFolderNew332)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'mv %s/rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf.example %s/rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf' % (UFWFirewall.pathToOWASFolderNew332, UFWFirewall.pathToOWASFolderNew332)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'mv %s/rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf.example %s/rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf' % (
            UFWFirewall.pathToOWASFolderNew332, UFWFirewall.pathToOWASFolderNew332)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                return 0, stderr

            content = """include {pathToOWASFolderNew}/crs-setup.conf
include {pathToOWASFolderNew}/rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf
include {pathToOWASFolderNew}/rules/REQUEST-901-INITIALIZATION.conf
include {pathToOWASFolderNew}/rules/REQUEST-905-COMMON-EXCEPTIONS.conf
include {pathToOWASFolderNew}/rules/REQUEST-910-IP-REPUTATION.conf
include {pathToOWASFolderNew}/rules/REQUEST-911-METHOD-ENFORCEMENT.conf
include {pathToOWASFolderNew}/rules/REQUEST-912-DOS-PROTECTION.conf
include {pathToOWASFolderNew}/rules/REQUEST-913-SCANNER-DETECTION.conf
include {pathToOWASFolderNew}/rules/REQUEST-920-PROTOCOL-ENFORCEMENT.conf
include {pathToOWASFolderNew}/rules/REQUEST-921-PROTOCOL-ATTACK.conf
include {pathToOWASFolderNew}/rules/REQUEST-930-APPLICATION-ATTACK-LFI.conf
include {pathToOWASFolderNew}/rules/REQUEST-931-APPLICATION-ATTACK-RFI.conf
include {pathToOWASFolderNew}/rules/REQUEST-932-APPLICATION-ATTACK-RCE.conf
include {pathToOWASFolderNew}/rules/REQUEST-933-APPLICATION-ATTACK-PHP.conf
include {pathToOWASFolderNew}/rules/REQUEST-941-APPLICATION-ATTACK-XSS.conf
include {pathToOWASFolderNew}/rules/REQUEST-942-APPLICATION-ATTACK-SQLI.conf
include {pathToOWASFolderNew}/rules/REQUEST-943-APPLICATION-ATTACK-SESSION-FIXATION.conf
include {pathToOWASFolderNew}/rules/REQUEST-949-BLOCKING-EVALUATION.conf
include {pathToOWASFolderNew}/rules/RESPONSE-950-DATA-LEAKAGES.conf
include {pathToOWASFolderNew}/rules/RESPONSE-951-DATA-LEAKAGES-SQL.conf
include {pathToOWASFolderNew}/rules/RESPONSE-952-DATA-LEAKAGES-JAVA.conf
include {pathToOWASFolderNew}/rules/RESPONSE-953-DATA-LEAKAGES-PHP.conf
include {pathToOWASFolderNew}/rules/RESPONSE-954-DATA-LEAKAGES-IIS.conf
include {pathToOWASFolderNew}/rules/RESPONSE-959-BLOCKING-EVALUATION.conf
include {pathToOWASFolderNew}/rules/RESPONSE-980-CORRELATION.conf
include {pathToOWASFolderNew}/rules/RESPONSE-999-EXCLUSION-RULES-AFTER-CRS.conf
"""
            writeToFile = open('%s/owasp-master.conf' % (UFWFirewall.pathToOWASFolderNew332), 'w')
            writeToFile.write(content.replace('{pathToOWASFolderNew}', UFWFirewall.pathToOWASFolderNew332))
            writeToFile.close()


            ### Fix conflick with nicepage plugin

            RuleExclusionPath = '%s/rules/REQUEST-900-EXCLUSION-RULES-BEFORE-CRS.conf' % (UFWFirewall.pathToOWASFolderNew332)

            WriteToFile = open(RuleExclusionPath, 'a')
            WriteToFile.write(UFWFirewall.RulesExclusions)
            WriteToFile.close()

            ###

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def installOWASP(self):
        try:
            result = self.setupOWASPRules()
            if result[0] == 0:
                return 0, result[1]

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                owaspRulesConf = """
modsecurity_rules_file /usr/local/lsws/conf/modsec/%s/owasp-master.conf
""" % (UFWFirewall.FinalFolder)

                confFile = '/usr/local/lsws/conf/httpd_config.conf'

                confData = open(confFile).readlines()

                conf = open(confFile, 'w')

                for items in confData:
                    if items.find('/usr/local/lsws/conf/modsec/rules.conf') > -1:
                        conf.writelines(items)
                        conf.write(owaspRulesConf)
                        continue
                    else:
                        conf.writelines(items)

                conf.close()
            else:
                confFile = os.path.join('/usr/local/lsws/conf/modsec.conf')
                confData = open(confFile).readlines()

                conf = open(confFile, 'w')

                for items in confData:
                    if items.find('/conf/comodo_litespeed/') > -1:
                        conf.writelines(items)
                        conf.write('Include /usr/local/lsws/conf/modsec/%s/*.conf\n' % (UFWFirewall.FinalFolder))
                        continue
                    else:
                        conf.writelines(items)

                conf.close()

            LitePagesExecutioner.RestartServer()

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def disableOWASP(self):
        try:

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:

                confFile = '/usr/local/lsws/conf/httpd_config.conf'
                confData = open(confFile).readlines()
                conf = open(confFile, 'w')

                for items in confData:
                    if items.find('modsec/%s' % (self.data['packName'])) > -1:
                        continue
                    else:
                        conf.writelines(items)
                conf.close()
            else:
                confFile = '/usr/local/lsws/conf/modsec.conf'

                confData = open(confFile).readlines()
                conf = open(confFile, 'w')

                for items in confData:
                    if items.find('modsec/%s' % (self.data['packName'])) > -1:
                        continue
                    else:
                        conf.writelines(items)

                conf.close()
            LitePagesExecutioner.RestartServer()

            return 1, None

        except BaseException as msg:
            return 1, None

    def FetchModSecRules(self):
        try:
            ## check init function as packName gets manually modified

            packName = self.data['packName']

            confPath = os.path.join('/usr/local/lsws/conf/modsec/%s/owasp-master.conf' % (UFWFirewall.FinalFolder))
            lpe = LitePagesExecutioner("sudo cat " + confPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                httpdConfig = stdout.splitlines()

                json_data = "["
                checker = 0
                counter = 0

                for items in httpdConfig:

                    if items.find('modsec/' + packName) > -1:
                        counter = counter + 1
                        if items[0] == '#':
                            status = False
                        else:
                            status = True

                        fileName = items.lstrip('#')
                        fileName = fileName.split('/')[-1]

                        dic = {
                            'id': counter,
                            'fileName': fileName,
                            'packName': packName,
                            'status': status,

                        }

                        if checker == 0:
                            json_data = json_data + json.dumps(dic)
                            checker = 1
                        else:
                            json_data = json_data + ',' + json.dumps(dic)

                json_data = json_data + ']'
                return 1, json_data
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def disableRuleFile(self):
        try:

            ## check init function as packName gets manually modified

            confFile = os.path.join('/usr/local/lsws/conf/modsec/%s/owasp-master.conf' % (UFWFirewall.FinalFolder))
            confData = open(confFile).readlines()
            conf = open(confFile, 'w')

            for items in confData:
                if items.find('modsec/' + self.data['packName']) > -1 and items.find(self.data['fileName']) > -1:
                    conf.write("#" + items)
                else:
                    conf.writelines(items)

            conf.close()

            LitePagesExecutioner.RestartServer()

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def enableRuleFile(self):
        try:

            ## check init function as packName gets manually modified

            confFile = os.path.join('/usr/local/lsws/conf/modsec/%s/owasp-master.conf' % (UFWFirewall.FinalFolder))
            confData = open(confFile).readlines()
            conf = open(confFile, 'w')

            for items in confData:
                if items.find('modsec/' + self.data['packName']) > -1 and items.find(self.data['fileName']) > -1:
                    conf.write(items.lstrip('#'))
                else:
                    conf.writelines(items)

            conf.close()

            LitePagesExecutioner.RestartServer()

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    @staticmethod
    def getSSHConfigs():
        try:
            retData = {}

            ## temporarily changing permission for sshd files

            pathToSSH = "/etc/ssh/sshd_config"

            data = open(pathToSSH, 'r').readlines()

            retData['permitRootLogin'] = 0
            retData['sshPort'] = "22"

            for items in data:
                if items.find("PermitRootLogin") > -1:
                    if items.find("Yes") > -1 or items.find("yes") > -1:
                        retData['permitRootLogin'] = 1
                        continue
                if items.find("Port") > -1 and not items.find("GatewayPorts") > -1:
                    retData['sshPort'] = items.split(" ")[1].strip("\n")

            return 1, retData

        except BaseException as msg:
            return 0, str(msg)

    def saveSSHConfigs(self):
        try:

            if self.data['rootLogin']:
                rootLogin = "PermitRootLogin yes\n"
            else:
                rootLogin = "PermitRootLogin no\n"

            sshPort = "Port " + self.data['sshPort'] + "\n"

            pathToSSH = "/etc/ssh/sshd_config"

            data = open(pathToSSH, 'r').readlines()

            writeToFile = open(pathToSSH, "w")

            for items in data:
                if items.find("PermitRootLogin") > -1:
                    if items.find("Yes") > -1 or items.find("yes"):
                        writeToFile.writelines(rootLogin)
                        continue
                elif items.find("Port") > -1:
                    writeToFile.writelines(sshPort)
                else:
                    writeToFile.writelines(items)
            writeToFile.close()

            lpe = LitePagesExecutioner('')
            lpe.command = 'systemctl restart sshd'
            ReturnCode, stdout, stderr = lpe.Execute()

            UFWFirewall.AddFirewallRule('tcp', '0.0.0.0/0', self.data['sshPort'])

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    ###

    def FetchUserSSHKeysGlobal(self):
        try:
            lpe = LitePagesExecutioner('')

            lpe.command = "cat /root/.ssh/authorized_keys"
            ReturnCode, stdout, stderr = lpe.Execute()
            data = stdout.split('\n')

            json_data = "["
            checker = 0

            for items in data:
                if items.find("ssh-rsa") > -1:
                    keydata = items.split(" ")

                    try:
                        key = "ssh-rsa " + keydata[1][:50] + "  ..  " + keydata[2]
                        try:
                            userName = keydata[2][:keydata[2].index("@")]
                        except:
                            userName = keydata[2]
                    except:
                        key = "ssh-rsa " + keydata[1][:50]
                        userName = ''

                    dic = {
                            'userName': userName,
                           'key': key,
                           }

                    if checker == 0:
                        json_data = json_data + json.dumps(dic)
                        checker = 1
                    else:
                        json_data = json_data + ',' + json.dumps(dic)

            json_data = json_data + ']'

            return 1, json_data

        except BaseException as msg:
            return 0, str(msg)

    def AddUserPublicKeyGlobal(self):
        try:

            lpe = LitePagesExecutioner('')

            lpe.command = "mkdir /root/.ssh"
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = "chmod 700 /root/.ssh"
            ReturnCode, stdout, stderr = lpe.Execute()

            pathToSSH = "/root/.ssh/authorized_keys"

            if os.path.exists(pathToSSH):
                pass
            else:
                sshFile = open(pathToSSH, 'w')
                sshFile.writelines('###############')
                sshFile.close()


            lpe.command = "chmod 600 %s" % (pathToSSH)
            ReturnCode, stdout, stderr = lpe.Execute()

            presenseCheck = 0
            try:
                data = open(pathToSSH, "r").readlines()
                for items in data:
                    if items.find(self.data['key']) > -1:
                        presenseCheck = 1
            except:
                pass

            if presenseCheck == 0:
                writeToFile = open(pathToSSH, 'a')
                writeToFile.writelines("\n")
                writeToFile.writelines(self.data['key'])
                writeToFile.writelines("\n")
                writeToFile.close()

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def DeleteUserPublicKeyGlobal(self):
        try:
            keyPart = self.data['key'].split(" ")[1]
            pathToSSH = "/root/.ssh/authorized_keys"

            data = open(pathToSSH, 'r').readlines()

            writeToFile = open(pathToSSH, "w")

            for items in data:
                if items.find("ssh-rsa") > -1 and items.find(keyPart) > -1:
                    continue
                else:
                    writeToFile.writelines(items)

            writeToFile.close()
            return 1, None

        except BaseException as msg:
            return 0, str(msg)

def Main():
    print(UFWFirewall.FetchRulesJson())

if __name__ == "__main__":
    Main()