import re
import time

from _cffi_backend import string
# from django.http import HttpResponse

import ApacheVhost
from ApacheController import ApacheController
from ApacheVhost import ApacheVhost
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
import os
import shutil
import subprocess, shlex
import json
import LPServer


from vhost import vhost
from vhostConfs import vhostConfs


class VirtualHost:
    VhostConfPath = '/usr/local/lsws/conf/vhosts'
    apache = 1
    ols = 2
    lsws = 3

    def __init__(self, data):

        ### Set some important variables to be used later.

        self.cwd = os.getcwd()
        self.ServerRootPath = '/usr/local/lsws/'
        self.LitePagesPath = '/usr/local/LitePages'
        self.LitePagesConfigPathDirectory = '/etc/LitePages'
        self.olsMasterMainConf = '%s/olsMasterMainConf' % (self.LitePagesConfigPathDirectory)
        self.olsMasterConf = '%s/olsMasterConf' % (self.LitePagesConfigPathDirectory)
        self.lswsMasterConf = '%s/lswsMasterConf' % (self.LitePagesConfigPathDirectory)
        self.LitePagesConfigPathFile = '/etc/LitePages/config'
        self.VhostPath = "%sconf/vhosts" % (self.ServerRootPath)

        ##

        self.data = data

    ### Code for creating VHOST

    @staticmethod
    def getPHPString(phpVersion):

        if phpVersion == "PHP 5.3":
            php = "53"
        elif phpVersion == "PHP 5.4":
            php = "54"
        elif phpVersion == "PHP 5.5":
            php = "55"
        elif phpVersion == "PHP 5.6":
            php = "56"
        elif phpVersion == "PHP 7.0":
            php = "70"
        elif phpVersion == "PHP 7.1":
            php = "71"
        elif phpVersion == "PHP 7.2":
            php = "72"
        elif phpVersion == "PHP 7.3":
            php = "73"
        elif phpVersion == "PHP 7.4":
            php = "74"
        elif phpVersion == "PHP 8.0":
            php = "80"
        elif phpVersion == "PHP 8.1":
            php = "81"
        elif phpVersion == "PHP 8.2":
            php = "82"
        elif phpVersion == "PHP 8.3":
            php = "83"

        return php

    def CreateUser(self):

        ### Create Virtual Host Directories

        lpe = LitePagesExecutioner('')

        ##

        LitePagesLogger.statusWriter('Creating Users..,5', LitePagesLogger.INFO, self.JobID)

        lpe.command = '/usr/sbin/adduser --no-create-home --home ' + self.path + ' --disabled-login --gecos "" ' + self.VHUser
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateUser')

        lpe.command = "/usr/sbin/groupadd " + self.VHUser
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateUser')

        lpe.command = "/usr/sbin/usermod -a -G " + self.VHUser + " " + self.VHUser
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateUser')

        return 1, None

    def CreateDirectories(self):

        ### Create Virtual Host Directories

        lpe = LitePagesExecutioner('')

        if not os.path.exists('/usr/local/lsws/Example/html/.well-known/acme-challenge'):
            lpe.command = 'mkdir -p /usr/local/lsws/Example/html/.well-known/acme-challenge'
            ReturnCode, stdout, stderr = lpe.Execute()

        ##

        LitePagesLogger.statusWriter('Creating directories..,30', LitePagesLogger.INFO, self.JobID)

        lpe.command = 'mkdir -p %s' % (self.path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        lpe.command = 'chown %s:%s %s' % (self.VHUser, self.VHUser, self.path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        lpe.command = 'chmod 711 %s' % (self.path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        ##

        lpe.command = 'mkdir -p %s' % (self.HTMLPath)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        lpe.command = 'chown %s:nogroup %s' % (self.VHUser, self.HTMLPath)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        lpe.command = 'chmod 750 %s' % (self.HTMLPath)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        ##

        lpe.command = 'mkdir -p %s' % (self.PathLogs)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        lpe.command = 'chown %s:nogroup %s' % (self.VHUser, self.PathLogs)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        lpe.command = 'chmod 750 %s' % (self.PathLogs)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateDirectories')

        return 1, None

    def CreateVHConfigFile(self):
        import os

        if not os.path.exists(self.VhostPath):

            command = 'mkdir -p %s' % (self.VhostPath)
            lpe = LitePagesExecutioner(command)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateVHConfigFile')

            lpe.command = 'chown lsadm:lsadm %s' % (self.VhostPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateVHConfigFile')

        # General Configurations tab

        php = VirtualHost.getPHPString(self.PHPVersion)

        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            try:

                confFile = open(self.CompletePathToConfigFile, "w+")

                from install import LitePagesInstall

                currentConf = LitePagesInstall.olsMasterConf
                currentConf = currentConf.replace('{adminEmails}', self.email)
                currentConf = currentConf.replace('{virtualHostUser}', self.VHUser)
                currentConf = currentConf.replace('{php}', php)
                currentConf = currentConf.replace('{adminEmails}', self.email)
                currentConf = currentConf.replace('{php}', php)

                if self.OpenBaseDir == 1:
                    currentConf = currentConf.replace('{open_basedir}', 'php_admin_value open_basedir "/tmp:$VH_ROOT"')
                else:
                    currentConf = currentConf.replace('{open_basedir}', '')

                confFile.write(currentConf)
                confFile.close()

            except BaseException as msg:
                return 0, str(msg)
        else:
            try:

                confFile = open(self.CompletePathToConfigFile, "w+")

                from install import LitePagesInstall

                currentConf = LitePagesInstall.lswsMasterConf

                currentConf = currentConf.replace('{virtualHostName}', self.domain)
                currentConf = currentConf.replace('{administratorEmail}', self.email)
                currentConf = currentConf.replace('{externalApp}', self.VHUser)
                currentConf = currentConf.replace('{php}', php)
                currentConf = currentConf.replace('{path}', self.path)

                confFile.write(currentConf)

                confFile.close()

            except BaseException as msg:
                return 0, str(msg)

        ## Fixing config file permissions

        lpe = LitePagesExecutioner('')

        lpe.command = "chown lsadm:lsadm %s" % (self.CompletePathToConfigFile)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateVHConfigFile')

        lpe.command = 'chmod 600 %s' % (self.CompletePathToConfigFile)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            LitePagesLogger.writeforCLI(stderr, LitePagesLogger.INFO, 'VirtualHost.CreateVHConfigFile')

        return 1, None

    def createConfigInMainVirtualHostFile(self):
        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            try:

                status, message = self.createNONSSLMapEntry()

                if status == 0:
                    return 0, message

                writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'a')

                currentConf = open(self.olsMasterMainConf, 'r').read()
                currentConf = currentConf.replace('{virtualHostName}', self.domain)
                currentConf = currentConf.replace('{path}', self.path)
                writeDataToFile.write(currentConf)

                writeDataToFile.close()

                return 1, None
            except BaseException as msg:
                return 0, str(msg)
        else:
            try:
                writeDataToFile = open("/usr/local/lsws/conf/httpd.conf", 'a')
                configFile = 'Include /usr/local/lsws/conf/vhosts/' + self.domain + '.conf\n'
                writeDataToFile.writelines(configFile)
                writeDataToFile.close()

                writeDataToFile.close()
                return 1, None
            except BaseException as msg:
                return 0, str(msg)

    ### This function below is for OLS only

    def createNONSSLMapEntry(self):
        try:
            data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()
            writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'w')

            map = "  map                     " + self.domain + " " + self.domain + "\n"

            mapchecker = 1

            for items in data:
                if (mapchecker == 1 and (items.find("listener") > -1 and items.find("Default") > -1)):
                    writeDataToFile.writelines(items)
                    writeDataToFile.writelines(map)
                    mapchecker = 0
                else:
                    writeDataToFile.writelines(items)

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    ## SSL Related Functions

    def checkSSLListener(self):
        try:

            data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()
            for items in data:
                if items.find("listener SSL") > -1:
                    return 1

        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'VirtualHost.checkSSLListener')
            return 0
        return 0

    def checkIfSSLMap(self):
        try:

            data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()

            sslCheck = 0

            for items in data:
                if items.find("listener") > -1 and items.find("SSL") > -1:
                    sslCheck = 1
                    continue
                if sslCheck == 1:
                    if items.find("}") > -1:
                        return 0
                if items.find(self.domain) > -1 and sslCheck == 1:
                    data = [_f for _f in items.split(" ") if _f]
                    if data[1] == self.domain:
                        return 1

        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'VirtualHost.checkIfSSLMap')
            return 0

    def obtainSSLForADomain(self):
        try:
            lpe = LitePagesExecutioner('')

            if not os.path.exists('/usr/local/lsws/Example/html/.well-known/acme-challenge'):
                lpe.command = 'mkdir -p /usr/local/lsws/Example/html/.well-known/acme-challenge'
                ReturnCode, stdout, stderr = lpe.Execute()

            acmePath = '/root/.acme.sh/acme.sh'
            acmePathSecond = '/.acme.sh/acme.sh'

            if not os.path.exists(acmePath) and not os.path.exists(acmePathSecond):
                ## Install ACME
                lpe.shell = True
                lpe.command = 'git clone https://github.com/acmesh-official/acme.sh.git; cd acme.sh; ./acme.sh --install --home /root/.acme.sh'
                lpe.Execute()

                lpe.command = '/root/.acme.sh/acme.sh --upgrade --auto-upgrade'
                lpe.Execute()

                lpe.shell = False

            if not os.path.exists(acmePath):
                acmePath = acmePathSecond

            ### Register acme acct for zero ssl

            lpe.command = '%s --register-account -m ssl@%s' % (acmePath, self.domain)
            lpe.Execute()

            ##

            existingCertPath = '/etc/letsencrypt/live/' + self.domain

            if not os.path.exists(existingCertPath):
                command = 'mkdir -p ' + existingCertPath
                subprocess.call(shlex.split(command))

            try:
                LitePagesLogger.statusWriter("Trying to obtain SSL for: " + self.domain + " and: www." + self.domain,
                                             LitePagesLogger.INFO, self.JobID)

                command = acmePath + " --issue -d " + self.domain + " -d www." + self.domain \
                          + ' --cert-file ' + existingCertPath + '/cert.pem' + ' --key-file ' + existingCertPath + '/privkey.pem' \
                          + ' --fullchain-file ' + existingCertPath + '/fullchain.pem' + ' -w ' + self.HTMLPath + ' -k ec-256 --force --server letsencrypt'

                LitePagesLogger.writeforCLI(command, LitePagesLogger.INFO, 'VirtualHost.obtainSSLForADomain')

                output = subprocess.check_output(shlex.split(command)).decode("utf-8")

                LitePagesLogger.writeforCLI(
                    "Successfully obtained SSL for: " + self.domain + " and: www." + self.domain, LitePagesLogger.INFO,
                    'VirtualHost.obtainSSLForADomain')

            except subprocess.CalledProcessError as msg:
                LitePagesLogger.SendStatus('%s [CreateVHConfigFile.obtainSSLForADomain:387]' % (str(msg)), self.JobID,
                                           1, 0, 0, 70)
                LitePagesLogger.writeforCLI("Failed to obtain SSL for: " + self.domain + " and: www." + self.domain,
                                            LitePagesLogger.INFO, 'VirtualHost.obtainSSLForADomain')
                try:
                    LitePagesLogger.writeforCLI("Trying to obtain SSL for: " + self.domain, LitePagesLogger.INFO,
                                                'VirtualHost.obtainSSLForADomain')

                    command = acmePath + " --issue -d " + self.domain + ' --cert-file ' + existingCertPath \
                              + '/cert.pem' + ' --key-file ' + existingCertPath + '/privkey.pem' \
                              + ' --fullchain-file ' + existingCertPath + '/fullchain.pem' + ' -w ' + self.HTMLPath + ' -k ec-256 --force --server letsencrypt'

                    LitePagesLogger.writeforCLI(command, LitePagesLogger.INFO, 'VirtualHost.obtainSSLForADomain')

                    output = subprocess.check_output(shlex.split(command)).decode("utf-8")
                    LitePagesLogger.writeforCLI("Successfully obtained SSL for: " + self.domain, LitePagesLogger.INFO,
                                                'VirtualHost.obtainSSLForADomain')

                except subprocess.CalledProcessError:
                    LitePagesLogger.SendStatus('%s [CreateVHConfigFile.obtainSSLForADomain:402]' % (str(msg)),
                                               self.JobID, 1, 0, 0, 70)
                    LitePagesLogger.writeforCLI('Failed to obtain SSL, issuing self-signed SSL for: ' + self.domain,
                                                LitePagesLogger.ERROR, 'VirtualHost.obtainSSLForADomain')
                    return 0

            if output.find('Cert success') > -1:
                return 1
            else:
                return 0

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [CreateVHConfigFile.obtainSSLForADomain:414]' % (str(msg)), self.JobID, 1, 0,
                                       0, 70)
            LitePagesLogger.writeforCLI('%s [CreateVHConfigFile.obtainSSLForADomain:383]' % (str(msg)),
                                        LitePagesLogger.ERROR, 'VirtualHost.obtainSSLForADomain')
            return 0

    def installSSLForDomain(self):

        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:

            completePathToConfigFile = self.CompletePathToConfigFile

            try:
                map = "  map                     " + self.domain + " " + self.domain + "\n"

                if self.checkSSLListener() != 1:

                    writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'a')

                    listener = "listener SSL {" + "\n"
                    address = "  address                 *:443" + "\n"
                    secure = "  secure                  1" + "\n"
                    keyFile = "  keyFile                  /etc/letsencrypt/live/" + self.domain + "/privkey.pem\n"
                    certFile = "  certFile                 /etc/letsencrypt/live/" + self.domain + "/fullchain.pem\n"
                    certChain = "  certChain               1" + "\n"
                    sslProtocol = "  sslProtocol             24" + "\n"
                    ciphers = "  ciphers                 EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH:ECDHE-RSA-AES128-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA128:DHE-RSA-AES128-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA128:ECDHE-RSA-AES128-SHA384:ECDHE-RSA-AES128-SHA128:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA128:DHE-RSA-AES128-SHA128:DHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA384:AES128-GCM-SHA128:AES128-SHA128:AES128-SHA128:AES128-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4" + "\n"
                    enableECDHE = "  enableECDHE             1" + "\n"
                    renegProtection = "  renegProtection         1" + "\n"
                    sslSessionCache = "  sslSessionCache         1" + "\n"
                    enableSpdy = "  enableSpdy              15" + "\n"
                    enableStapling = "  enableStapling           1" + "\n"
                    ocspRespMaxAge = "  ocspRespMaxAge           86400" + "\n"
                    map = "  map                     " + self.domain + " " + self.domain + "\n"
                    final = "}" + "\n" + "\n"

                    writeDataToFile.writelines("\n")
                    writeDataToFile.writelines(listener)
                    writeDataToFile.writelines(address)
                    writeDataToFile.writelines(secure)
                    writeDataToFile.writelines(keyFile)
                    writeDataToFile.writelines(certFile)
                    writeDataToFile.writelines(certChain)
                    writeDataToFile.writelines(sslProtocol)
                    writeDataToFile.writelines(ciphers)
                    writeDataToFile.writelines(enableECDHE)
                    writeDataToFile.writelines(renegProtection)
                    writeDataToFile.writelines(sslSessionCache)
                    writeDataToFile.writelines(enableSpdy)
                    writeDataToFile.writelines(enableStapling)
                    writeDataToFile.writelines(ocspRespMaxAge)
                    writeDataToFile.writelines(map)
                    writeDataToFile.writelines(final)
                    writeDataToFile.writelines("\n")
                    writeDataToFile.close()


                else:

                    if self.checkIfSSLMap() == 0:

                        data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()
                        writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'w')
                        sslCheck = 0

                        for items in data:
                            if items.find("listener") > -1 and items.find("SSL") > -1:
                                sslCheck = 1

                            if (sslCheck == 1):
                                writeDataToFile.writelines(items)
                                writeDataToFile.writelines(map)
                                sslCheck = 0
                            else:
                                writeDataToFile.writelines(items)
                        writeDataToFile.close()

                    ###################### Write per host Configs for SSL ###################

                    data = open(completePathToConfigFile, "r").readlines()

                    ## check if vhssl is already in vhconf file

                    vhsslPresense = 0

                    for items in data:
                        if items.find("vhssl") > -1:
                            vhsslPresense = 1

                    if vhsslPresense == 0:
                        writeSSLConfig = open(completePathToConfigFile, "a")

                        vhssl = "vhssl  {" + "\n"
                        keyFile = "  keyFile                 /etc/letsencrypt/live/" + self.domain + "/privkey.pem\n"
                        certFile = "  certFile                /etc/letsencrypt/live/" + self.domain + "/fullchain.pem\n"
                        certChain = "  certChain               1" + "\n"
                        sslProtocol = "  sslProtocol             24" + "\n"
                        ciphers = "  ciphers                 EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH:ECDHE-RSA-AES128-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA128:DHE-RSA-AES128-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA128:ECDHE-RSA-AES128-SHA384:ECDHE-RSA-AES128-SHA128:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA128:DHE-RSA-AES128-SHA128:DHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA384:AES128-GCM-SHA128:AES128-SHA128:AES128-SHA128:AES128-SHA:AES128-SHA:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4" + "\n"
                        enableECDHE = "  enableECDHE             1" + "\n"
                        renegProtection = "  renegProtection         1" + "\n"
                        sslSessionCache = "  sslSessionCache         1" + "\n"
                        enableSpdy = "  enableSpdy              15" + "\n"
                        enableStapling = "  enableStapling           1" + "\n"
                        ocspRespMaxAge = "  ocspRespMaxAge           86400" + "\n"
                        final = "}"

                        writeSSLConfig.writelines("\n")

                        writeSSLConfig.writelines(vhssl)
                        writeSSLConfig.writelines(keyFile)
                        writeSSLConfig.writelines(certFile)
                        writeSSLConfig.writelines(certChain)
                        writeSSLConfig.writelines(sslProtocol)
                        writeSSLConfig.writelines(ciphers)
                        writeSSLConfig.writelines(enableECDHE)
                        writeSSLConfig.writelines(renegProtection)
                        writeSSLConfig.writelines(sslSessionCache)
                        writeSSLConfig.writelines(enableSpdy)
                        writeSSLConfig.writelines(enableStapling)
                        writeSSLConfig.writelines(ocspRespMaxAge)
                        writeSSLConfig.writelines(final)

                        writeSSLConfig.writelines("\n")

                        writeSSLConfig.close()

                return 1
            except BaseException as msg:
                LitePagesLogger.writeforCLI('%s' % (str(msg)), LitePagesLogger.ERROR, 'VirtualHost.installSSLForDomain')
                return 0
        else:
            completePathToConfigFile = self.CompletePathToConfigFile

            ## Check if SSL VirtualHost already exists

            data = open(completePathToConfigFile, 'r').readlines()

            for items in data:
                if items.find('*:443') > -1:
                    return 1

            try:

                DocumentRoot = f'    DocumentRoot {self.path}/public_html\n'

                data = open(completePathToConfigFile, 'r').readlines()
                phpHandler = ''

                for items in data:
                    if items.find('AddHandler') > -1 and items.find('php') > -1:
                        phpHandler = items
                        break

                confFile = open(completePathToConfigFile, 'a')

                cacheRoot = """    <IfModule LiteSpeed>
        CacheRoot lscache
        CacheLookup on
    </IfModule>
"""

                VirtualHost = '\n<VirtualHost *:443>\n\n'
                ServerName = '    ServerName ' + self.domain + '\n'
                ServerAlias = '    ServerAlias www.' + self.domain + '\n'
                ServerAdmin = '    ServerAdmin ' + self.email + '\n'
                SeexecUserGroup = '    SuexecUserGroup ' + self.VHUser + ' ' + self.VHUser + '\n'
                CustomLogCombined = '    CustomLog %s/logs/%s.access_log combined\n' % (self.path, self.domain)

                confFile.writelines(VirtualHost)
                confFile.writelines(ServerName)
                confFile.writelines(ServerAlias)
                confFile.writelines(ServerAdmin)
                confFile.writelines(SeexecUserGroup)
                confFile.writelines(DocumentRoot)
                confFile.writelines(CustomLogCombined)
                confFile.writelines(cacheRoot)

                SSLEngine = '    SSLEngine on\n'
                SSLVerifyClient = '    SSLVerifyClient none\n'
                SSLCertificateFile = '    SSLCertificateFile /etc/letsencrypt/live/' + self.domain + '/fullchain.pem\n'
                SSLCertificateKeyFile = '    SSLCertificateKeyFile /etc/letsencrypt/live/' + self.domain + '/privkey.pem\n'

                confFile.writelines(SSLEngine)
                confFile.writelines(SSLVerifyClient)
                confFile.writelines(SSLCertificateFile)
                confFile.writelines(SSLCertificateKeyFile)
                confFile.writelines(phpHandler)

                VirtualHostEnd = '</VirtualHost>\n'
                confFile.writelines(VirtualHostEnd)
                confFile.close()
                return 1
            except BaseException as msg:
                LitePagesLogger.writeforCLI('%s' % (str(msg)), LitePagesLogger.ERROR, 'VirtualHost.installSSLForDomain')
                return 0

    ### SSL Related ends here

    def issueSSLForDomain(self):
        try:
            if self.obtainSSLForADomain() == 1:
                if self.installSSLForDomain() == 1:
                    return 1
                else:
                    return 0
            else:

                pathToStoreSSLPrivKey = "/etc/letsencrypt/live/%s/privkey.pem" % (self.domain)
                pathToStoreSSLFullChain = "/etc/letsencrypt/live/%s/fullchain.pem" % (self.domain)

                command = 'openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" -keyout ' + pathToStoreSSLPrivKey + ' -out ' + pathToStoreSSLFullChain
                cmd = shlex.split(command)
                subprocess.call(cmd)

                if self.installSSLForDomain() == 1:
                    LitePagesLogger.writeforCLI("Self signed SSL issued for " + self.domain + ".", LitePagesLogger.INFO,
                                                'VirtualHost.issueSSLForDomain')
                    LitePagesLogger.SendStatus("Self signed SSL issued for " + self.domain + ".", self.JobID, 1, 0, 0,
                                               70)
                    return 1
                else:
                    return 0

        except BaseException as msg:
            LitePagesLogger.writeforCLI('%s [CreateVHConfigFile.issueSSLForDomain:601]' % (str(msg)),
                                        LitePagesLogger.ERROR, 'VirtualHost.issueSSLForDomain')
            LitePagesLogger.SendStatus('%s [CreateVHConfigFile.issueSSLForDomain:601]' % (str(msg)), self.JobID, 1, 0,
                                       0, 70)
            return 0

    def CreateVirtualHost(self):
        try:

            self.JobID = self.data['JobID']
            self.domain = self.data['domain']
            self.path = self.data['path'].rstrip('/')
            self.HTMLPath = '%s/public_html' % (self.path)
            self.PathLogs = '/usr/local/lsws/logs/%s' % (self.data['domain'])
            self.VHUser = self.data['VHUser']
            self.CompletePathToConfigFile = '%s/%s.conf' % (self.VhostPath, self.domain)
            self.PHPVersion = self.data['PHPVersion']
            self.email = self.data['email']
            self.OpenBaseDir = self.data['OpenBaseDir']
            self.ssl = self.data['ssl']
            try:
                self.PhpMyAdmin = self.data['PhpMyAdmin']
                self.Mautic = self.data['Mautic']
            except:
                self.PhpMyAdmin = False
                self.Mautic = False

            LitePagesLogger.writeToFile("Value of Mautic ...:%s" % self.Mautic, LitePagesLogger.ERROR, 0)

            ##

            LitePagesLogger.SendStatus('Creating user..', self.JobID, 1, 0, 0, 5)

            status, message = self.CreateUser()
            if status == 0:
                LitePagesLogger.SendStatus(message, self.JobID, 1, 1, 0, 0)
                return 0

            ###

            LitePagesLogger.SendStatus('Creating directories..', self.JobID, 1, 0, 0, 30)

            status, message = self.CreateDirectories()
            if status == 0:
                LitePagesLogger.SendStatus(message, self.JobID, 1, 1, 0, 0)
                return 0

            ##

            LitePagesLogger.SendStatus('Creating main config files..', self.JobID, 1, 0, 0, 60)

            status, message = self.CreateVHConfigFile()
            if status == 0:
                LitePagesLogger.SendStatus(message, self.JobID, 1, 1, 0, 0)
                return 0

            ##

            status, message = self.createConfigInMainVirtualHostFile()
            if status == 0:
                LitePagesLogger.SendStatus(message, self.JobID, 1, 1, 0, 0)
                return 0

            ### Restart Web server before ssl issue

            LitePagesExecutioner.RestartServer()

            ##

            if self.ssl == 1:
                LitePagesLogger.SendStatus('Obtaining ssl..', self.JobID, 1, 0, 0, 70)
                self.HTMLPath = '/usr/local/lsws/Example/html'
                self.issueSSLForDomain()
                self.HTMLPath = '%s/public_html' % (self.path)
                LitePagesExecutioner.RestartServer()

            if self.PhpMyAdmin == True:
                passdata = {
                    'VHUser': self.VHUser,
                    'path': self.path,
                    'JobID': self.JobID
                }
                from LPBackup import LPBackups
                obj = LPBackups(passdata)
                obj.DeployPhpmyadmin()

            if self.Mautic == True:
                self.MDBname = self.data['MDBname']
                self.MDBUser = self.data['MDBUser']
                self.MDBPW = self.data['MDBPW']
                self.MauticPassword = self.data['MauticPassword']
                self.MauticEmail = self.data['MauticEmail']

                pasata = {
                    'VHUser': self.VHUser,
                    'path': self.path,
                    'JobID': self.JobID,
                    'MDBname': self.MDBname,
                    'MDBUser': self.MDBUser,
                    'MDBPW': self.MDBPW,
                    'MauticPassword': self.MauticPassword,
                    'MauticEmail': self.MauticEmail,
                    'Domain': self.domain
                }
                from LPBackup import LPBackups
                obj = LPBackups(pasata)
                obj.DeployMautic()


            if self.data['WebServerType'] == '1':

                LitePagesLogger.SendStatus("Installing Apache, this may take some time.." , self.JobID, 1, 0, 0, 80)

                confPath = vhost.Server_root + "/conf/vhosts/" + self.data['domain']
                completePathToConfigFile = confPath

                if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:

                    if ApacheController.checkIfApacheInstalled() == 0:

                        result = ApacheController.setupApache(self.data['JobID'])
                        if result[0] == 0:
                            raise BaseException(result[1])

                        result = ApacheVhost.setupApacheVhost(self.data['UserEmail'], self.data['externalApp'],
                                                     self.data['externalApp'],
                                                     self.data['PHPVersion'], self.data['domain'], self.path)
                        if result[0] == 0:
                            raise BaseException(result[1])
                        else:
                            LitePagesLogger.writeforCLI(f'Complete path of config file when Apache is used {completePathToConfigFile}',
                                                        LitePagesLogger.ERROR, 'CreateVirtualHost')

                            ApacheVhost.perHostVirtualConfOLS(completePathToConfigFile, self.data['UserEmail'])
                            LitePagesLogger.SendStatus('Almost Done..', self.data['JobID'], 1, 0, 0,90)

                            lpe = LitePagesExecutioner('')

                            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                                lpe.command = "systemctl restart lsws"
                            else:
                                lpe.command = "/usr/local/lsws/bin/lswsctrl restart"

                            ReturnCode, stdout, stderr = lpe.Execute()
                            phpService = ApacheVhost.DecideFPMServiceName(self.data['PHPVersion'])

                            lpe.command = f"systemctl restart {phpService}"
                            ReturnCode, stdout, stderr = lpe.Execute()


            LitePagesLogger.SendStatus('Compeleted', self.data['JobID'], 1, 1, 1, 100)

            return 1


        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [CreateVHConfigFile.CreateVirtualHost:646]' % (str(msg)), self.JobID, 1, 1,
                                       0, 0)
            return 0

    ### Code for creating VHOST ends here

    ### Code for deleting VHOST

    def deleteCoreConf(self):
        try:

            if os.path.exists(self.path):
                shutil.rmtree(self.path)

            if os.path.exists(self.CompletePathToConfigFile):
                os.remove(self.CompletePathToConfigFile)

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                try:

                    data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()

                    writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'w')

                    check = 1
                    sslCheck = 1

                    for items in data:
                        if self.NumberOfSites == 1:
                            if (items.find(' ' + self.domain) > -1 and items.find(
                                    "  map                     " + self.domain) > -1):
                                continue
                            if (items.find(' ' + self.domain) > -1 and (
                                    items.find("virtualHost") > -1 or items.find("virtualhost") > -1)):
                                check = 0
                            if items.find("listener") > -1 and items.find("SSL") > -1:
                                sslCheck = 0
                            if (check == 1 and sslCheck == 1):
                                writeDataToFile.writelines(items)
                            if (items.find("}") > -1 and (check == 0 or sslCheck == 0)):
                                check = 1
                                sslCheck = 1
                        else:
                            if (items.find(' ' + self.domain) > -1 and items.find(
                                    "  map                     " + self.domain) > -1):
                                continue
                            if (items.find(' ' + self.domain) > -1 and (
                                    items.find("virtualHost") > -1 or items.find("virtualhost") > -1)):
                                check = 0
                            if (check == 1):
                                writeDataToFile.writelines(items)
                            if (items.find("}") > -1 and check == 0):
                                check = 1
                except BaseException as msg:
                    LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'VirtualHost.deleteCoreConf')
                    return 0, str(msg)
            else:
                try:
                    data = open("/usr/local/lsws/conf/httpd.conf").readlines()

                    writeDataToFile = open("/usr/local/lsws/conf/httpd.conf", 'w')

                    for items in data:
                        if items.find('/' + self.domain + '.conf') > -1:

                            pass
                        else:
                            writeDataToFile.writelines(items)

                    writeDataToFile.close()
                except BaseException as msg:
                    LitePagesLogger.writeforCLI('%s' % (str(msg)), LitePagesLogger.ERROR, 'VirtalHost.deleteCoreConf')
                    return 0, str(msg)
            return 1, None
        except BaseException as msg:
            LitePagesLogger.writeforCLI('%s' % (str(msg)), LitePagesLogger.ERROR, 'VirtalHost.deleteCoreConf')
            return 0, str(msg)

    def DeleteVirtualHost(self):

        self.domain = self.data['domain']
        self.CompletePathToConfigFile = '%s/%s.conf' % (self.VhostPath, self.domain)
        self.NumberOfSites = self.data['NumberOfSites']
        self.path = self.data['path']
        self.VHUser = self.data['VHUser']

        status, message = self.deleteCoreConf()

        if status == 0:
            return 0, message

        try:
            lpe = LitePagesExecutioner('')

            ##

            lpe.command = 'deluser %s' % (self.VHUser)
            lpe.Execute()

            #

            lpe.command = 'groupdel %s' % (self.VHUser)
            lpe.Execute()

            ### Delete Acme folder

            if os.path.exists('/root/.acme.sh/%s' % (self.domain)):
                shutil.rmtree('/root/.acme.sh/%s' % (self.domain))

            ApacheVhost.DeleteApacheVhost(self.domain)

            return 1, None

        except BaseException as msg:
            LitePagesLogger.writeforCLI('%s [CreateVHConfigFile.deleteCoreConf:753]' % (str(msg)),
                                        LitePagesLogger.ERROR, 'CreateVHConfigFile.deleteCoreConf')
            return 0, str(msg)

    ### PHP Functions

    def ChangeWebsitePHP(self):

        phpDetachUpdatePath = '%s/lsphp_restart.txt' % (self.data['path'])
        php = self.getPHPString(self.data['PHPVersion'])
        vhFile = '%s/%s.conf' % (VirtualHost.VhostConfPath, self.data['domain'])

        if not os.path.exists("/usr/local/lsws/lsphp" + str(php) + "/bin/lsphp"):
            return 0, 'This PHP version is not available on your CloudPages.'

        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            if ApacheVhost.changePHP(self.data['PHPVersion'], vhFile, self.data['path']) == 0:
                try:
                    data = open(vhFile, "r").readlines()

                    writeDataToFile = open(vhFile, "w")

                    path = "  path                    /usr/local/lsws/lsphp" + str(php) + "/bin/lsphp\n"

                    for items in data:
                        if items.find("/usr/local/lsws/lsphp") > -1 and items.find("path") > -1:
                            writeDataToFile.writelines(path)
                        else:
                            writeDataToFile.writelines(items)

                    writeDataToFile.close()

                    writeToFile = open(phpDetachUpdatePath, 'w')
                    writeToFile.close()

                    LitePagesExecutioner.RestartServer()
                    try:
                        os.remove(phpDetachUpdatePath)
                    except:
                        pass


                    return 1, 'None'
                except BaseException as msg:
                    return 0, str(msg)
            else:

                phpService = ApacheVhost.DecideFPMServiceName(self.data['PHPVersion'])

                lpe = LitePagesExecutioner('')

                lpe.command = f"systemctl restart {phpService}"
                ReturnCode, stdout, stderr = lpe.Execute()
        else:
            try:
                data = open(vhFile, "r").readlines()

                writeDataToFile = open(vhFile, "w")

                finalString = '    AddHandler application/x-httpd-php' + str(php) + ' .php\n'

                for items in data:
                    if items.find("AddHandler application/x-httpd") > -1:
                        writeDataToFile.writelines(finalString)
                    else:
                        writeDataToFile.writelines(items)

                writeDataToFile.close()

                writeToFile = open(phpDetachUpdatePath, 'w')
                writeToFile.close()

                LitePagesExecutioner.RestartServer()
                try:
                    os.remove(phpDetachUpdatePath)
                except:
                    pass

                return 1, 'None'
            except BaseException as msg:
                return 0, str(msg)

    ### Create SSH Key

    def FetchPublicKey(self):
        FinalKeyPath = '%s/.ssh/id_rsa' % (self.data['path'])

        lpe = LitePagesExecutioner('')
        if not os.path.exists(FinalKeyPath):

            ##

            lpe.command = "sudo -u %s ssh-keygen -f %s -t rsa -N ''" % (self.data['VHUser'], FinalKeyPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

        lpe.command = "sudo -u %s cat %s.pub" % (self.data['VHUser'], FinalKeyPath)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr
        else:
            return 1, stdout

    ### Change SSH Password

    def ChangeUserPasswordSSH(self):
        try:

            value = self.data['PasswordByPass']

            if value.find(';') > -1 or value.find('&&') > -1 or value.find('|') > -1 or value.find('...') > -1 \
                    or value.find("`") > -1 or value.find("$") > -1 or value.find("(") > -1 or value.find(")") > -1 \
                    or value.find("'") > -1 or value.find("[") > -1 or value.find("]") > -1 or value.find(
                "{") > -1 or value.find("}") > -1 \
                    or value.find(":") > -1 or value.find("<") > -1 or value.find(">") > -1:
                return 0, 'Password field can not contain some characters, following characters are not allowed in the input ` $ & ( ) [ ] { } ; : ‘ < >'

            lpe = LitePagesExecutioner('')

            lpe.shell = True
            lpe.command = "echo '%s:%s' | chpasswd" % (self.data['VHUser'], self.data['PasswordByPass'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, stdout
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def FetchUserSSHKeys(self):
        try:
            lpe = LitePagesExecutioner('')

            lpe.command = "sudo -u %s cat %s/.ssh/authorized_keys" % (self.data['VHUser'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()
            data = stdout.split('\n')

            json_data = "["
            checker = 0

            for items in data:
                if items.find("ssh-rsa") > -1:
                    keydata = items.split(" ")

                    try:
                        key = "ssh-rsa " + keydata[1][:50] + "  ..  " + keydata[2]
                        try:
                            userName = keydata[2][:keydata[2].index("@")]
                        except:
                            userName = keydata[2]
                    except:
                        key = "ssh-rsa " + keydata[1][:50]
                        userName = ''

                    dic = {
                        'userName': userName,
                        'key': key,
                    }

                    if checker == 0:
                        json_data = json_data + json.dumps(dic)
                        checker = 1
                    else:
                        json_data = json_data + ',' + json.dumps(dic)

            json_data = json_data + ']'

            return 1, json_data

        except BaseException as msg:
            return 0, str(msg)

    def AddUserPublicKey(self):
        try:

            lpe = LitePagesExecutioner('')

            lpe.command = "sudo -u %s mkdir %s/.ssh" % (self.data['VHUser'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = "sudo -u %s chmod 700 %s/.ssh" % (self.data['VHUser'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            pathToSSH = "%s/.ssh/authorized_keys" % (self.data['path'])

            if os.path.exists(pathToSSH):
                pass
            else:
                sshFile = open(pathToSSH, 'w')
                sshFile.writelines('###############')
                sshFile.close()

            lpe.command = "sudo -u %s chmod 600 %s" % (self.data['VHUser'], pathToSSH)
            ReturnCode, stdout, stderr = lpe.Execute()

            presenseCheck = 0
            try:
                data = open(pathToSSH, "r").readlines()
                for items in data:
                    if items.find(self.data['key']) > -1:
                        presenseCheck = 1
            except:
                pass

            if presenseCheck == 0:
                writeToFile = open(pathToSSH, 'a')
                writeToFile.writelines("\n")
                writeToFile.writelines(self.data['key'])
                writeToFile.writelines("\n")
                writeToFile.close()

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def DeleteUserPublicKey(self):
        try:
            keyPart = self.data['key'].split(" ")[1]
            pathToSSH = "%s/.ssh/authorized_keys" % (self.data['path'])

            data = open(pathToSSH, 'r').readlines()

            writeToFile = open(pathToSSH, "w")

            for items in data:
                if items.find("ssh-rsa") > -1 and items.find(keyPart) > -1:
                    continue
                else:
                    writeToFile.writelines(items)

            writeToFile.close()
            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    @staticmethod
    def FixPermissions(path, VHUser, jobid=None):
        lpe = LitePagesExecutioner('')

        ##

        lpe.command = 'chown -R %s:nogroup %s' % (VHUser, path)
        ReturnCode, stdout, stderr = lpe.Execute()

    ### Logs

    @staticmethod
    def GetLogs(fileName, page):
        try:

            if os.path.islink(fileName):
                print("0, %s file is symlinked." % (fileName))
                return 0

            lpe = LitePagesExecutioner('')

            ##

            lpe.command = 'sudo -u nobody wc -l %s' % (fileName)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                numberOfTotalLines = int(stdout.split(" ")[0])
            else:
                return 0, stderr

            if numberOfTotalLines < 25:
                lpe.command = 'sudo -u nobody cat %s' % (fileName)
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode:
                    data = stdout
                else:
                    return 0, stderr
            else:
                if page == 1:
                    end = numberOfTotalLines
                    start = end - 24
                    if start <= 0:
                        start = 1
                    startingAndEnding = "'" + str(start) + "," + str(end) + "p'"
                    lpe.command = "sudo -u nobody sed -n " + startingAndEnding + " " + fileName
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode:
                        data = stdout
                    else:
                        return 0, stderr
                else:
                    end = numberOfTotalLines - ((page - 1) * 25)
                    start = end - 24
                    if start <= 0:
                        start = 1
                    startingAndEnding = "'" + str(start) + "," + str(end) + "p'"
                    lpe.command = " sudo -u nobody sed -n " + startingAndEnding + " " + fileName
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode:
                        data = stdout
                    else:
                        return 0, stderr
            return 1, data
        except BaseException as msg:
            return 0, str(msg)

    def getDataFromLogFile(self):
        try:

            logType = self.data['logType']
            self.domain = self.data['domain']
            page = self.data['page']

            if logType == 1:
                fileName = '/usr/local/lsws/logs/%s/%s.access_log' % (self.domain, self.domain)
                if not os.path.exists(fileName):
                    fileName = '%s/logs/%s.access_log' % (self.data['path'], self.domain)
            else:
                fileName = '/usr/local/lsws/logs/%s/%s.error_log' % (self.domain, self.domain)
                if not os.path.exists(fileName):
                    fileName = '%s/logs/%s.error_log' % (self.data['path'], self.domain)

            output = VirtualHost.GetLogs(fileName, page)

            if output[0] == 0:
                return 0, output[1]

            ## get log ends here.

            if logType == 1:
                data = output[1].split("\n")

                json_data = "["
                checker = 0

                for items in reversed(data):
                    if len(items) > 10:
                        logData = items.split(" ")
                        domain = logData[5].strip('"')
                        ipAddress = logData[0].strip('"')
                        time = (logData[3]).strip("[").strip("]")
                        resource = logData[6].strip('"')
                        size = logData[9].replace('"', '')

                        dic = {'domain': domain,
                               'ipAddress': ipAddress,
                               'time': time,
                               'resource': resource,
                               'size': size,
                               }

                        if checker == 0:
                            json_data = json_data + json.dumps(dic)
                            checker = 1
                        else:
                            json_data = json_data + ',' + json.dumps(dic)

                json_data = json_data + ']'
                return 1, json_data
            else:
                return 1, output[1]

        except BaseException as msg:
            return 0, str(msg)

    ## Bubble Wrap

    def BubbleWrapStatus(self):

        vhFile = '%s/%s.conf' % (VirtualHost.VhostConfPath, self.data['domain'])
        AlreadyWritten = 0

        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            try:

                data = open(vhFile, "r").readlines()
                writeDataToFile = open(vhFile, "w")

                for items in data:
                    if items.find('bubbleWrap') > -1:
                        AlreadyWritten = 1
                        if self.data['BubbleWrap']:
                            writeDataToFile.writelines('bubbleWrap                2\n')
                        else:
                            writeDataToFile.writelines('bubbleWrap                1\n')
                    else:
                        writeDataToFile.writelines(items)

                writeDataToFile.close()

                ## If bubblewrap was not in config file then append to end of file

                if AlreadyWritten == 0:
                    writeDataToFile = open(vhFile, "a")
                    if self.data['BubbleWrap']:
                        writeDataToFile.writelines('bubbleWrap                2\n')
                    else:
                        writeDataToFile.writelines('bubbleWrap                1\n')

                    writeDataToFile.close()

                return 1, 'None'
            except BaseException as msg:
                return 0, str(msg)
        else:
            try:
                data = open(vhFile, "r").readlines()
                writeDataToFile = open(vhFile, "w")

                for items in data:
                    if items.find('BubbleWrap') > -1:
                        AlreadyWritten = 1
                        if self.data['BubbleWrap']:
                            writeDataToFile.writelines('        BubbleWrap on\n')
                        else:
                            writeDataToFile.writelines('        BubbleWrap off\n')
                    else:
                        writeDataToFile.writelines(items)

                writeDataToFile.close()

                ## If bubblewrap was not in config file then append to end of file

                if AlreadyWritten == 0:
                    writeDataToFile = open(vhFile, "w")
                    for items in data:
                        if items.find('IfModule LiteSpeed') > -1:
                            writeDataToFile.writelines(items)
                            if self.data['BubbleWrap']:
                                writeDataToFile.writelines('        BubbleWrap on\n')
                            else:
                                writeDataToFile.writelines('        BubbleWrap off\n')
                        else:
                            writeDataToFile.writelines(items)
                    writeDataToFile.close()

                return 1, 'None'
            except BaseException as msg:
                return 0, str(msg)

    ## reCaptchs

    def reCAPTCHASettings(self):

        vhFile = '%s/%s.conf' % (VirtualHost.VhostConfPath, self.data['domain'])
        AlreadyWritten = 0

        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            try:

                data = open(vhFile, 'r').readlines()

                FoundCheck = 0
                writeDataToFile = open(vhFile, "w")
                for items in data:
                    if items.find('lsrecaptcha') > -1:
                        FoundCheck = 1
                    elif FoundCheck and items.find('}') > -1:
                        FoundCheck = 0
                    elif FoundCheck == 0:
                        writeDataToFile.writelines(items)

                ## Write Final Settings
                reCAPTCHA = '0'
                if self.data['reCAPTCHA']:
                    reCAPTCHA = '1'

                reCAPTCHAType = '1'
                if self.data['reCAPTCHAType'] == 'Checkbox':
                    reCAPTCHAType = '1'
                else:
                    reCAPTCHAType = '2'

                content = """
lsrecaptcha  {
  enabled                 %s
  type                    %s
}
""" % (reCAPTCHA, reCAPTCHAType)

                writeDataToFile = open(vhFile, "a")
                writeDataToFile.write(content)
                writeDataToFile.close()

                return 1, 'None'
            except BaseException as msg:
                return 0, str(msg)
        else:
            try:

                data = open(vhFile, "r").readlines()
                writeDataToFile = open(vhFile, "w")

                for items in data:
                    if items.find('LsRecaptcha') > -1:
                        AlreadyWritten = 1
                        if self.data['reCAPTCHA']:
                            writeDataToFile.writelines('        LsRecaptcha 30\n')
                        else:
                            writeDataToFile.writelines('        LsRecaptcha 0\n')
                    else:
                        writeDataToFile.writelines(items)
                writeDataToFile.close()

                ## If LsRecaptcha was not in config file then append to end of file

                if AlreadyWritten == 0:
                    writeDataToFile = open(vhFile, "w")
                    for items in data:
                        if items.find('IfModule LiteSpeed') > -1:
                            writeDataToFile.writelines(items)
                            if self.data['reCAPTCHA']:
                                writeDataToFile.writelines('        LsRecaptcha 30\n')
                            else:
                                writeDataToFile.writelines('        LsRecaptcha 0\n')
                        else:
                            writeDataToFile.writelines(items)

                    writeDataToFile.close()

                return 1, 'None'
            except BaseException as msg:
                return 0, str(msg)

    def FetchCurrentLSConfig(self):
        vhFile = '%s/%s.conf' % (VirtualHost.VhostConfPath, self.data['domain'])
        lpe = LitePagesExecutioner('')

        ##

        lpe.command = 'cat %s' % (vhFile)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode:
            return 1, stdout
        else:
            return 0, stderr

    def SaveLSConfig(self):
        try:
            vhFile = '%s/%s.conf' % (VirtualHost.VhostConfPath, self.data['domain'])

            WriteToFile = open(vhFile, 'w')
            WriteToFile.write(self.data['conf'])
            WriteToFile.close()

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def FetchCurrentRewriteRules(self):
        path = '%s/public_html/.htaccess' % (self.data['path'])
        lpe = LitePagesExecutioner('')

        ##

        lpe.command = 'cat %s' % (path)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode:
            return 1, stdout
        else:
            return 0, 'Emtpy file'

    def SaveRewriteRules(self):
        try:
            path = '%s/public_html/.htaccess' % (self.data['path'])

            WriteToFile = open(path, 'w')
            WriteToFile.write(self.data['rules'])
            WriteToFile.close()

            ### Restart Web server before ssl issue

            LitePagesExecutioner.RestartServer()

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def FetchSSLDetails(self):
        try:

            data = {}
            lpe = LitePagesExecutioner('')

            ##

            lpe.command = 'openssl x509 -enddate -noout -in /etc/letsencrypt/live/%s/fullchain.pem' % (
                self.data['domain'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                data['SSLExpireDate'] = stdout.replace('notAfter=', '')
            else:
                return 0, 'SSL Does not exist'

            lpe.command = 'openssl x509 -issuer -noout -in /etc/letsencrypt/live/%s/fullchain.pem' % (
                self.data['domain'])

            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                data['SSLProvider'] = stdout
            else:
                return 0, stderr

            lpe.command = 'cat /etc/letsencrypt/live/%s/fullchain.pem' % (self.data['domain'])

            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                data['Certificate'] = stdout
            else:
                return 0, stderr

            lpe.command = 'cat /etc/letsencrypt/live/%s/privkey.pem' % (self.data['domain'])

            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                data['Key'] = stdout
            else:
                return 0, stderr

            return 1, data

        except BaseException as msg:
            return 0, str(msg)

    def SaveCustomSSL(self):
        try:

            CertificatePath = '/etc/letsencrypt/live/%s/fullchain.pem' % (self.data['domain'])
            KeyPath = '/etc/letsencrypt/live/%s/privkey.pem' % (self.data['domain'])

            WriteToFile = open(CertificatePath, 'w')
            WriteToFile.write(self.data['Certificate'])
            WriteToFile.close()

            WriteToFile = open(KeyPath, 'w')
            WriteToFile.write(self.data['Key'])
            WriteToFile.close()

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def IssueFreeSSL(self):

        self.JobID = self.data['JobID']

        LitePagesLogger.SendStatus('Issuing SSL certificate for %s..' % (self.data['domain']), self.JobID, 1, 0, 0, 10)
        self.domain = self.data['domain']
        self.path = self.data['path'].rstrip('/')

        self.HTMLPath = '%s/public_html' % (self.path)
        self.HTMLPath = '/usr/local/lsws/Example/html'
        self.PathLogs = '/usr/local/lsws/logs/%s' % (self.data['domain'])
        self.VHUser = self.data['VHUser']
        self.CompletePathToConfigFile = '%s/%s.conf' % (self.VhostPath, self.domain)
        self.PHPVersion = self.data['PHPVersion']
        self.email = self.data['email']

        self.issueSSLForDomain()
        LitePagesExecutioner.RestartServer()

        LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

    def FetchPHPDetails(self):
        try:
            phpVers = self.data['PHPVersion']

            phpVers = "php" + VirtualHost.getPHPString(phpVers)

            initial = phpVers[3]
            final = phpVers[4]

            completeName = str(initial) + '.' + str(final)
            path = "/usr/local/lsws/ls" + phpVers + "/etc/php/" + completeName + "/litespeed/php.ini"

            allow_url_fopen = "0"
            display_errors = "0"
            file_uploads = "0"
            allow_url_include = "0"
            memory_limit = ""
            max_execution_time = ""
            upload_max_filesize = ""
            max_input_time = ""
            post_max_size = ""

            data = open(path, 'r').read().split('\n')

            for items in data:
                if items.find("allow_url_fopen") > -1 and items.find("=") > -1:
                    if items.find("On") > -1:
                        allow_url_fopen = "1"
                if items.find("display_errors") > -1 and items.find("=") > -1:
                    if items.find("On") > -1:
                        display_errors = "1"
                if items.find("file_uploads") > -1 and items.find("=") > -1:
                    if items.find("On") > -1:
                        file_uploads = "1"
                if items.find("allow_url_include") > -1 and items.find("=") > -1:
                    if items.find("On") > -1:
                        allow_url_include = "1"
                if items.find("memory_limit") > -1 and items.find("=") > -1:
                    memory_limit = re.findall(r"[A-Za-z0-9_]+", items)[1]
                if items.find("max_execution_time") > -1 and items.find("=") > -1:
                    max_execution_time = re.findall(r"[A-Za-z0-9_]+", items)[1]
                if items.find("upload_max_filesize") > -1 and items.find("=") > -1:
                    upload_max_filesize = re.findall(r"[A-Za-z0-9_]+", items)[1]
                if items.find("max_input_time") > -1 and items.find("=") > -1:
                    max_input_time = re.findall(r"[A-Za-z0-9_]+", items)[1]
                if items.find("post_max_size") > -1 and items.find("=") > -1:
                    post_max_size = re.findall(r"[A-Za-z0-9_]+", items)[1]

            final_dic = {
                'allow_url_fopen': allow_url_fopen,
                'display_errors': display_errors,
                'file_uploads': file_uploads,
                'allow_url_include': allow_url_include,
                'memory_limit': memory_limit,
                'max_execution_time': max_execution_time,
                'upload_max_filesize': upload_max_filesize,
                'max_input_time': max_input_time,
                'post_max_size': post_max_size
            }

            return 1, final_dic
        except BaseException as msg:
            return 0, str(msg)

    def savePHPConfigBasic(self):
        try:

            phpVers = self.data['PHPVersion']
            allow_url_fopen = self.data['allow_url_fopen']
            display_errors = self.data['display_errors']
            file_uploads = self.data['file_uploads']
            allow_url_include = self.data['allow_url_include']
            memory_limit = self.data['memory_limit']
            max_execution_time = self.data['max_execution_time']
            upload_max_filesize = self.data['upload_max_filesize']
            max_input_time = self.data['max_input_time']
            post_max_size = self.data['post_max_size']

            if allow_url_fopen == True:
                allow_url_fopen = "allow_url_fopen = On"
            else:
                allow_url_fopen = "allow_url_fopen = Off"

            if display_errors == True:
                display_errors = "display_errors = On"
            else:
                display_errors = "display_errors = Off"

            if file_uploads == True:
                file_uploads = "file_uploads = On"
            else:
                file_uploads = "file_uploads = Off"

            if allow_url_include == True:
                allow_url_include = "allow_url_include = On"
            else:
                allow_url_include = "allow_url_include = Off"

            lpe = LitePagesExecutioner('')
            serverLevelPHPRestart = '/usr/local/lsws/admin/tmp/.lsphp_restart.txt'
            lpe.command = 'touch %s' % (serverLevelPHPRestart)
            lpe.Execute()

            phpVers = "php" + VirtualHost.getPHPString(phpVers)

            initial = phpVers[3]
            final = phpVers[4]

            completeName = str(initial) + '.' + str(final)
            path = "/usr/local/lsws/ls" + phpVers + "/etc/php/" + completeName + "/litespeed/php.ini"

            data = open(path, 'r').readlines()

            writeToFile = open(path, 'w')

            for items in data:
                if items.find("allow_url_fopen") > -1 and items.find("=") > -1:
                    writeToFile.writelines(allow_url_fopen + "\n")
                elif items.find("display_errors") > -1 and items.find("=") > -1:
                    writeToFile.writelines(display_errors + "\n")
                elif items.find("file_uploads") > -1 and items.find("=") > -1 and not items.find(
                        "max_file_uploads") > -1:
                    writeToFile.writelines(file_uploads + "\n")
                elif items.find("allow_url_include") > -1 and items.find("=") > -1:
                    writeToFile.writelines(allow_url_include + "\n")

                elif items.find("memory_limit") > -1 and items.find("=") > -1:
                    writeToFile.writelines("memory_limit = " + memory_limit + "\n")

                elif items.find("max_execution_time") > -1 and items.find("=") > -1:
                    writeToFile.writelines("max_execution_time = " + max_execution_time + "\n")

                elif items.find("upload_max_filesize") > -1 and items.find("=") > -1:
                    writeToFile.writelines("upload_max_filesize = " + upload_max_filesize + "\n")

                elif items.find("max_input_time") > -1 and items.find("=") > -1:
                    writeToFile.writelines("max_input_time = " + max_input_time + "\n")
                elif items.find("post_max_size") > -1 and items.find("=") > -1:
                    writeToFile.writelines("post_max_size = " + post_max_size + "\n")
                else:
                    writeToFile.writelines(items)

            writeToFile.close()

            LitePagesExecutioner.RestartServer()
            LitePagesExecutioner.RestartServerLSPHP()

            return 1, None

        except BaseException as msg:
            return 0, (str(msg))

    def FetchPHPDetailsAdvanced(self):
        try:
            phpVers = self.data['PHPVersion']

            phpVers = "php" + VirtualHost.getPHPString(phpVers)

            initial = phpVers[3]
            final = phpVers[4]

            completeName = str(initial) + '.' + str(final)
            path = "/usr/local/lsws/ls" + phpVers + "/etc/php/" + completeName + "/litespeed/php.ini"
            return 1, open(path, 'r').read()
        except BaseException as msg:
            return 0, str(msg)

    def savePHPConfigAdvanced(self):
        try:
            phpVers = self.data['PHPVersion']

            phpVers = "php" + VirtualHost.getPHPString(phpVers)

            initial = phpVers[3]
            final = phpVers[4]

            completeName = str(initial) + '.' + str(final)
            path = "/usr/local/lsws/ls" + phpVers + "/etc/php/" + completeName + "/litespeed/php.ini"

            WriteToFile = open(path, 'w')
            WriteToFile.write(self.data['PHPConfigAdvanced'])
            WriteToFile.close()

            LitePagesExecutioner.RestartServerLSPHP()
            LitePagesExecutioner.RestartServer()

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def FetchLiteSpeedSettingsSL(self):
        try:

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                path = '/usr/local/lsws/conf/httpd_config.conf'
            else:
                path = '/usr/local/lsws/conf/httpd.conf'

            return 1, open(path, 'r').read()
        except BaseException as msg:
            return 0, str(msg)

    def SaveLSSettingsSL(self):
        try:

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                path = '/usr/local/lsws/conf/httpd_config.conf'
            else:
                path = '/usr/local/lsws/conf/httpd_config.conf'

            WriteToFile = open(path, 'w')
            WriteToFile.write(self.data['LiteSpeedConfigurationsSL'])

            LitePagesExecutioner.RestartServer()

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def SetValueOfProxyHeader(self, value):
        try:
            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                path = '/usr/local/lsws/conf/httpd_config.conf'

                #### Check if proxy header already presents

                ProxyHeaderPresent = 0

                data = open(path, 'r').readlines()

                WriteToFile = open(path, 'w')

                for items in data:
                    if items.find('useIpInProxyHeader') > -1:
                        ProxyHeaderPresent = 1
                        content = 'useIpInProxyHeader        %s\n' % (str(value))
                        WriteToFile.write(content)
                    else:
                        WriteToFile.write(items)

                WriteToFile.close()

                ### If proxy header was not already present

                if ProxyHeaderPresent == 0:
                    WriteToFile = open(path, 'w')

                    for items in data:
                        if items.find('showVersionNumber') > -1:
                            WriteToFile.write(items)
                            content = 'useIpInProxyHeader        %s\n' % (str(value))
                            WriteToFile.write(content)
                        else:
                            WriteToFile.write(items)
                    WriteToFile.close()
                return 1, None

            else:
                path = '/usr/local/lsws/conf/httpd_config.xml'

                ### If proxy header was not already present

                data = open(path, 'r').readlines()

                WriteToFile = open(path, 'w')

                for items in data:
                    if items.find('useIpInProxyHeader') > -1:
                        content = '  <useIpInProxyHeader>%s</useIpInProxyHeader>\n' % (str(value))
                        WriteToFile.write(content)
                    else:
                        WriteToFile.write(items)
                WriteToFile.close()

                return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def SetValueOfAllowHeader(self):
        try:

            ## Get Cloudflare IPs

            import requests
            response = requests.get('https://www.cloudflare.com/ips-v4')

            if response.status_code == 200:
                result = response.text
                ips = result.split('\n')

                finalIPs = ''

                for ip in ips:
                    finalIPs = '%s, %sT' % (finalIPs, ip)
            else:
                return 0, response.text

            #####

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                path = '/usr/local/lsws/conf/httpd_config.conf'

                data = open(path, 'r').readlines()
                WriteToFile = open(path, 'w')

                for items in data:
                    if items.find('allow') > -1 and items.find('ALL') > -1:
                        content = '  allow                   ALL %s\n' % (finalIPs)
                        WriteToFile.write(content)
                    else:
                        WriteToFile.write(items)

                WriteToFile.close()

                return 1, None
            else:
                path = '/usr/local/lsws/conf/httpd.conf'
                return 1, None


        except BaseException as msg:
            return 0, str(msg)

    def ShowRealVisitorIP(self):
        try:
            ## 2 means Trusted IP only.
            status, message = self.SetValueOfProxyHeader(2)

            if status == 0:
                return 0, message

            ## 2 means Trusted IP only.
            status, message = self.SetValueOfAllowHeader()

            if status == 0:
                return 0, message

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def IssueSSLv2(self):
        try:
            demomsg = " SSL Vs Issued Done"
            import sslv2
            virtualHost = self.data['VHUser']
            adminEmail = self.data['email']
            path = self.data['path']
            owner = self.data['owner']
            domain = self.data['domain']

            retValues = sslv2.issueSSLForDomain(domain, adminEmail, path, owner)

            if retValues[0] == 0:
                print("0," + str(retValues[1]))
                # logging.CyberCPLogFileWriter.writeToFile(str(retValues[1]))
                return 0, str(retValues[1])

            # installUtilities.installUtilities.reStartLiteSpeed()

            return 1, retValues[1]
        except BaseException as msg:
            return 0, str(msg)

    def CloudflareForSSL(self):
        try:
            SAVED_CF_Email = self.data['Clouflare_Email']
            SAVED_CF_Key = self.data['API_Token']

            lpe = LitePagesExecutioner('')
            ## remove existing keys first

            if os.path.exists('/root/.acme.sh'):
                path = '/root/.acme.sh/account.conf'
            else:
                path = '/.acme.sh/account.conf'
            lpe.command = f"sed -i '/SAVED_CF_Key/d;/SAVED_CF_Email/d' {path}"
            ReturnCode, stdout, stderr = lpe.Execute()

            CFContent = f"""
SAVED_CF_Key='{SAVED_CF_Key}'
SAVED_CF_Email='{SAVED_CF_Email}'
"""
            lpe.shell = True
            lpe.command = f'echo "{CFContent}" >> {path}'
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False

            return 1, "Operation Done Successfully"
        except BaseException as msg:
            return 0, str(msg)

    def GetApacheStatus(self):
        try:
            self.domain = self.data['domain']
            path = self.data['path']

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                finalConfPath = ApacheVhost.configBasePath + self.domain + '.conf'

                if os.path.exists(finalConfPath):

                    lpe = LitePagesExecutioner('')
                    LitePagesLogger.writeToFile('IN IF condition OLS Domain is ' + self.domain, LitePagesLogger.ERROR,
                                                0)
                    phpPath = ApacheVhost.whichPHPExists(self.domain)

                    lpe.command = 'sudo cat ' + phpPath

                    ReturnCode, stdout, stderr = lpe.Execute()
                    LitePagesLogger.writeToFile(
                        'IN IF condition OLS.... output od forst command' + ",,,,,,,,," + stdout + "......" + stderr,
                        LitePagesLogger.ERROR, 0)
                    phpConf = stdout.splitlines()
                    # LitePagesLogger.writeToFile('IN IF condition OLS....... PHP CONF ' + str(phpConf), LitePagesLogger.ERROR, 0)

                    pmMaxChildren = phpConf[8].split(' ')[2]
                    pmStartServers = phpConf[9].split(' ')[2]
                    pmMinSpareServers = phpConf[10].split(' ')[2]
                    pmMaxSpareServers = phpConf[11].split(' ')[2]

                    data = {}
                    data['status'] = 1

                    data['server'] = VirtualHost.apache
                    data['pmMaxChildren'] = pmMaxChildren
                    data['pmStartServers'] = pmStartServers
                    data['pmMinSpareServers'] = pmMinSpareServers
                    data['pmMaxSpareServers'] = pmMaxSpareServers
                    data['phpPath'] = phpPath
                    LitePagesLogger.writeToFile('IN IF condition OLS.......', LitePagesLogger.ERROR, 0)
                    lpe.command = f'cat {finalConfPath}'
                    ReturnCode, stdout, stderr = lpe.Execute()
                    data['configData'] = stdout
                else:
                    data = {}
                    data['status'] = 1
                    data['server'] = VirtualHost.ols

            else:
                data = {}
                data['status'] = 1
                data['server'] = VirtualHost.lsws

            return 1, data
        except BaseException as msg:
            return 0, str(msg)

    def switchServer(self):
        try:
            domain = self.data['domain']
            path = self.data['path']
            phpVersion = self.data['phpVersion']

            WebServerType = self.data['WebServerType']

            LitePagesLogger.writeToFile('Switch server function start', LitePagesLogger.ERROR, 0)
            confPath = vhost.Server_root + "/conf/vhosts/" + self.data['domain']
            completePathToConfigFile = confPath

            if WebServerType == VirtualHost.apache:

                if os.path.exists(completePathToConfigFile):
                    os.remove(completePathToConfigFile)

                if ApacheController.checkIfApacheInstalled() == 0:
                    result = ApacheController.setupApache(self.data['JobID'])
                    if result[0] == 0:
                        raise BaseException(result[1])

                ApacheVhost.setupApacheVhost(self.data['UserEmail'], self.data['externalApp'], self.data['externalApp'],
                                             self.data['phpVersion'], self.data['domain'], self.data['path'])
                if result[0] == 0:
                    raise BaseException(result[1])

                LitePagesLogger.SendStatus('Switching OpenLiteSpeed  to Apache', self.data['JobID'], 1, 0, 0, 50)

                LitePagesLogger.writeforCLI(
                    f'Complete path of config file when Apache is used during switch {completePathToConfigFile}',
                    LitePagesLogger.ERROR, 'CreateVirtualHost')

                result = ApacheVhost.perHostVirtualConfOLS(completePathToConfigFile, self.data['UserEmail'])
                if result[0] == 0:
                    raise BaseException(result[1])


                # logging.CyberCPLogFileWriter.statusWriter(tempStatusPath, 'Restarting servers and phps..,90')
                LitePagesLogger.SendStatus('Switching OpenLiteSpeed  to Apache', self.data['JobID'], 1, 0, 0, 70)

                phpversion = phpVersion.split()
                php = phpversion[1].replace(".", "")

                phpService = ApacheVhost.DecideFPMServiceName(phpVersion)


                lpe = LitePagesExecutioner('')

                lpe.command = f"systemctl stop {phpService}"
                ReturnCode, stdout, stderr = lpe.Execute()

                lpe.command = f"systemctl restart {phpService}"
                ReturnCode, stdout, stderr = lpe.Execute()

                lpe.command = f"systemctl restart {ApacheVhost.serviceName}"
                ReturnCode, stdout, stderr = lpe.Execute()

                ###

                # replace this ;ine with below code to restart server
                # installUtilities.installUtilities.reStartLiteSpeed()
                lpe = LitePagesExecutioner('')

                if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                    lpe.command = "systemctl restart lsws"
                else:
                    lpe.command = "/usr/local/lsws/bin/lswsctrl restart"

                ReturnCode, stdout, stderr = lpe.Execute()

                # logging.CyberCPLogFileWriter.statusWriter(tempStatusPath, 'Successfully converted.[200]')
                # LitePagesLogger.SendStatus('Switching OpenLiteSpeed  to Apache', self.data['JobID'], 1, 0, 0, 50)
                time.sleep(3)
                LitePagesLogger.SendStatus('Compeleted', self.data['JobID'], 1, 1, 1, 100)
                return 1, None
            else:
                # logging.CyberCPLogFileWriter.statusWriter(tempStatusPath, 'Starting Conversion..,0')
                ApacheVhost.DeleteApacheVhost(self.data['domain'])

                vhost.perHostVirtualConf(completePathToConfigFile+'.conf', self.data['UserEmail'], self.data['externalApp'],
                                         phpVersion, self.data['domain'], 0,)

                # logging.CyberCPLogFileWriter.statusWriter(tempStatusPath, 'Restarting server..,90')
                # installUtilities.installUtilities.reStartLiteSpeed()

                lpe = LitePagesExecutioner('')
                LitePagesLogger.SendStatus('Switching Apache to Openlitespeed', self.data['JobID'], 1, 0, 0, 50)

                if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
                    lpe.command = "systemctl restart lsws"
                else:
                    lpe.command = "/usr/local/lsws/bin/lswsctrl restart"

                ReturnCode, stdout, stderr = lpe.Execute()
                # logging.CyberCPLogFileWriter.statusWriter(tempStatusPath, 'Successfully converted. [200]')

                LitePagesLogger.SendStatus('Compeleted', self.data['JobID'], 1, 1, 1, 100)
                return 1, None

            # lpe = LitePagesExecutioner('')
            #
            # lpe.command = "ls"
            # ReturnCode, stdout, stderr = lpe.Execute()
            # if ReturnCode == 0:
            #     LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
            #     return 0, stderr
            #
            # LitePagesLogger.SendStatus('Switching Server Done', self.JobID, 1, 0, 0, 80)
            #
            # time.sleep(3)
            #
            # LitePagesLogger.SendStatus('Compeleted', self.JobID, 1, 1, 1, 100)

            return 1, None

        except BaseException as msg:
            LitePagesLogger.SendStatus(str(msg), self.data['JobID'], 1, 1, 0, 0)
            return 0, str(msg)

    def saveApacheConfig(self):
        try:
            domain = self.data['domain']

            configData = self.data['configData']
            filepath = ApacheVhost.configBasePath + self.data['domain'] + '.conf'
            vhost = open(filepath, "w")
            vhost.write(configData)
            vhost.close()

            lpe = LitePagesExecutioner('')
            lpe.command = f"systemctl restart {ApacheVhost.serviceName}"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, stdout
            else:
                return 0, stderr


        except BaseException as msg:
            return 0, str(msg)


    # def tuneApacheSettings(self):
    #     try:
    #         domainName = self.data['domain']
    #
    #         pmMaxChildren = self.data['pmMaxChildren']
    #         pmStartServers = self.data['pmStartServers']
    #         pmMinSpareServers = self.data['pmMinSpareServers']
    #         pmMaxSpareServers = self.data['pmMaxSpareServers']
    #         path = self.data['path']
    #         externalApp = self.data['externalApp']
    #
    #
    #         lpe = LitePagesExecutioner('')
    #         # lpe.command = f"systemctl restart {ApacheVhost.serviceName}"
    #         # ReturnCode, stdout, stderr = lpe.Execute()
    #         #
    #         # if ReturnCode:
    #         #     return 1, stdout
    #         # else:
    #         #     return 0, stderr
    #
    #         tempStatusPath = "/home/cyberpanel/" + str(randint(1000, 9999))
    #
    #
    #         sockPath = '/var/run/php/'
    #         group = 'nogroup'
    #
    #         phpFPMConf = vhostConfs.phpFpmPoolReplace
    #         phpFPMConf = phpFPMConf.replace('{externalApp}', externalApp)
    #         phpFPMConf = phpFPMConf.replace('{pmMaxChildren}', pmMaxChildren)
    #         phpFPMConf = phpFPMConf.replace('{pmStartServers}', pmStartServers)
    #         phpFPMConf = phpFPMConf.replace('{pmMinSpareServers}', pmMinSpareServers)
    #         phpFPMConf = phpFPMConf.replace('{pmMaxSpareServers}', pmMaxSpareServers)
    #         phpFPMConf = phpFPMConf.replace('{www}', "".join(re.findall("[a-zA-Z]+", domainName))[:7])
    #         phpFPMConf = phpFPMConf.replace('{Sock}', domainName)
    #         phpFPMConf = phpFPMConf.replace('{sockPath}', sockPath)
    #         phpFPMConf = phpFPMConf.replace('{group}', group)
    #
    #         writeToFile = open(tempStatusPath, 'w')
    #         writeToFile.writelines(phpFPMConf)
    #         writeToFile.close()
    #
    #         lpe.command = 'sudo mv %s %s' % (tempStatusPath, phpPath)
    #         ReturnCode, stdout, stderr = lpe.Execute()
    #
    #         phpPath = phpPath.split('/')
    #
    #         if os.path.exists(ProcessUtilities.debugPath):
    #             LitePagesLogger.writeforCLI(f'PHP path in tune settings {phpPath}')
    #
    #
    #         if ProcessUtilities.decideDistro() == ProcessUtilities.centos or ProcessUtilities.decideDistro() == ProcessUtilities.cent8:
    #             if phpPath[1] == 'etc':
    #                 phpVersion = phpPath[4][3] + phpPath[4][4]
    #                 phpVersion = f'PHP {phpPath[4][3]}.{phpPath[4][4]}'
    #             else:
    #                 phpVersion = phpPath[3][3] + phpPath[3][4]
    #                 phpVersion = f'PHP {phpPath[3][3]}.{phpPath[3][4]}'
    #         else:
    #             phpVersion = f'PHP {phpPath[2]}'
    #
    #         # php = PHPManager.getPHPString(phpVersion)
    #
    #         if os.path.exists(ProcessUtilities.debugPath):
    #             LitePagesLogger.writeforCLI(f'PHP Version in tune settings {phpVersion}')
    #
    #         phpService = ApacheVhost.DecideFPMServiceName(phpVersion)
    #
    #         if os.path.exists(ProcessUtilities.debugPath):
    #             LitePagesLogger.writeforCLI(f'PHP service in tune settings {phpService}')
    #
    #         lpe.command = f"systemctl stop {phpService}"
    #         ReturnCode, stdout, stderr = lpe.Execute()
    #
    #         lpe.command = f"systemctl restart {phpService}"
    #         ReturnCode, stdout, stderr = lpe.Execute()
    #
    #         data_ret = {'status': 1}
    #
    #         return 1, stdout
    #     except BaseException as msg:
    #
    #         return 0, str(msg)



