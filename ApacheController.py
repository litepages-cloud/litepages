#!/usr/local/CyberCP/bin/python
import os
import subprocess
import shlex

from ApacheVhost import ApacheVhost
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger


class ApacheController:
    php54Path = '/etc/php/5.4/fpm/pool.d/'
    php55Path = '/etc/php/5.5/fpm/pool.d/'
    php56Path = '/etc/php/5.6/fpm/pool.d/'
    php70Path = '/etc/php/7.0/fpm/pool.d/'
    php71Path = '/etc/php/7.1/fpm/pool.d/'
    php72Path = '/etc/php/7.2/fpm/pool.d/'
    php73Path = '/etc/php/7.3/fpm/pool.d/'
    php74Path = '/etc/php/7.4/fpm/pool.d/'
    php80Path = '/etc/php/8.0/fpm/pool.d/'
    php81Path = '/etc/php/8.1/fpm/pool.d/'
    php82Path = '/etc/php/8.2/fpm/pool.d/'
    apacheInstallStatusPath = '/home/cyberpanel/apacheInstallStatus'
    serverRootPath = '/etc/httpd'
    mpmConfigs = """# Select the MPM module which should be used by uncommenting exactly
# one of the following LoadModule lines:

# prefork MPM: Implements a non-threaded, pre-forking web server
# See: http://httpd.apache.org/docs/2.4/mod/prefork.html
#LoadModule mpm_prefork_module modules/mod_mpm_prefork.so

# worker MPM: Multi-Processing Module implementing a hybrid
# multi-threaded multi-process web server
# See: http://httpd.apache.org/docs/2.4/mod/worker.html
#
#LoadModule mpm_worker_module modules/mod_mpm_worker.so

# event MPM: A variant of the worker MPM with the goal of consuming
# threads only for connections with active processing
# See: http://httpd.apache.org/docs/2.4/mod/event.html
#
LoadModule mpm_event_module modules/mod_mpm_event.so

<IfModule mpm_event_module>
    StartServers 2
    MinSpareThreads          25
    MaxSpareThreads          75
    ThreadLimit                      64
    ThreadsPerChild          25
    MaxRequestWorkers    30
    MaxConnectionsPerChild    1000
</IfModule>"""
    mpmConfigsPath = "/etc/httpd/conf.modules.d/00-mpm.conf"

    @staticmethod
    def checkIfApacheInstalled():
        try:
            if os.path.exists(ApacheController.serverRootPath):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php54Path):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php55Path):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php56Path):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php70Path):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php71Path):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php72Path):
                pass
            else:
                return 0

            if os.path.exists(ApacheVhost.php73Path):
                return 1
            else:
                return 0
        except BaseException as msg:
            message = "%s. [%s]" % (str(msg), '[ApacheController.checkIfApacheInstalled]')
            # logging.CyberCPLogFileWriter.writeToFile(message)

    @staticmethod
    def executioner(command):
        try:
            # subprocess.call(shlex.split(command))
            res = subprocess.call(shlex.split(command))
            if res == 1:
                return 0
            else:
                return 1
        except BaseException as msg:
            # logging.CyberCPLogFileWriter.writeToFile(str(msg))
            return 0

    @staticmethod
    def InstallApache():
        try:

            lpe = LitePagesExecutioner('')


            lpe.command = "apt-get update -y && sudo apt-get upgrade -y && apt-get install apache2 -y"

            lpe.shell = True

            ReturnCode, stdout, stderr = lpe.Execute()

            #if ReturnCode == 0:
            #    return stderr


            # confPath = ApacheVhost.serverRootPath + "/apache2.conf"
            confPath =  "/etc/apache2/apache2.conf"

            portsPath = '/etc/apache2/ports.conf'

            WriteToFile = open(portsPath, 'w')
            WriteToFile.write('Listen 8083\nListen 8082\n')
            WriteToFile.close()

            lpe = LitePagesExecutioner('')

            lpe.command = f"sed -i 's/User ${{APACHE_RUN_USER}}/User nobody/g' {confPath}"
            lpe.shell = True

            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                return "Apache run user change failed"

            lpe.command = f"sed -i 's/Group ${{APACHE_RUN_GROUP}}/Group nogroup/g' {confPath}"
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return "Apache run group change failed"



            lpe.command = 'apt-get install apache2-suexec-pristine -y'
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            # if ReturnCode == 0:
            #     return "Apache run apache2-suexec-pristine"

            lpe.command = 'a2enmod suexec proxy ssl proxy_fcgi proxy rewrite'
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            # if ReturnCode == 0:
            #     return "Apache run suexec proxy ssl"


            WriteToFile = open(confPath, 'a')
            WriteToFile.writelines('\nSetEnv LSWS_EDITION Openlitespeed\nSetEnv X-LSCACHE on\n')
            WriteToFile.close()

            ###

            serviceName = 'apache2'

            lpe.command = f"systemctl start {serviceName}.service"
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = f"systemctl enable {serviceName}.service"
            ReturnCode, stdout, stderr = lpe.Execute()

            return 1

        except BaseException as msg:
            return str(msg)

    @staticmethod
    def phpVersions():
        # Version 5.4

        # if ProcessUtilities.decideDistro() == ProcessUtilities.centos or ProcessUtilities.decideDistro() == ProcessUtilities.cent8:
        #
        #     command = 'yum install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm'
        #     ApacheController.executioner(command)
        #
        #     command = "yum install -y php?? php??-php-fpm  php??-php-mysql php??-php-curl php??-php-gd php??-php-mbstring php??-php-xml php??-php-zip php??-php-intl"
        #     if ProcessUtilities.executioner(command, None, True) == 0:
        #         return "Failed to install php54-fpm"
        #
        #
        # else:

        lpe = LitePagesExecutioner('')
        lpe.shell = True
        lpe.command = 'apt-get install software-properties-common -y'
        ReturnCode, stdout, stderr = lpe.Execute()


        lpe.command = 'apt install python-software-properties -y'
        ReturnCode, stdout, stderr = lpe.Execute()

        lpe.command = 'add-apt-repository ppa:ondrej/php -y'
        lpe.shell = True
        ReturnCode, stdout, stderr = lpe.Execute()
        if ReturnCode == 0:
            return "Failed to ppa:ondrej/php"


        lpe = LitePagesExecutioner('')
        lpe.shell = True
        lpe.command = "DEBIAN_FRONTEND=noninteractive apt-get install -y php-fpm php?.?-fpm php?.?-fpm php?.?-mysql php?.?-curl php?.?-gd php?.?-mbstring php?.?-xml php?.?-zip php?.?-intl"
        # lpe.command = "apt-get install -y php-fpm php?.?-fpm php?.?-fpm php?.?-mysql php?.?-curl php?.?-gd php?.?-mbstring php?.?-xml php?.?-zip php?.?-intl"
        ReturnCode, stdout, stderr = lpe.Execute()

        try:
            wwwConfPath = ApacheController.php54Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)

            wwwConfPath = ApacheController.php55Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)

            wwwConfPath = ApacheController.php56Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)

            wwwConfPath = ApacheController.php70Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)

            wwwConfPath = ApacheController.php71Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)

            wwwConfPath = ApacheController.php72Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)

            wwwConfPath = ApacheController.php73Path + "/www.conf"

            if os.path.exists(wwwConfPath):
                os.remove(wwwConfPath)
        except:
            pass

        return 1

    @staticmethod
    def setupApache(statusFile):
        try:



            result = ApacheController.InstallApache()

            if result != 1:
                return [0,result]

            LitePagesLogger.SendStatus("Installing PHP-FPMs, this may take some time..", statusFile, 1, 0, 0, 85)

            result = ApacheController.phpVersions()

            if result != 1:
                return [0,result]

            return [1, 'None']
        except BaseException as msg:
            return [0, str(msg)]