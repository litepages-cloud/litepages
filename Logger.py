import json
import subprocess
import time
import os
import smtplib
from socket import socket


class LitePagesLogger:

    LitePagesConfigPathFile = '/etc/LitePages/config'
    fileName = "/var/log/litepages.logs"

    ### These are level of info, for example when writing anything to log it will belong to any of 3 categories.
    ### If something is error, then it needs to be checked

    INFO = 0
    WARNING = 1
    DEBUG = 2
    ERROR = 3

    @staticmethod
    def AddFromHeader(sender, message):
        try:
            import re

            if not re.search('^From: ', message, re.MULTILINE):
                message = 'From: {}\n{}'.format(sender, message)

            return message
        except BaseException as msg:
            LitePagesLogger.writeToFile(str(msg) + ' [AddFromHeader]', 1)

    @staticmethod
    def SendEmail(sender, receivers, message, subject=None, type=None):
        try:
            smtpPath = '/home/cyberpanel/smtpDetails'

            if os.path.exists(smtpPath):
                import json

                mailSettings = json.loads(open(smtpPath, 'r').read())
                smtpHost = mailSettings['smtpHost']
                smtpPort = mailSettings['smtpPort']
                smtpUserName = mailSettings['smtpUserName']
                smtpPassword = mailSettings['smtpPassword']

                smtpServer = smtplib.SMTP(str(smtpHost), int(smtpPort))
                smtpServer.login(smtpUserName, smtpPassword)

                ##

                if subject != None:
                    message = 'Subject: {}\n\n{}'.format(subject, message)

                message = LitePagesLogger.AddFromHeader(sender, message)
                smtpServer.sendmail(smtpUserName, receivers, message)
            else:
                smtpObj = smtplib.SMTP('localhost')

                message = LitePagesLogger.AddFromHeader(sender, message)
                smtpObj.sendmail(sender, receivers, message)
        except BaseException as msg:
            LitePagesLogger.writeToFile(str(msg))

    @staticmethod
    def writeToFile(message, level, email=None):
        try:

            file = open(LitePagesLogger.fileName,'a')
            file.writelines("[" + time.strftime(
                    "%m.%d.%Y_%H-%M-%S") + "] "+ message + "\n")
            file.close()

            ## Send Email

            emailPath = '/usr/local/CyberCP/emailDebug'

            try:
                if os.path.exists(emailPath):
                    SUBJECT = "CyberPanel log reporting"
                    adminEmailPath = '/home/cyberpanel/adminEmail'
                    adminEmail = open(adminEmailPath, 'r').read().rstrip('\n')
                    sender = 'root@%s' % (socket.gethostname())
                    TO = [adminEmail]
                    message = """\
From: %s
To: %s
Subject: %s

%s
""" % (
                    sender, ", ".join(TO), SUBJECT, '[%s] %s. \n' % (time.strftime("%m.%d.%Y_%H-%M-%S"), message))

                    if email == None or email == 1:
                        LitePagesLogger.SendEmail(sender, TO, message)
            except BaseException as msg:
                file = open(LitePagesLogger.fileName, 'a')
                file.writelines("[" + time.strftime(
                    "%m.%d.%Y_%H-%M-%S") + "] " + str(msg) + "\n")
                file.close()

        except BaseException as msg:
            return "Can not write to error file."

    @staticmethod
    def writeforCLI(message, level, method):
        try:

            if level == LitePagesLogger.INFO:
                FinalLevel = 'INFO'
            elif level == LitePagesLogger.ERROR:
                FinalLevel = 'ERROR'
            elif level == LitePagesLogger.DEBUG:
                FinalLevel = 'DEBUG'
            elif level == LitePagesLogger.WARNING:
                FinalLevel = 'WARNING'

            FinalMessage = '[%s][%s][%s] %s\n' % (time.strftime("%m.%d.%Y_%H-%M-%S"), FinalLevel, method, message)

            file = open(LitePagesLogger.fileName, 'a')
            file.writelines(FinalMessage)
            file.close()
            file.close()
        except BaseException:
            return "Can not write to error file!"

    @staticmethod
    def SendStatus(message, jobid, level, abort, AbortStatus, percentage):
        config = json.loads(open(LitePagesLogger.LitePagesConfigPathFile, 'r').read())
        StatusURL = 'https://%s/Base/GlobalAjax' % (config['company'])
        counter = 0
        while True:
            try:
                import requests
                finalData = json.dumps({'function': 'LogWriting', 'token': config['token'], "job": jobid, "message": message,
                                        'level': level, 'abort': abort, 'AbortStatus': AbortStatus,
                                        'Percentage': percentage})
                requests.post(StatusURL, data=finalData, verify=False, timeout=20)
                break
            except TimeoutError as msg:
                if counter < 3:
                    time.sleep(3)
                    counter = counter + 1
                    LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'LitePagesLogger.SendStatus.TimeoutError')
                else:
                    break
            except ConnectionError as msg:
                if counter < 3:
                    time.sleep(3)
                    counter = counter + 1
                    LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR,
                                                'LitePagesLogger.SendStatus.ConnectionError')
                else:
                    break
            except BaseException as msg:
                if counter < 3:
                    time.sleep(3)
                    counter = counter + 1
                    LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR,
                                                'LitePagesLogger.SendStatus.BaseException')
                else:
                    break

    @staticmethod
    def SendMessageGeneral(function, data):
        config = json.loads(open(LitePagesLogger.LitePagesConfigPathFile, 'r').read())
        StatusURL = 'https://%s/Base/GlobalAjax' % (config['company'])
        try:
            import requests
            finalData = {'function': function, 'token': config['token'], 'id': config['id']}
            finalData.update(data)
            finalData = json.dumps(finalData)
            result = requests.post(StatusURL, data=finalData, verify=False)

            result = json.loads(result.text)

            if result['status'] == 0:
                LitePagesLogger.writeforCLI(result['error_message'], LitePagesLogger.ERROR, 'LitePagesLogger.SendMessageGeneral')

        except BaseException as msg:
            LitePagesLogger.writeforCLI(str(msg), LitePagesLogger.ERROR, 'LitePagesLogger.SendMessageGeneral')

    @staticmethod
    def readLastNFiles(numberOfLines,fileName):
        try:

            lastFewLines = str(subprocess.check_output(["tail", "-n",str(numberOfLines),fileName]).decode("utf-8"))

            return lastFewLines

        except subprocess.CalledProcessError as msg:
            return "File was empty"

    @staticmethod
    def statusWriter(mesg, level, JobID):
        LitePagesLogger.writeToFile(mesg, level)

    @staticmethod
    def CheckCurrentLogLevel():
        return LitePagesLogger.ERROR