import time

from LitePagesExecutioner import LitePagesExecutioner


class LPUpdates:

    def __init__(self, data=None):
        self.data = data

    def CloudPagesUpdate(self):
        lpe = LitePagesExecutioner('')
        lpe.command = "cd  /usr/local/LitePages"
        ReturnCode, stdout, stderr = lpe.Execute()

        lpe.command = "git pull"
        ReturnCode, stdout, stderr = lpe.Execute()

        lpe.command = "systemctl stop litepages"
        ReturnCode, stdout, stderr = lpe.Execute()

        time.sleep(5)

        lpe.command = "systemctl start litepages"
        ReturnCode, stdout, stderr = lpe.Execute()



