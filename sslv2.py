import logging
import time

import requests

# from plogical import CyberCPLogFileWriter as logging
import os
import shlex
import subprocess
import socket

from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger


# from plogical.acl import ACLManager
# from plogical.processUtilities import ProcessUtilities

# try:
#     from websiteFunctions.models import ChildDomains, Websites
# except:
#     pass


class sslUtilities:
    Server_root = "/usr/local/lsws"
    redisConf = '/usr/local/lsws/conf/dvhost_redis.conf'

    @staticmethod
    def checkIfSSLMap(virtualHostName):
        try:
            data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()

            sslCheck = 0

            for items in data:
                if items.find("listener") > - 1 and items.find("SSL") > -1:
                    sslCheck = 1
                    continue
                if sslCheck == 1:
                    if items.find("}") > -1:
                        return 0
                if items.find(virtualHostName) > -1 and sslCheck == 1:
                    data = [_f for _f in items.split(" ") if _f]
                    if data[1] == virtualHostName:
                        return 1

        except BaseException as msg:
            # logging.CyberCPLogFileWriter.writeToFile(str(msg) + " [IO Error with main config file [checkIfSSLMap]]")
            return 0

    @staticmethod
    def checkSSLListener():
        try:
            data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()
            for items in data:
                if items.find("listener SSL") > -1:
                    return 1

        except BaseException as msg:
            # logging.CyberCPLogFileWriter.writeToFile(str(msg) + " [IO Error with main config file [checkSSLListener]]")
            return str(msg)
        return 0

    @staticmethod
    def checkSSLIPv6Listener():
        try:
            data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()
            for items in data:
                if items.find("listener SSL IPv6") > -1:
                    return 1

        except BaseException as msg:
            # logging.CyberCPLogFileWriter.writeToFile(
            #     str(msg) + " [IO Error with main config file [checkSSLIPv6Listener]]")
            return str(msg)
        return 0

    @staticmethod
    def getDNSRecords(virtualHostName):
        try:

            withoutWWW = socket.gethostbyname(virtualHostName)
            withWWW = socket.gethostbyname('www.' + virtualHostName)

            return [1, withWWW, withoutWWW]

        except BaseException as msg:
            return [0, "347 " + str(msg) + " [issueSSLForDomain]"]

    @staticmethod
    def installSSLForDomain(virtualHostName, adminEmail='example@example.org', owner=None, path=None):

        try:

            adminEmail = adminEmail
        except BaseException as msg:
            pass
            # logging.CyberCPLogFileWriter.writeToFile('%s [installSSLForDomain:72]' % (str(msg)))

        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            confPath = sslUtilities.Server_root + "/conf/vhosts/" + virtualHostName
            completePathToConfigFile = confPath + "/vhost.conf"

            try:
                map = "  map                     " + virtualHostName + " " + virtualHostName + "\n"

                if sslUtilities.checkSSLListener() != 1:

                    writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'a')

                    listener = "listener SSL {" + "\n"
                    address = "  address                 *:443" + "\n"
                    secure = "  secure                  1" + "\n"
                    keyFile = "  keyFile                  /etc/letsencrypt/live/" + virtualHostName + "/privkey.pem\n"
                    certFile = "  certFile                 /etc/letsencrypt/live/" + virtualHostName + "/fullchain.pem\n"
                    certChain = "  certChain               1" + "\n"
                    sslProtocol = "  sslProtocol             24" + "\n"
                    enableECDHE = "  enableECDHE             1" + "\n"
                    renegProtection = "  renegProtection         1" + "\n"
                    sslSessionCache = "  sslSessionCache         1" + "\n"
                    enableSpdy = "  enableSpdy              15" + "\n"
                    enableStapling = "  enableStapling           1" + "\n"
                    ocspRespMaxAge = "  ocspRespMaxAge           86400" + "\n"
                    map = "  map                     " + virtualHostName + " " + virtualHostName + "\n"
                    final = "}" + "\n" + "\n"

                    writeDataToFile.writelines("\n")
                    writeDataToFile.writelines(listener)
                    writeDataToFile.writelines(address)
                    writeDataToFile.writelines(secure)
                    writeDataToFile.writelines(keyFile)
                    writeDataToFile.writelines(certFile)
                    writeDataToFile.writelines(certChain)
                    writeDataToFile.writelines(sslProtocol)
                    writeDataToFile.writelines(enableECDHE)
                    writeDataToFile.writelines(renegProtection)
                    writeDataToFile.writelines(sslSessionCache)
                    writeDataToFile.writelines(enableSpdy)
                    writeDataToFile.writelines(enableStapling)
                    writeDataToFile.writelines(ocspRespMaxAge)
                    writeDataToFile.writelines(map)
                    writeDataToFile.writelines(final)
                    writeDataToFile.writelines("\n")
                    writeDataToFile.close()

                elif sslUtilities.checkSSLIPv6Listener() != 1:

                    writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'a')

                    listener = "listener SSL IPv6 {" + "\n"
                    address = "  address                 [ANY]:443" + "\n"
                    secure = "  secure                  1" + "\n"
                    keyFile = "  keyFile                  /etc/letsencrypt/live/" + virtualHostName + "/privkey.pem\n"
                    certFile = "  certFile                 /etc/letsencrypt/live/" + virtualHostName + "/fullchain.pem\n"
                    certChain = "  certChain               1" + "\n"
                    sslProtocol = "  sslProtocol             24" + "\n"
                    enableECDHE = "  enableECDHE             1" + "\n"
                    renegProtection = "  renegProtection         1" + "\n"
                    sslSessionCache = "  sslSessionCache         1" + "\n"
                    enableSpdy = "  enableSpdy              15" + "\n"
                    enableStapling = "  enableStapling           1" + "\n"
                    ocspRespMaxAge = "  ocspRespMaxAge           86400" + "\n"
                    map = "  map                     " + virtualHostName + " " + virtualHostName + "\n"
                    final = "}" + "\n" + "\n"

                    writeDataToFile.writelines("\n")
                    writeDataToFile.writelines(listener)
                    writeDataToFile.writelines(address)
                    writeDataToFile.writelines(secure)
                    writeDataToFile.writelines(keyFile)
                    writeDataToFile.writelines(certFile)
                    writeDataToFile.writelines(certChain)
                    writeDataToFile.writelines(sslProtocol)
                    writeDataToFile.writelines(enableECDHE)
                    writeDataToFile.writelines(renegProtection)
                    writeDataToFile.writelines(sslSessionCache)
                    writeDataToFile.writelines(enableSpdy)
                    writeDataToFile.writelines(enableStapling)
                    writeDataToFile.writelines(ocspRespMaxAge)
                    writeDataToFile.writelines(map)
                    writeDataToFile.writelines(final)
                    writeDataToFile.writelines("\n")
                    writeDataToFile.close()

                else:

                    if sslUtilities.checkIfSSLMap(virtualHostName) == 0:

                        data = open("/usr/local/lsws/conf/httpd_config.conf").readlines()
                        writeDataToFile = open("/usr/local/lsws/conf/httpd_config.conf", 'w')
                        sslCheck = 0

                        for items in data:
                            if items.find("listener") > -1 and items.find("SSL") > -1:
                                sslCheck = 1

                            if (sslCheck == 1):
                                writeDataToFile.writelines(items)
                                writeDataToFile.writelines(map)
                                sslCheck = 0
                            else:
                                writeDataToFile.writelines(items)
                        writeDataToFile.close()

                    ###################### Write per host Configs for SSL ###################

                    data = open(completePathToConfigFile, "r").readlines()

                    ## check if vhssl is already in vhconf file

                    vhsslPresense = 0

                    for items in data:
                        if items.find("vhssl") > -1:
                            vhsslPresense = 1

                    if vhsslPresense == 0:
                        writeSSLConfig = open(completePathToConfigFile, "a")

                        vhssl = "vhssl  {" + "\n"
                        keyFile = "  keyFile                 /etc/letsencrypt/live/" + virtualHostName + "/privkey.pem\n"
                        certFile = "  certFile                /etc/letsencrypt/live/" + virtualHostName + "/fullchain.pem\n"
                        certChain = "  certChain               1" + "\n"
                        sslProtocol = "  sslProtocol             24" + "\n"
                        enableECDHE = "  enableECDHE             1" + "\n"
                        renegProtection = "  renegProtection         1" + "\n"
                        sslSessionCache = "  sslSessionCache         1" + "\n"
                        enableSpdy = "  enableSpdy              15" + "\n"
                        enableStapling = "  enableStapling           1" + "\n"
                        ocspRespMaxAge = "  ocspRespMaxAge           86400" + "\n"
                        final = "}"

                        writeSSLConfig.writelines("\n")

                        writeSSLConfig.writelines(vhssl)
                        writeSSLConfig.writelines(keyFile)
                        writeSSLConfig.writelines(certFile)
                        writeSSLConfig.writelines(certChain)
                        writeSSLConfig.writelines(sslProtocol)
                        writeSSLConfig.writelines(enableECDHE)
                        writeSSLConfig.writelines(renegProtection)
                        writeSSLConfig.writelines(sslSessionCache)
                        writeSSLConfig.writelines(enableSpdy)
                        writeSSLConfig.writelines(enableStapling)
                        writeSSLConfig.writelines(ocspRespMaxAge)
                        writeSSLConfig.writelines(final)

                        writeSSLConfig.writelines("\n")

                        writeSSLConfig.close()

                return 1
            except BaseException as msg:
                # logging.CyberCPLogFileWriter.writeToFile(str(msg) + " [installSSLForDomain]]")
                return 0
        else:
            if not os.path.exists(sslUtilities.redisConf):
                confPath = sslUtilities.Server_root + "/conf/vhosts/" + virtualHostName
                completePathToConfigFile = confPath + "/vhost.conf"

                ## Check if SSL VirtualHost already exists

                data = open(completePathToConfigFile, 'r').readlines()

                for items in data:
                    if items.find('*:443') > -1:
                        return 1

                try:

                    # try:
                    #     chilDomain = ChildDomains.objects.get(domain=virtualHostName)
                    #     externalApp = chilDomain.master.externalApp
                    #     DocumentRoot = '    DocumentRoot ' + path + '\n'
                    # except BaseException as msg:
                    #     website = Websites.objects.get(domain=virtualHostName)
                    #     externalApp = website.externalApp
                    #     DocumentRoot = '    DocumentRoot /home/' + virtualHostName + '/public_html\n'

                    DocumentRoot = '    DocumentRoot ' + path + '\n'
                    data = open(completePathToConfigFile, 'r').readlines()
                    phpHandler = ''

                    for items in data:
                        if items.find('AddHandler') > -1 and items.find('php') > -1:
                            phpHandler = items
                            break

                    confFile = open(completePathToConfigFile, 'a')

                    cacheRoot = """    <IfModule LiteSpeed>
            CacheRoot lscache
            CacheLookup on
        </IfModule>
    """

                    VirtualHost = '\n<VirtualHost *:443>\n\n'
                    ServerName = '    ServerName ' + virtualHostName + '\n'
                    ServerAlias = '    ServerAlias www.' + virtualHostName + '\n'
                    ServerAdmin = '    ServerAdmin ' + adminEmail + '\n'
                    SeexecUserGroup = '    SuexecUserGroup ' + owner + ' ' + owner + '\n'
                    CustomLogCombined = '    CustomLog /home/' + virtualHostName + '/logs/' + virtualHostName + '.access_log combined\n'

                    confFile.writelines(VirtualHost)
                    confFile.writelines(ServerName)
                    confFile.writelines(ServerAlias)
                    confFile.writelines(ServerAdmin)
                    confFile.writelines(SeexecUserGroup)
                    confFile.writelines(DocumentRoot)
                    confFile.writelines(CustomLogCombined)
                    confFile.writelines(cacheRoot)

                    SSLEngine = '    SSLEngine on\n'
                    SSLVerifyClient = '    SSLVerifyClient none\n'
                    SSLCertificateFile = '    SSLCertificateFile /etc/letsencrypt/live/' + virtualHostName + '/fullchain.pem\n'
                    SSLCertificateKeyFile = '    SSLCertificateKeyFile /etc/letsencrypt/live/' + virtualHostName + '/privkey.pem\n'

                    confFile.writelines(SSLEngine)
                    confFile.writelines(SSLVerifyClient)
                    confFile.writelines(SSLCertificateFile)
                    confFile.writelines(SSLCertificateKeyFile)
                    confFile.writelines(phpHandler)

                    VirtualHostEnd = '</VirtualHost>\n'
                    confFile.writelines(VirtualHostEnd)
                    confFile.close()
                    return 1
                except BaseException as msg:
                    # logging.CyberCPLogFileWriter.writeToFile(str(msg) + " [installSSLForDomain]")
                    return 0
            else:
                cert = open('/etc/letsencrypt/live/' + virtualHostName + '/fullchain.pem').read().rstrip('\n')
                key = open('/etc/letsencrypt/live/' + virtualHostName + '/privkey.pem', 'r').read().rstrip('\n')
                lpe = LitePagesExecutioner('')
                lpe.command = 'redis-cli hmset "ssl:%s" crt "%s" key "%s"' % (virtualHostName, cert, key)
                ReturnCode, stdout, stderr = lpe.Execute()

                return 1


    @staticmethod
    def FetchCloudFlareAPIKeyFromAcme():
        try:

            if os.path.exists('/root/.acme.sh'):
                acmePath = '/root/.acme.sh/account.conf'
            else:
                acmePath = '/.acme.sh/account.conf'

            lpe = LitePagesExecutioner('')

            lpe.command = f'grep SAVED_CF_Key= {acmePath} | cut -d= -f2 | tr -d "\'"'
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 1:
                SAVED_CF_Key = stdout.rstrip('\n')
            else:
                return 0, stdout, stdout

            lpe.command = f'grep SAVED_CF_Email= {acmePath} | cut -d= -f2 | tr -d "\'"'
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 1:
                SAVED_CF_Email = stdout.rstrip('\n')

                if len(SAVED_CF_Key) > 3 and len(SAVED_CF_Email) > 3:
                    return 1, SAVED_CF_Key, SAVED_CF_Email
                else:
                    return 0, 'Key not defined', SAVED_CF_Email
            else:
                return 0, stdout, stdout

        except BaseException as msg:
            LitePagesLogger.writeforCLI(f'FetchCloudFlareAPIKeyFromAcme, Error: {str(msg)}', LitePagesLogger.ERROR, 'FetchCloudFlareAPIKeyFromAcme')
            return 0, str(msg), None

    @staticmethod
    def FindIfDomainInCloudflare(virtualHostName):
        try:
            try:
                import tldextract
            except:
                return 0, str("tldextract is not installed, it can be installed using pip3 install tldextract"), None

            RetStatus, SAVED_CF_Key, SAVED_CF_Email = sslUtilities.FetchCloudFlareAPIKeyFromAcme()

            if RetStatus:

                extractDomain = tldextract.extract(virtualHostName)
                topLevelDomain = extractDomain.domain + '.' + extractDomain.suffix
                # logging.CyberCPLogFileWriter.writeToFile(f'top level domain in cf: {topLevelDomain}')
                try:
                    import CloudFlare
                except:
                    return 0, str("cloudflare is not installed, it can be installed using pip3 install cloudflare")

                params = {'name': topLevelDomain, 'per_page': 50}


                cf = CloudFlare.CloudFlare(email=SAVED_CF_Email, token=SAVED_CF_Key)


                try:
                    zones = cf.zones.get(params=params)
                except BaseException as msg:
                    return 0, str(msg)

                for zone in sorted(zones, key=lambda v: v['name']):
                    # logging.CyberCPLogFileWriter.writeToFile(f'zone: {zone["name"]}')
                    if zone['name'] == topLevelDomain:
                        if zone['status'] == 'active':
                            return 1, None

                return 0, 'Zone not found in Cloudflare'

            else:
                return 0, SAVED_CF_Key
        except BaseException as msg:
            LitePagesLogger.writeforCLI(f'FindIfDomainInCloudflare, Error: {str(msg)}', LitePagesLogger.ERROR, 'FindIfDomainInCloudflare')
            return 0, str(msg)


    @staticmethod
    def obtainSSLForADomain(virtualHostName, adminEmail, sslpath, aliasDomain=None):
        sender_email = 'root@%s' % (socket.gethostname())

        lpe = LitePagesExecutioner('')
        lpe.shell = True

        CF_Check = 0
        Namecheck_Check = 0

        CF_Check, message = sslUtilities.FindIfDomainInCloudflare(virtualHostName)

        DNS_TO_USE = ''

        if CF_Check:
            DNS_TO_USE = 'dns_cf'
        else:
            return 0, message

        RetStatus, SAVED_CF_Key, SAVED_CF_Email = sslUtilities.FetchCloudFlareAPIKeyFromAcme()

        if RetStatus:
            initialCommand = f'export CF_Key={SAVED_CF_Key} CF_Email={SAVED_CF_Email} && '
        else:
            return 0, SAVED_CF_Key

        try:

            acmePath = '/root/.acme.sh/acme.sh'
            acmePathSecond = '/.acme.sh/acme.sh'

            if not os.path.exists(acmePath) and not os.path.exists(acmePathSecond):
                ## Install ACME
                lpe.shell = True
                lpe.command = 'git clone https://github.com/acmesh-official/acme.sh.git; cd acme.sh; ./acme.sh --install --home /root/.acme.sh'
                lpe.Execute()

                lpe.command = '/root/.acme.sh/acme.sh --upgrade --auto-upgrade'
                lpe.Execute()

                lpe.shell = False

            if not os.path.exists(acmePath):
                acmePath = acmePathSecond

            acmePath = f'{initialCommand} {acmePath}'


            ### register account for zero ssl

            command = '%s --register-account -m %s' % (acmePath, adminEmail)
            ReturnCode, stdout, stderr = lpe.Execute()

            # if ProcessUtilities.decideDistro() == ProcessUtilities.ubuntu:
            #     acmePath = '/home/cyberpanel/.acme.sh/acme.sh'

            existingCertPath = '/etc/letsencrypt/live/' + virtualHostName
            if not os.path.exists(existingCertPath):
                command = 'mkdir -p ' + existingCertPath
                subprocess.check_output(shlex.split(command))

            try:
                lpe.command = acmePath + f" --issue -d *.{virtualHostName} -d {virtualHostName}" \
                              + ' --cert-file ' + existingCertPath + '/cert.pem' + ' --key-file ' + existingCertPath + '/privkey.pem' \
                              + ' --fullchain-file ' + existingCertPath + '/fullchain.pem' + f' --dns {DNS_TO_USE} -k ec-256 --force --server letsencrypt'


                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode:
                    output = stdout

                else:
                    output = stdout + stderr

            except subprocess.CalledProcessError:

                try:
                    LitePagesLogger.writeToFile(virtualHostName, LitePagesLogger.ERROR, 0)
                    lpe.command = acmePath + " --issue -d " + virtualHostName + ' --cert-file ' + existingCertPath \
                                  + '/cert.pem' + ' --key-file ' + existingCertPath + '/privkey.pem' \
                                  + ' --fullchain-file ' + existingCertPath + '/fullchain.pem' + f' --dns {DNS_TO_USE} -k ec-256 --force --server letsencrypt'

                    LitePagesLogger.writeToFile(command, LitePagesLogger.ERROR, 0)
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode:
                        output = stdout
                    else:
                        output = stdout + stderr


                except subprocess.CalledProcessError as msg:
                    return 0, str(msg)

            ##

            if output.find('Cert success') > -1:
                return 1, output
            else:
                return 0, output
        except BaseException as msg:
            # logging.CyberCPLogFileWriter.writeToFile(str(msg) + " [Failed to obtain SSL. [obtainSSLForADomain]]")
            return 0, str(msg)


def issueSSLForDomain(domain, adminEmail, sslpath, owner, aliasDomain=None):
    try:
        retStatus, message = sslUtilities.obtainSSLForADomain(domain, adminEmail, sslpath, aliasDomain)
        if retStatus == 1:
            if sslUtilities.installSSLForDomain(domain, adminEmail, owner, sslpath) == 1:
                return [1, message]
            else:
                return [0, message]
        else:
            return [0, message]

    except BaseException as msg:
        return [0, "347 " + str(msg) + " [issueSSLForDomain]"]
