import os
import random, time
import shutil
from random import seed
import string
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
from VirtualHost import VirtualHost
try:
    seed(time.perf_counter())
except:
    pass

class WPManager:

    LitePagesPath = '/var/litepages'
    SecureTempPath = '%s/secure' % (LitePagesPath)

    @staticmethod
    def generate_pass( length=20):
        chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
        size = length
        return ''.join(random.choice(chars) for x in range(size))

    def __init__(self, data):
        self.data = data

    def SecurePath(self):

        if not os.path.exists(WPManager.SecureTempPath):

            command = '/usr/sbin/adduser --home %s --disabled-login --gecos "" litepages' % (WPManager.LitePagesPath)
            lpe = LitePagesExecutioner(command)
            ReturnCode, stdout, stderr = lpe.Execute()

            os.chmod(WPManager.LitePagesPath, 0o711)
            os.mkdir(WPManager.SecureTempPath)
            os.chmod(WPManager.SecureTempPath, 0o711)

            lpe.command = 'chown litepages:litepages %s' % (WPManager.SecureTempPath)
            lpe.Execute()


    def CheckAndInstallWPCli(self):

        command = '/usr/bin/wp --help --allow-root'
        lpe = LitePagesExecutioner(command)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            lpe.command = 'rm -f /usr/bin/wp'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'rm -f  /usr/local/bin/wp'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'wget -O /usr/bin/wp https://github.com/wp-cli/wp-cli/releases/download/v2.7.0/wp-cli-2.7.0.phar'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'chmod +x /usr/bin/wp'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            lpe.command = 'ln -s /usr/bin/wp /usr/local/bin/wp'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                return 0, stderr

            ## Again check

            lpe.command = '/usr/bin/wp --help --allow-root'

            if ReturnCode == 0:
                return 0, stderr
            else:
                return 1, None

        else:
            return 1, None

    def DeployWordPress(self):
        try:

            status, message = self.CheckAndInstallWPCli()

            if status == 0:
                LitePagesLogger.SendStatus(message, self.JobID, 1, 1, 1, 0)
                return 0, message

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            lpe = LitePagesExecutioner('')

            self.JobID = self.data['JobID']

            ##

            LitePagesLogger.SendStatus('Downloading WordPress Core.', self.JobID, 1, 0, 0, 20)


            lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp core download --path=%s --version=%s" % (
            self.data['VHUser'], FinalPHPPath, self.data['path'], self.data['WPVersion'])
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = True
            if ReturnCode == 0:
                lpe.shell = False

                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            lpe.shell = False
            ##

            LitePagesLogger.SendStatus('Configuring WordPress..', self.JobID, 1, 0, 0, 30)




            lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp core config --dbname=%s --dbuser=%s --dbpass=%s --dbhost=%s:%s --dbprefix=wp_ --path=%s" % (
            self.data['VHUser'], FinalPHPPath, self.data['database'], self.data['DBUser'], self.data['DBPassword'],
            self.data['DBHost'], self.data['DBPort'], self.data['path'])

            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            ### Installing WordPress

            LitePagesLogger.SendStatus('Configuring WordPress..', self.JobID, 1, 0, 0, 40)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp core install --url="http://%s" --title="%s" --admin_user="%s" --admin_password="%s" --admin_email="%s" --path=%s' % (
            self.data['VHUser'], FinalPHPPath, self.data['FinalURL'], self.data['title'], self.data['UserName'],
            self.data['Password'], self.data['Email'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr


            ## Enable permalinks


            lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp rewrite structure '/%%postname%%/' --path=%s" % (
            self.data['VHUser'], FinalPHPPath, self.data['path'])

            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            DefaultRewriteRules = """
# BEGIN WordPress
# The directives (lines) between "BEGIN WordPress" and "END WordPress" are
# dynamically generated, and should only be modified via WordPress filters.
# Any changes to the directives between these markers will be overwritten.
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>

# END WordPress            
"""

            self.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(self.data['path']).st_uid
            websiteVHUserGID = os.stat(self.data['path']).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)

            TempHtaccessFilePath = '%s/.htaccess' % (TempPath)

            htWrite = open(TempHtaccessFilePath, 'w')
            htWrite.write(DefaultRewriteRules)
            htWrite.close()

            os.chown(TempHtaccessFilePath, websiteVHUserUID, websiteVHUserGID)

            lpe.command = 'sudo -u %s mv %s %s' % (self.data['VHUser'], TempHtaccessFilePath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            shutil.rmtree(TempPath)

            ### Installing LSCache plugin

            LitePagesLogger.SendStatus('Installing LSCache plugin..', self.JobID, 1, 0, 0, 45)

            lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin install litespeed-cache --path=%s --activate" % (
            self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr


            ## Configuring auto updates

            LitePagesLogger.SendStatus('Configuring automatic updates', self.JobID, 1, 0, 0, 70)

            if self.data['AutomaticUpdates'] == 'Disabled':
                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp config set WP_AUTO_UPDATE_CORE false --raw --path=%s" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr
            elif self.data['AutomaticUpdates'] == 'Minor and Security Updates':
                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp config set WP_AUTO_UPDATE_CORE minor --path=%s" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr
            else:
                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp config set WP_AUTO_UPDATE_CORE true --raw --path=%s" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr

            ### Plugin themes packs

            if self.data['PluginsThemes'] == 'WordPress+LSCache+Classic Editor':

                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin install classic-editor --path=%s --activate" % (
                self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr


            elif self.data['PluginsThemes'] == 'WordPress+LSCache+WooCommerce':

                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin install woocommerce --path=%s --activate" % (
                self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr



            ###installed saved plugin

            LitePagesLogger.writeToFile("installed saved plugin starts.......value is:%s"%self.data['SavedPlugins'], LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("saved plugin list.......:%s"%self.data['AllPluginList'], LitePagesLogger.ERROR, 0)


            try:
                if self.data['SavedPlugins'] == True:
                    ab= self.data['AllPluginList']
                    for i in range(len(ab)):
                        # print("sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin install %s --path=%s" % (self.data['VHUser'], FinalPHPPath, ab[i], self.data['path']))
                        lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin install %s --path=%s --activate" % (
                            self.data['VHUser'], FinalPHPPath, ab[i], self.data['path'])
                        ReturnCode, stdout, stderr = lpe.Execute()

                        if ReturnCode == 0:
                            LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                            return 0, stderr

            except BaseException as msg:
                LitePagesLogger.writeToFile("Error in installed saved plugin starts.......value is:%s" %msg, LitePagesLogger.ERROR, 0)


            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.DeployWordPress:107]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def DeleteWordPress(self):
        try:

            lpe = LitePagesExecutioner('')
            self.JobID = self.data['JobID']

            ##

            WebsitePath = '%s/public_html' % self.data['WebsitePath'].rstrip('/')
            path = self.data['path'].rstrip('/')

            if WebsitePath != path:
                lpe.command = "sudo -u %s rm -rf %s" % (self.data['VHUser'], self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.DeleteWordPress:130]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def FetchWordPressDetails(self):
        try:
            finalDic = {}

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp core version --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                finalDic['version'] = str(stdout)
            else:
                return 0, stderr

            ## LSCache

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin status litespeed-cache --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                if stdout.find('Status: Active') > -1:
                    finalDic['lscache'] = 1
                else:
                    finalDic['lscache'] = 0
            else:
                finalDic['lscache'] = 0

            ## Debug

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config list --skip-plugins --skip-themes --path=%s' % (
            self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                finalDic['debugging'] = 0
                for items in stdout.split('\n'):
                    if items.find('WP_DEBUG') > -1 and items.find('1') > - 1:
                        finalDic['debugging'] = 1
                        break
            else:
                return 0, stderr

            ## Search index

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp option get blog_public --skip-plugins --skip-themes --path=%s' % (
                self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                finalDic['searchIndex'] = int(stdout.splitlines()[-1])
            else:
                return 0, stderr

            ## Maintenece mode

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp maintenance-mode status --skip-plugins --skip-themes --path=%s' % (
            self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                result = stdout.splitlines()[-1]

                if result.find('not active') > -1:
                    finalDic['maintenanceMode'] = 0
                else:
                    finalDic['maintenanceMode'] = 1
            else:
                return 0, stderr

            ## Get title

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp option get blogname --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                finalDic['title'] = stdout.splitlines()[-1]
            else:
                return 0, stderr

            return 1, finalDic

        except BaseException as msg:
            return 0, str(msg)

    def UpdateWPSettings(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            if self.data['setting'] == 'lscache':
                if self.data['settingValue']:

                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin install litespeed-cache --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr

                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin activate litespeed-cache --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr

                else:
                    lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin deactivate litespeed-cache --path=%s --skip-plugins --skip-themes' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr

            elif self.data['setting'] == 'debugging':

                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp litespeed-purge all --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if self.data['settingValue']:
                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp config set WP_DEBUG true --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr


                else:
                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp config set WP_DEBUG false --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr


            elif self.data['setting'] == 'searchIndex':

                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp litespeed-purge all --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()


                if self.data['settingValue']:
                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp option update blog_public 1 --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr
                else:
                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp option update blog_public 0 --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr
            elif self.data['setting'] == 'maintenanceMode':

                lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp litespeed-purge all --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()


                if self.data['settingValue']:

                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp maintenance-mode activate --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr
                else:
                    lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp maintenance-mode deactivate --path=%s --skip-plugins --skip-themes" % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        return 0, stderr

            return 1, None

        except BaseException as msg:
            return 0, str(msg)

    def AutoLogin(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            ## Get title

            password = WPManager.generate_pass(32)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp user create autologin %s --role=administrator --user_pass="%s" --path=%s --skip-plugins --skip-themes' % (self.data['VHUser'], FinalPHPPath, 'autologin@cloudpages.cloud', password, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp user update autologin --user_pass="%s" --path=%s --skip-plugins --skip-themes' % (self.data['VHUser'], FinalPHPPath, password, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                return 1, password
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    ### Plugins

    def GetCurrentPlugins(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin list --skip-plugins --skip-themes --format=json --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                json_data = stdout.splitlines()[-1]
                return 1, json_data
            else:
                return 0, stderr

        except BaseException as msg:
            return 0, str(msg)

    def UpdatePlugins(self):
        try:

            self.JobID = self.data['JobID']

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            LitePagesLogger.SendStatus('Starting plugin(s) update..', self.JobID, 1, 0, 0, 20)

            if self.data['plugin'] == 'all':
                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin update --all --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            elif self.data['plugin'] == 'selected':

                pluginsList = ''

                for plugin in self.data['plugins']:
                    pluginsList = '%s %s' % (pluginsList, plugin)

                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin update %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, pluginsList, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            else:
                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin update %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)


            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.UpdatePlugins:403]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def DeletePlugins(self):
        try:

            self.JobID = self.data['JobID']

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            if self.data['plugin'] == 'selected':
                pluginsList = ''

                for plugin in self.data['plugins']:
                    pluginsList = '%s %s' % (pluginsList, plugin)

                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin delete %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, pluginsList, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            else:
                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin delete %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])

                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.UpdatePlugins:403]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def ChangeStatus(self):
        try:

            self.JobID = self.data['JobID']

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin status %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()


            if ReturnCode == 1:
                if stdout.find('Status: Active') > -1:
                    lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin deactivate %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()

                    if ReturnCode == 0:
                        LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                        return 0
                else:

                    lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin activate %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode == 0:
                        LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                        return 0
            else:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.UpdatePlugins:403]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    ### Themes functions

    def GetCurrentThemes(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            lpe = LitePagesExecutioner('')

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme list --skip-plugins --skip-themes --format=json --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                json_data = stdout.splitlines()[-1]
                return 1, json_data
            else:
                return 0, stderr
        except BaseException as msg:
            return 0, str(msg)

    def UpdateThemes(self):
        try:
            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            self.JobID = self.data['JobID']

            lpe = LitePagesExecutioner('')

            if self.data['plugin'] == 'all':
                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme update --all --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            elif self.data['plugin'] == 'selected':

                pluginsList = ''

                for plugin in self.data['plugins']:
                    pluginsList = '%s %s' % (pluginsList, plugin)

                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme update %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, pluginsList, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)
            else:
                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme update %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.UpdateThemes:574]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def ChangeStatusThemes(self):
        try:
            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            self.JobID = self.data['JobID']

            lpe = LitePagesExecutioner('')

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme status %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                if stdout.find('Status: Active') > -1:
                    lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme deactivate %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode == 0:
                        LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                        return 0
                    else:
                        LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)
                else:
                    lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme activate %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'], self.data['path'])
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode == 0:
                        LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                        return 0
                    else:
                        LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            else:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.ChangeStateThemes:604]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def DeleteThemes(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            self.JobID = self.data['JobID']

            lpe = LitePagesExecutioner('')

            if self.data['plugin'] == 'selected':
                pluginsList = ''

                for plugin in self.data['plugins']:
                    pluginsList = '%s %s' % (pluginsList, plugin)

                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme delete %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, pluginsList, self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)
            else:
                lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme delete %s --skip-plugins --skip-themes --path=%s' % (self.data['VHUser'], FinalPHPPath, self.data['plugin'],self.data['path'])
                ReturnCode, stdout, stderr = lpe.Execute()
                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0
                else:
                    LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 50)

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)
        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [WPManager.DeleteThemes:637]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    ## Staging

    def CreateStagingFinal(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            self.JobID = self.data['JobID']

            MasterDomain = self.data['MasterDomain']
            StagingDomain = self.data['StagingDomain']
            MasterPath = self.data['MasterPath']
            StagingPath = self.data['StagingPath']
            StagingVHUser = self.data['StagingVHUser']
            MasterVHUser = self.data['MasterVHUser']
            Database = self.data['database']
            DatabaseUser = self.data['DatabaseUser']
            DatabasePassword = self.data['DatabasePassword']
            DatabaseHost = self.data['DatabaseHost']
            DatabasePort = self.data['DatabasePort']

            ### Setup secure path

            self.SecurePath()

            TempPath = '%s/%s' % (WPManager.SecureTempPath, str(random.randint(1000, 9999)))
            self.TempPath = TempPath

            MasterVHUserID = os.stat(MasterPath).st_uid
            MasterVHUserGID = os.stat(MasterPath).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, MasterVHUserID, MasterVHUserGID)
            os.chmod(TempPath, 0o700)

            ###

            lpe = LitePagesExecutioner('')

            LitePagesLogger.SendStatus('Creating staging site now..', self.JobID, 1, 0, 0, 50)

            ### check if there is forward slash and remove

            if MasterPath.endswith('/'):
                MasterPath = MasterPath[:-1]

            if StagingPath.endswith('/'):
                StagingPath = StagingPath[:-1]

            configPath = '%s/wp-config.php' % (MasterPath)

            ## Check if WP Detected on Main Site

            lpe.command = 'sudo -u %s ls -la %s' % (MasterVHUser, configPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus('No WordPress site detected.', self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ### Get table prefix from master site

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get table_prefix --skip-plugins --skip-themes --path=%s' % (
            MasterVHUser, FinalPHPPath, MasterPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                TablePrefix = stdout.rstrip('\n')
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Creating WP Site and setting Database

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp core download --path=%s' % (StagingVHUser, FinalPHPPath, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)


            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp core config --dbname=%s --dbuser=%s --dbpass=%s --dbhost=%s:%s --path=%s' % (StagingVHUser, FinalPHPPath, Database, DatabaseUser, DatabasePassword, DatabaseHost, DatabasePort, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ### Set table prefix

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config set table_prefix %s --path=%s' % (
            StagingVHUser, FinalPHPPath, TablePrefix, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Exporting database

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp --skip-plugins --skip-themes --path=%s db export %s/dbexport-stage.sql' % (MasterVHUser, FinalPHPPath, MasterPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ### Get path of WP Content

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme path --skip-plugins --skip-themes --path=%s' % (
            MasterVHUser, FinalPHPPath, MasterPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                WpContentPath = stdout.splitlines()[-1].replace('themes', '')


            ### Move master WP Content to Temppath

            lpe.command = 'sudo -u %s cp -Rp %s %s' % (MasterVHUser, WpContentPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Copy Master htaccess to temp

            lpe.command = 'sudo -u %s cp -f %s/.htaccess %s/' % (MasterVHUser, WpContentPath.replace('/wp-content/', ''), TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            # lpe.command = 'cp -f %s/.htaccess %s/' % (WpContentPath.replace('/wp-content/', ''), StagingPath)
            # ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ### Change owner of Temppath folder to staging


            lpe.command = 'chown -R %s:%s %s' % (StagingVHUser, StagingVHUser, TempPath)

            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Import database

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp --allow-root --skip-plugins --skip-themes --path=%s --quiet db import %s/dbexport-stage.sql' % (
            StagingVHUser, FinalPHPPath, StagingPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)


            lpe.command = 'sudo -u %s cp -pR %s/wp-content/ %s/' % (StagingVHUser, TempPath, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s cp -f %s/.htaccess %s/' % (StagingVHUser, TempPath, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Search and replace url

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --path=%s "%s" "%s"' % (StagingVHUser, FinalPHPPath, StagingPath, MasterDomain.rstrip('/'), StagingDomain)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --allow-root --path=%s "www.%s" "%s"' % (StagingVHUser, FinalPHPPath, StagingPath, MasterDomain.rstrip('/'), StagingDomain)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --allow-root --path=%s "https://www.%s" "http://%s"' % (
            StagingVHUser, FinalPHPPath, StagingPath, StagingDomain, StagingDomain)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --allow-root --path=%s "https://%s" "http://%s"' % (
                StagingVHUser, FinalPHPPath, StagingPath, StagingDomain, StagingDomain)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ### disable lscache plugin in staging site

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin deactivate litespeed-cache --path=%s --skip-plugins --skip-themes' % (StagingVHUser, FinalPHPPath, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp plugin deactivate wp-rocket --path=%s --skip-plugins --skip-themes' % (
            StagingVHUser, FinalPHPPath, StagingPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ###

            lpe.command = "sudo -u %s %s -d error_reporting=0 /usr/bin/wp option update blog_public 0 --path=%s --skip-plugins --skip-themes" % (
            StagingVHUser, FinalPHPPath, StagingPath)
            lpe.Execute()


            LitePagesExecutioner.RestartServer()

            shutil.rmtree(self.TempPath)

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 0
        except BaseException as msg:
            shutil.rmtree(self.TempPath)
            LitePagesLogger.SendStatus('%s [WPManager.startCloning:736]' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0

    def DeployToProduction(self):
        try:

            php = VirtualHost.getPHPString(self.data['PHPVersion'])
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            self.JobID = self.data['JobID']
            lpe = LitePagesExecutioner('')

            MasterPath = self.data['MasterPath']
            StagingPath = self.data['StagingPath']
            StagingVHUser = self.data['StagingVHUser']
            MasterVHUser = self.data['MasterVHUser']
            MasterDomain = self.data['MasterDomain']
            StagingDomain = self.data['StagingDomain']

            ### Setup secure path

            self.SecurePath()

            TempPath = '%s/%s' % (WPManager.SecureTempPath, str(random.randint(1000, 9999)))
            self.TempPath = TempPath

            StagingPathVHUserID = os.stat(StagingPath).st_uid
            StagingPathVHUserGID = os.stat(StagingPath).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, StagingPathVHUserID, StagingPathVHUserGID)

            ###

            configPath = '%s/wp-config.php' % (StagingPath)
            lpe.command = 'sudo -u %s ls -la %s' % (StagingVHUser, configPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus('No WordPress site detected.', self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Restore db

            LitePagesLogger.SendStatus('Syncing databases..', self.JobID, 1, 0, 0, 10)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp --allow-root --skip-plugins --skip-themes --path=%s db export %s/dbexport-stage.sql' % (StagingVHUser, FinalPHPPath, StagingPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## Get path of WP Content Folder

            LitePagesLogger.SendStatus('Syncing data..', self.JobID, 1, 0, 0, 50)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp theme path --skip-plugins --skip-themes --path=%s' % (
                MasterVHUser, FinalPHPPath, MasterPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                WpContentPath = stdout.splitlines()[-1].replace('wp-content/themes', '')

            lpe.command = 'sudo -u %s cp -R %s/wp-content/ %s' % (StagingVHUser, StagingPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ## COPY Htaccess

            lpe.command = 'sudo -u %s cp -f %s/.htaccess %s' % (StagingVHUser, StagingPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)


            ## Restore to master domain

            lpe.command = 'chown -R %s:%s %s' % (MasterVHUser, MasterVHUser, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp --allow-root --skip-plugins --skip-themes --path=%s --quiet db import %s/dbexport-stage.sql' % (MasterVHUser, FinalPHPPath, MasterPath, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s cp -pR %s/wp-content/ %s/' % (MasterVHUser, TempPath, MasterPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s cp -f %s/.htaccess %s/' % (MasterVHUser, TempPath, MasterPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)


            ## Search and replace url

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --allow-root --skip-plugins --skip-themes --path=%s "%s" "%s"' % (MasterVHUser, FinalPHPPath, MasterPath, StagingDomain, MasterDomain.rstrip('/'))
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --allow-root --skip-plugins --skip-themes --path=%s "www.%s" "%s"' % (MasterVHUser, FinalPHPPath, MasterPath, StagingDomain, MasterDomain.rstrip('/'))
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            ### Now take care of https urls

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --allow-root --skip-plugins --skip-themes --path=%s "https://www.%s" "http://%s"' % (MasterVHUser,
            FinalPHPPath, MasterPath, MasterDomain.rstrip('/'), MasterDomain.rstrip('/'))

            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)


            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp search-replace --allow-root --skip-plugins --skip-themes --path=%s "https://%s" "http://%s"' % (MasterVHUser,
                FinalPHPPath, MasterPath, MasterDomain.rstrip('/'), MasterDomain.rstrip('/'))
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0
            else:
                LitePagesLogger.SendStatus(stdout, self.JobID, 1, 0, 0, 0)

            shutil.rmtree(self.TempPath)

            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 0
        except BaseException as msg:
            shutil.rmtree(self.TempPath)
            LitePagesLogger.SendStatus('%s [WPManager.startSyncing:893]' % (str(msg)), self.JobID, 1, 1, 0, 0)

            return 0

    # def SaveAutoUpdateSettings(self):
    #     try:
    #         website = Websites.objects.get(domain=self.data['domainName'])
    #         domainName = self.data['domainName']
    #         from cloudAPI.models import WPDeployments
    #
    #         try:
    #             wpd = WPDeployments.objects.get(owner=website)
    #             config = json.loads(wpd.config)
    #         except:
    #             wpd = WPDeployments(owner=website)
    #             config = {}
    #
    #         try:
    #             from cloudAPI.models import WPDeployments
    #             wpd = WPDeployments.objects.get(owner=website)
    #             path = json.loads(wpd.config)['path']
    #             path = '/home/%s/public_html/%s' % (self.data['domain'], path)
    #
    #         except:
    #             path = '/home/%s/public_html' % (self.data['domain'])
    #
    #         config['updates'] = self.data['wpCore']
    #         config['pluginUpdates'] = self.data['plugins']
    #         config['themeUpdates'] = self.data['themes']
    #         wpd.config = json.dumps(config)
    #         wpd.save()
    #
    #         if self.data['wpCore'] == 'Disabled':
    #             command = "wp config set WP_AUTO_UPDATE_CORE false --skip-plugins --skip-themes --raw --path=%s" % (path)
    #             ProcessUtilities.executioner(command, website.externalApp)
    #         elif self.data['wpCore'] == 'Minor and Security Updates':
    #             command = "wp config set WP_AUTO_UPDATE_CORE minor --skip-plugins --skip-themes --allow-root --path=%s" % (path)
    #             ProcessUtilities.executioner(command, website.externalApp)
    #         else:
    #             command = "wp config set WP_AUTO_UPDATE_CORE true --raw --allow-root --path=%s" % (path)
    #             ProcessUtilities.executioner(command, website.externalApp)
    #
    #         final_json = json.dumps(
    #             {'status': 1, 'message': "Autoupdates configured."})
    #         return HttpResponse(final_json)
    #     except BaseException as msg:
    #         final_dic = {'status': 0, 'fetchStatus': 0, 'error_message': str(msg)}
    #         final_json = json.dumps(final_dic)
    #         return HttpResponse(final_json)

