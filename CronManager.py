import os
import time
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger


class CronManager:

    def __init__(self, data):
        self.data = data

    def addNewCron(self):
        try:
            from random import randint, seed
            try:
                seed(time.perf_counter())
            except:
                pass

            TempFile = str(randint(1000, 9999))
            cronPath = "/var/spool/cron/crontabs/" + self.data['VHUser']
            finalCron = '%s %s' % (self.data['ConRun'], self.data['command'])

            if os.path.exists(cronPath):
                FullCrons = open(cronPath, 'r').read()
                finalCron = '%s%s\n' % (FullCrons, finalCron)
                with open(TempFile, "w") as file:
                    file.write(finalCron)
            else:
                with open(TempFile, "w") as file:
                    file.write(finalCron + '\n')

            lpe = LitePagesExecutioner('')
            lpe.command = 'cp %s %s' % (TempFile, cronPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe = LitePagesExecutioner('')
            lpe.command = 'chmod 600 %s' % (cronPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = 'systemctl restart cron'
            ReturnCode, stdout, stderr = lpe.Execute()

            try:
                os.remove(TempFile)
            except:
                pass

            return 1, None
        except BaseException as msg:
            return 0, str(msg)

    def FetchCronJobs(self):
        try:
            cronPath = "/var/spool/cron/crontabs/" + self.data['VHUser']
            FinalCrons = []

            if os.path.exists(cronPath):
                counter = 0
                for line in open(cronPath, 'r').readlines():
                    if line:
                        split = line.split(" ", 5)
                        if len(split) == 6:
                            FinalCrons.append({"line": counter,
                                               "minute": split[0],
                                               "hour": split[1],
                                               "monthday": split[2],
                                               "month": split[3],
                                               "weekday": split[4],
                                               "command": split[5]})
                            counter += 1

                return 1, FinalCrons
            else:
                return 1, FinalCrons
        except BaseException as msg:
            return 0, str(msg)

    def DeleteCronFinal(self):
        try:
            from random import randint, seed
            try:
                seed(time.perf_counter())
            except:
                pass

            TempFile = str(randint(1000, 9999))
            cronPath = "/var/spool/cron/crontabs/" + self.data['VHUser']

            counter = 0

            if os.path.exists(cronPath):
                FullCrons = open(cronPath, 'r').readlines()
                WriteToFile = open(TempFile, 'w')
                for line in FullCrons:
                    if int(self.data['CronJobID']) == counter:
                        counter += 1
                        continue
                    else:
                        WriteToFile.write(line)
                    counter += 1

                WriteToFile.close()

            lpe = LitePagesExecutioner('')
            lpe.command = 'cp %s %s' % (TempFile, cronPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe = LitePagesExecutioner('')
            lpe.command = 'chmod 600 %s' % (cronPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.command = 'systemctl restart cron'
            ReturnCode, stdout, stderr = lpe.Execute()

            try:
                os.remove(TempFile)
            except:
                pass

            return 1, None
        except BaseException as msg:
            return 0, str(msg)
