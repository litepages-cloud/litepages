import json
import os
import random
import shutil
import time

from WPManager import WPManager
from Logger import LitePagesLogger
from LitePagesExecutioner import LitePagesExecutioner

try:
    import boto3
except:
    lpe = LitePagesExecutioner('')
    lpe.command = "pip3 install boto3"
    lpe.Execute()
    import boto3

try:
    random.seed(time.perf_counter())
except:
    pass

from VirtualHost import VirtualHost


class LPBackups:

    def __init__(self, data=None):
        self.data = data

    def CreateBackup(self):
        try:
            websitepath = self.data["websitepath"]
            websitedomain = self.data['websitedomain']
            websiteVHUser = self.data['Vhost']
            databasename = self.data['DatabaseName']
            WPsitepath = self.data['WPsitepath']
            self.JobID = self.data['JobID']

            lpe = LitePagesExecutioner('')

            ### Set up a secure path for backups

            from WPManager import WPManager

            wpm = WPManager({})
            wpm.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(websitepath).st_uid
            websiteVHUserGID = os.stat(websitepath).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)

            ###

            usr = {}
            wpsites = self.data['WPsites']
            usr['DBusrname'] = self.data['DBusrname']

            config = {}
            config["DomainName"] = websitedomain
            config['backupWebsitepath'] = websitepath
            config["WPsite"] = wpsites
            config["DatabaseName"] = databasename
            config["DatabaseUser"] = []
            config['RandomPath'] = RandomPath

            LitePagesLogger.SendStatus('Fetching database user.. ', self.JobID, 1, 0, 0, 10)

            ## NEEDS CHECK

            for x in usr['DBusrname']:

                lpe.command = """mysql -e 'select password from mysql.user where user="%s"'""" % x['UserName']
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr
                else:
                    if os.path.exists('/var/log/LPDebug'):
                        LitePagesLogger.writeToFile(
                            "get DB USerapassword code---%s: error: %s: " % (ReturnCode, stderr),
                            LitePagesLogger.ERROR, 0)

                    passwd = stdout.rstrip('\n')
                    config["DatabaseUser"].append({x['UserName']: passwd})

            LitePagesLogger.SendStatus('Creating backup directory..', self.JobID, 1, 0, 0, 20)

            ################Make directory for backup

            lpe.command = "sudo -u %s mkdir -p %s/public_html" % (websiteVHUser, TempPath)
            # LitePagesLogger.writeToFile("cmd make directory back:sudo - u %s mkdir -p %s/backup/%s" % (websiteVHUser, websitepath, websitedomain) , LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr
            # LitePagesLogger.writeToFile("mkdir return code---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ###############Create config.Json file
            ###############Create config.Json file

            lpe.command = "sudo -u %s touch %s/config.json" % (websiteVHUser, TempPath)
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cmd:Create config.Json file:sudo - u %s touch %s/backup/config.json" % (
                        websiteVHUser, websitepath),
                    LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            ###### write into config
            json_object = json.dumps(config, indent=4)
            try:

                configPath = "%s/config.json" % (TempPath)
                file = open(configPath, "w")
                file.write(json_object)
                file.close()

                os.chmod(configPath, 0o600)

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile("write into cofig", LitePagesLogger.ERROR, 0)

            except  BaseException as msg:
                LitePagesLogger.SendStatus(str(msg), self.JobID, 1, 1, 0, 0)
                LitePagesLogger.writeToFile(" write eerror---%s:" % str(msg), LitePagesLogger.ERROR, 0)
                return 0, str(msg)

            LitePagesLogger.SendStatus('Copying website data..', self.JobID, 1, 0, 0, 30)

            try:

                ############## Copy Public_htnl to backup

                lpe.command = "sudo -u %s cp -pR %s* %s/public_html" % (websiteVHUser, WPsitepath, TempPath)

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile(
                        "cmd Copy Public_htnl to backup:sudo -u %s cp -R %s* %s/backup/%s/public_html" % (
                            websiteVHUser, WPsitepath, websitepath, websitedomain), LitePagesLogger.ERROR, 0)

                lpe.shell = True
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    lpe.shell = False
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr

                lpe.shell = False
                lpe.command = "sudo -u %s cp -R %s.[^.]* %s/public_html/" % (websiteVHUser, WPsitepath, TempPath)

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile(
                        "cmd Copy Public_htnl to backup:sudo -u %s cp -R %s.[^.]* %s/backup/%s/public_html/" % (
                            websiteVHUser, WPsitepath, websitepath, websitedomain), LitePagesLogger.ERROR, 0)

                lpe.shell = True
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    lpe.shell = False
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)

                lpe.shell = False

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile("Copy Public_html to backup ReutrnCode---%s:" % ReturnCode,
                                                LitePagesLogger.ERROR, 0)
            except BaseException as msg:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            ##### SQLDUMP database into new directory

            # LitePagesLogger.writeToFile("SSL Code start", LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Copying website SSL..', self.JobID, 1, 0, 0, 40)
            try:
                ssltemp = TempPath + "/letsencrypt"

                os.mkdir(ssltemp)
                lpe.shell = True
                lpe.command = "cp -R /etc/letsencrypt/live/%s/* %s" % (websitedomain, ssltemp)
                ReturnCode, stdout, stderr = lpe.Execute()
                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile(
                        "cmd Copy SSL cp /etc/letsencrypt/live/%s %s" % (websitedomain, TempPath), LitePagesLogger.ERROR, 0)
                    LitePagesLogger.writeToFile("Copy SLL ReutrnCode---%s:" % ReturnCode,
                                                LitePagesLogger.ERROR, 0)
                lpe.shell = False

                lpe.command = f"chown -R {websiteVHUser}:{websiteVHUser} {ssltemp}"
                ReturnCode, stdout, stderr = lpe.Execute()

            except BaseException as msg:
                LitePagesLogger.SendStatus(msg, self.JobID, 1, 1, 0, 0)
                return 0, msg

            ## NEEDS CHECK

            try:
                LitePagesLogger.SendStatus('Coyping DataBase..', self.JobID, 1, 0, 0, 45)
                lpe.command = "mysqldump %s --result-file %s/%s.sql" % (databasename, TempPath, databasename)
                LitePagesLogger.writeToFile('MYSQL DUMP:mysqldump %s --result-file %s/%s.sql' % (databasename, TempPath, databasename), LitePagesLogger.ERROR, 0)

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile(
                        "SQLDUMP database into new directory:sudo -u %s mysqldump %s --result-file %s/backup/%s.sql" % (
                            websiteVHUser, databasename, websitepath, databasename), LitePagesLogger.ERROR, 0)
                ReturnCode, stdout, stderr = lpe.Execute()

                if ReturnCode == 0:
                    LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                    return 0, stderr
                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile("sqldump return code---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            except BaseException as msg:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr



            LitePagesLogger.SendStatus('Compressing backup files..', self.JobID, 1, 0, 0, 50)
            ######## Zip backup directory


            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    'Value of website path: %s' % websitepath, LitePagesLogger.ERROR, 0)

            FinalZipPath = '%s/%s.zip' % (websitepath, RandomPath)

            lpe.command = "sudo -u %s tar -czvf %s -P %s" % (websiteVHUser, FinalZipPath, TempPath)
            LitePagesLogger.writeToFile('ZIp cmd:sudo -u %s tar -czvf %s -P %s' % (websiteVHUser, FinalZipPath, TempPath), LitePagesLogger.ERROR, 0)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "zip backup directory:sudo -u %s tar -czvf %s/backup/%s.zip %s/backup/%s" % (
                        websiteVHUser, websitepath, websitedomain, websitepath, websitedomain), LitePagesLogger.ERROR,
                    0)

            # sudo -u websiteo zip -r /home/websitegh/backup/website.gh.zip /home/websitegh/backup/website.gh
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            lpe.shell = False
            LitePagesLogger.writeToFile("zip file return code---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            retDic = {'FinalZipPath': FinalZipPath, 'RandomPath': RandomPath, 'json_object': json_object,
                      'TempPath': TempPath}
            return 1, retDic
        except BaseException as msg:
            return 0, str(msg)

    def CreateLocalBackup(self):
        try:

            websitepath = self.data["websitepath"]
            websitedomain = self.data['websitedomain']
            websiteVHUser = self.data['Vhost']
            databasename = self.data['DatabaseName']
            KeyID = self.data['keyID']
            Scrtkey = self.data['srctkey']
            Endurl = self.data['endpoint']
            backjobID = self.data['bakupjobid']
            WPsitepath = self.data['WPsitepath']
            self.JobID = self.data['JobID']

            status, retData = self.CreateBackup()

            if status:
                FinalZipPath = retData['FinalZipPath']
                RandomPath = retData['RandomPath']
                json_object = retData['json_object']
                TempPath = retData['TempPath']
                self.TempPath = TempPath
                self.FinalZipPath = FinalZipPath
            else:
                if os.path.exists(self.TempPath):
                    shutil.rmtree(self.TempPath)
                return 0, retData

            #################create bucket and upload zip file

            self.config = json.loads(open(LitePagesLogger.LitePagesConfigPathFile, 'r').read())

            IP = LitePagesExecutioner.FetchIPAddress().replace(".", "")

            session = boto3.session.Session()

            client = session.client(
                's3',
                endpoint_url=Endurl,
                aws_access_key_id=KeyID,
                aws_secret_access_key=Scrtkey,
                verify=False
            )
            LitePagesLogger.SendStatus('Creating bucket..', self.JobID, 1, 0, 0, 60)

            try:
                client.create_bucket(Bucket=self.config['id'] + 'z' + IP)
                LitePagesLogger.SendStatus('Bucket Created Successfully', self.JobID, 1, 0, 0, 65)
            except BaseException as msg:
                LitePagesLogger.writeToFile("Create bucket error---%s:" % str(msg), LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus('Creating bucket Error: %s' % str(msg), self.JobID, 1, 0, 0, 0)
                # return 0, str(msg)

            uploadfilename = 'backup-' + websitedomain + "-" + time.strftime("%m.%d.%Y_%H-%M-%S")

            # uploadfilename = 'Backup_%s.zip_%s' % (websitedomain, dt)
            LitePagesLogger.SendStatus('Uploading file to cloud..', self.JobID, 1, 0, 0, 70)

            try:
                res = client.upload_file(Filename=FinalZipPath, Bucket=self.config['id'] + 'z' + IP, Key=uploadfilename)

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile("Uploading file res---%s:" % res, LitePagesLogger.ERROR, 0)

                LitePagesLogger.SendStatus('Backup files Uploaded successfully', self.JobID, 1, 0, 0, 85)
            except BaseException as msg:
                LitePagesLogger.writeToFile("Uploading file error---%s:" % str(msg), LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus('Failed to upload file: Error: %s' % str(msg), self.JobID, 0, 1, 0, 0)
                if os.path.exists(self.TempPath):
                    shutil.rmtree(self.TempPath)
                return 0, str(msg)

            ###################### version id, this only applied to blackbaze
            try:

                s3 = boto3.resource(
                    's3',
                    endpoint_url=Endurl,
                    aws_access_key_id=KeyID,
                    aws_secret_access_key=Scrtkey,
                )

                bucket = self.config['id'] + 'z' + IP
                key = uploadfilename
                versions = s3.Bucket(bucket).object_versions.filter(Prefix=key)
                data = {}

                for version in versions:
                    obj = version.get()
                    LitePagesLogger.writeToFile("VersionId---%s:" % obj.get('VersionId'), LitePagesLogger.ERROR, 0)
                    data['backupVersionId'] = obj.get('VersionId')

                ab = os.path.getsize(FinalZipPath)
                filesize = float(ab) / 1024.0

                LitePagesLogger.SendMessageGeneral('UpdateBackupSpace',
                                                   {'backupsize': filesize, 'Dumpfilename': "%s.sql" % databasename,
                                                    'Filename': uploadfilename, 'config': json_object,
                                                    'backupVersionId': data['backupVersionId'],
                                                    'backupjobsID': backjobID})

                if os.path.exists('/var/log/LPDebug'):
                    LitePagesLogger.writeToFile("upload & send data---", LitePagesLogger.ERROR, 0)
            except BaseException as msg:
                LitePagesLogger.writeToFile("Uploading file error---%s:" % str(msg), LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus("Failed to fetch file data, Error : %s" % str(msg), self.JobID, 0, 1, 0, 0)
                if os.path.exists(self.TempPath):
                    shutil.rmtree(self.TempPath)
                return 0, str(msg)

            #### remove the the main backup directory

            try:
                shutil.rmtree(self.TempPath)
                ##NEEDS CHECK
                os.remove(self.FinalZipPath)
            except:
                pass

            LitePagesLogger.SendStatus('Backup created and uploaded to cloud.', self.JobID, 1, 1, 1, 100)

            return 1, None

        except BaseException as msg:
            try:
                shutil.rmtree(self.TempPath)
                ##NEEDS CHECK
                os.remove(self.FinalZipPath)
            except:
                pass
            LitePagesLogger.writeToFile('%s. [CreateLocalBackup]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('Failed, Error %s.' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def Getdatabasename(self):
        try:
            PHPversion = self.data["PHPversion"]
            VHuser = self.data["VHuser"]
            WPpath = self.data["WPpath"]

            php = VirtualHost.getPHPString(PHPversion)
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("FinalPHPPath---%s:" % FinalPHPPath, LitePagesLogger.ERROR, 0)

            lpe = LitePagesExecutioner('')

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_NAME  --skip-plugins --skip-themes --path=%s' % (
                VHuser, FinalPHPPath, WPpath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cmd :sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_NAME  --skip-plugins --skip-themes --path=%s" % (
                        VHuser, FinalPHPPath, WPpath), LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("fetch DataBaseName---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            if ReturnCode == 0:
                return 0, stderr
            else:
                Finaldbname = stdout

            return 1, Finaldbname
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [Getdatabasename]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)

    ### This runs when the new website is created from scratch

    def RestoreLocalBackup(self):
        try:
            filename = self.data["filename"]
            serverIP = self.data['serverIP']
            RestoreserverIP = self.data['RestoreserverIP']
            serverID = self.data['serverID']
            KeyID = self.data['keyID']
            Scrtkey = self.data['srctkey']
            Endurl = self.data['endpoint']
            OldDomain = self.data['OldDomain']
            newwebpathname = self.data['newwebpathname']
            Newdatabasename = self.data['Newdatabasename']
            Mysqlfilename = self.data['Mysqlfilename']
            NewVHUser = self.data['NewVHUser']
            DB_USER = self.data['DB_USER']
            DB_PASSWORD = self.data['DB_PASSWORD']
            olddomainpath = self.data['olddomainpath']
            newwppath = self.data['newwppath']
            oldFinalurl = self.data['oldFinalurl']
            newurl = self.data['newurl']
            Tempfoldername = self.data['Tempfoldername']
            self.JobID = self.data['JobID']

            Wm = WPManager(None)
            Wm.CheckAndInstallWPCli()

            lpe = LitePagesExecutioner('')

            SIP = serverIP.replace(".", "")
            ID = str(serverID)
            IP = ID + 'z' + SIP

            ### Set up a secure path for backups

            wpm = WPManager({})
            wpm.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(newwebpathname).st_uid
            websiteVHUserGID = os.stat(newwebpathname).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)

            session = boto3.session.Session()

            LitePagesLogger.SendStatus('Downloading %s file..' % filename, self.JobID, 1, 0, 0, 10)

            client = session.client(
                's3',
                endpoint_url=Endurl,
                aws_access_key_id=KeyID,
                aws_secret_access_key=Scrtkey,
                verify=False
            )
            FinalZipPath = '%s/%s.zip' % (TempPath, filename)
            try:

                client.download_file(IP, filename, FinalZipPath)
                os.chown(FinalZipPath, websiteVHUserUID, websiteVHUserGID)
                os.chmod(FinalZipPath, 0o600)
            except BaseException as msg:
                LitePagesLogger.writeToFile("downloadfile error---%s:" % msg, LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus('Unable to Download Backup File error: %s' % msg, self.JobID, 0, 1, 0, 0)
                return 0, str(msg)

            # mak new dir
            lpe.command = "sudo -u %s mkdir %s/ab" % (NewVHUser, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Created new dir", LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Extracting : %s ..' % filename, self.JobID, 1, 0, 0, 30)

            ########## Unzip file

            lpe.command = "sudo -u %s tar -xvf  %s/%s.zip -C %s/ab" % (NewVHUser, TempPath, filename, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Tempfoldername name---%s:" % Tempfoldername, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("UNzip file---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Restoring website..', self.JobID, 1, 0, 0, 50)

            ###### Web site path copy to new website path
            lpe.shell = True
            lpe.command = "sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s " % (
                NewVHUser, TempPath, Tempfoldername, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Copy old path to new web site path---%s:" % ReturnCode,
                                            LitePagesLogger.ERROR, 0)

            lpe.shell = False

            lpe.command = "sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s " % (
                NewVHUser, TempPath, Tempfoldername, newwppath)
            lpe.shell = True
            LitePagesLogger.writeToFile("sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s " % (
                NewVHUser, TempPath, Tempfoldername, newwppath), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Copy old path to new web site path---%s:" % ReturnCode,
                                            LitePagesLogger.ERROR, 0)

            lpe.shell = False

            LitePagesLogger.SendStatus('Restoring database.. ', self.JobID, 1, 0, 0, 70)
            ##### SQLDUMP database
            lpe.command = "mysql -u root %s < %s/ab/var/litepages/secure/%s/%s" % (
                Newdatabasename, TempPath, Tempfoldername, Mysqlfilename)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("restore mysqldump Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("restore mysqldump stdoutput---%s:" % stdout, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("restore mysqldump stderr---%s:" % stderr, LitePagesLogger.ERROR, 0)
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            ### Set DB name

            lpe.command = "sudo -u %s /usr/local/lsws/lsphp74/bin/php /usr/bin/wp config set DB_NAME %s --skip-plugins --skip-themes --path=%s" % (
                NewVHUser, Newdatabasename, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB Name Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("Set DB Name stderr---%s:" % stderr, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(
                    "sudo -u %s /usr/local/lsws/lsphp74/bin/php /usr/bin/wp config set DB_NAME %s --skip-plugins --skip-themes --path=%s" % (
                        NewVHUser, Newdatabasename, newwppath), LitePagesLogger.ERROR, 0)

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            ### Set DB_USER

            lpe.command = "sudo -u %s /usr/local/lsws/lsphp74/bin/php /usr/bin/wp config set DB_USER %s --skip-plugins --skip-themes --path=%s" % (
                NewVHUser, DB_USER, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB USer Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ### Set DB password

            lpe.command = "sudo -u %s /usr/local/lsws/lsphp74/bin/php /usr/bin/wp config set DB_PASSWORD %s --skip-plugins --skip-themes --path=%s" % (
                NewVHUser, DB_PASSWORD, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB Pssword Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Replacing URL..', self.JobID, 1, 0, 0, 90)
            #### replace urls

            lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --path=%s "%s" "%s"' % (
                NewVHUser, newwppath, oldFinalurl, newurl)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Return code simple recplce url ---%s:" % ReturnCode, LitePagesLogger.ERROR,
                                            0)
                LitePagesLogger.writeToFile("STderr code simple recplce url ---%s:" % stderr, LitePagesLogger.ERROR, 0)

            lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --allow-root --path=%s "https://www.%s" "http://%s"' % (
                NewVHUser, newwppath, oldFinalurl, newurl)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Return code www recplce url ---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("STderr code www recplce url ---%s:" % stderr, LitePagesLogger.ERROR, 0)

            shutil.rmtree(TempPath)
            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Restored Completed', self.JobID, 1, 1, 1, 100)

            return 1, None

        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [RestoreLocalBackup]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus(str(msg), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    ## Found wordpress site and final url also match
    def ExRestoreLocalBackup(self):
        try:
            filename = self.data["filename"]
            KeyID = self.data['keyID']
            Scrtkey = self.data['srctkey']
            Endurl = self.data['endpoint']
            VHuser = self.data['RVHuser']
            Rebpathname = self.data['Rebpathname']
            serverIP = self.data['serverIP']
            serverID = self.data['serverID']
            olddomainpath = self.data['olddomainpath']
            OldDomain = self.data['olddomain']
            Mysqlfilename = self.data['Mysqlfilename']
            PHPversion = self.data['PHPversion']
            wpsitepath = self.data['wpsitepath']
            self.JobID = self.data['JobID']
            oldvhuser = self.data['oldvhuser']
            servrIP = self.data['servrIP']
            servrID = self.data['servrID']
            newurl = self.data['newurl']
            oldfurl = self.data['oldfurl']
            Tempfoldername = self.data['Tempfoldername']

            Wm = WPManager(None)
            Wm.CheckAndInstallWPCli()

            lpe = LitePagesExecutioner('')

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("JobID---%s:" % self.JobID, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Getting WordPress data..', self.JobID, 1, 0, 0, 10)
            php = VirtualHost.getPHPString(PHPversion)
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("FinalPHPPath---%s:" % FinalPHPPath, LitePagesLogger.ERROR, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_NAME  --skip-plugins --skip-themes --path=%s' % (
                VHuser, FinalPHPPath, wpsitepath)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cmd : sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_NAME  --skip-plugins --skip-themes --path=%s" % (
                        VHuser, FinalPHPPath, wpsitepath), LitePagesLogger.ERROR, 0)

            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()

            lpe.shell = False

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            Finaldbname = stdout.rstrip("\n")

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("fetch DataBaseName---%s:" % Finaldbname, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("fetch err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_USER  --skip-plugins --skip-themes --path=%s' % (
                VHuser, FinalPHPPath, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("fetch DataBaseName---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            Finaldbuser = stdout.rstrip("\n")

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("fetch DataBaseUser---%s:" % Finaldbuser, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("fetch err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            #
            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_PASSWORD  --skip-plugins --skip-themes --path=%s' % (
                VHuser, FinalPHPPath, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("fetch DataBaseName---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            Finaldbpassword = stdout.rstrip("\n")

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("fetch DataBasePassword---%s:" % Finaldbpassword, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("fetch err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            SIP = servrIP.replace(".", "")
            ID = str(servrID)
            IP = ID + 'z' + SIP
            ### Set up a secure path for backups

            wpm = WPManager({})
            wpm.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(Rebpathname).st_uid
            websiteVHUserGID = os.stat(Rebpathname).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)
            LitePagesLogger.SendStatus('Downloading backup file..', self.JobID, 1, 0, 0, 25)

            session = boto3.session.Session()

            client = session.client(
                's3',
                endpoint_url=Endurl,
                aws_access_key_id=KeyID,
                aws_secret_access_key=Scrtkey,
                verify=False
            )

            FinalZipPath = '%s/%s.zip' % (TempPath, filename)

            try:
                client.download_file(IP, filename, '%s/%s.zip' % (TempPath, filename))
                os.chown(FinalZipPath, websiteVHUserUID, websiteVHUserGID)
                os.chmod(FinalZipPath, 0o600)
            except BaseException as msg:
                LitePagesLogger.SendStatus('Unable to download backup file. Error %s ' % msg, self.JobID, 0, 1, 0, 0)
                LitePagesLogger.writeToFile("Download file error---%s:" % msg, LitePagesLogger.ERROR, 0)
                return 0, str(msg)

            # mak new dir

            lpe.command = "sudo -u %s mkdir %s/ab" % (VHuser, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("created new dir Returncode ----%s" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Extracting data..', self.JobID, 1, 0, 0, 50)
            ########## UNzip file

            lpe.command = "sudo -u %s tar -xvf  %s/%s.zip -C %s/ab" % (VHuser, TempPath, filename, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "UNzip file cmd:sudo -u %s tar -xvf  %s/%s.zip -C %s/ab" % (VHuser, TempPath, filename, TempPath),
                    LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("UNzip file---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Restoring  data', self.JobID, 1, 0, 0, 75)

            ###### Web site path copy to new website path
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cmd cp: sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (
                        VHuser, TempPath, Tempfoldername, wpsitepath), LitePagesLogger.ERROR, 0)
            lpe.shell = True
            lpe.command = "sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (
                VHuser, TempPath, Tempfoldername, wpsitepath)
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cpy cmmmmd: sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s " % (
                        VHuser, TempPath, Tempfoldername, wpsitepath), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Copy old path to new web site path---%s:" % ReturnCode,
                                            LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            lpe.shell = False

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cmd cp: sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s" % (
                        VHuser, TempPath, Tempfoldername, wpsitepath), LitePagesLogger.ERROR, 0)
            lpe.shell = True
            lpe.command = " sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s" % (
                VHuser, TempPath, Tempfoldername, wpsitepath)
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "cpy cmmmmd:  sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s" % (
                        VHuser, TempPath, Tempfoldername, wpsitepath), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Copy old path to new web site path---%s:" % ReturnCode,
                                            LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            lpe.shell = False

            ##### SQLDUMP database
            lpe.shell = True
            lpe.command = "mysql -u root %s < %s/ab/var/litepages/secure/%s/%s" % (
                Finaldbname, TempPath, Tempfoldername, Mysqlfilename)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("DUMP cmd: mysql -u root %s < %s/ab/var/litepages/secure/%s/%s" % (
                    Finaldbname, TempPath, Tempfoldername, Mysqlfilename), LitePagesLogger.ERROR, 0)

            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("restore mysqldump Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("restore mysqldump Output---%s:" % stdout, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("restore mysqldump stderr---%s:" % stderr, LitePagesLogger.ERROR, 0)

            ### Set DB name

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_NAME %s --skip-plugins --skip-themes --path=%s" % (
                VHuser, FinalPHPPath, Finaldbname, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB Name Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ### Set DB_USER

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_USER %s --skip-plugins --skip-themes --path=%s" % (
                VHuser, FinalPHPPath, Finaldbuser, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB USer Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ### Set DB password

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_PASSWORD %s --skip-plugins --skip-themes --path=%s" % (
                VHuser, FinalPHPPath, Finaldbpassword, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr
            LitePagesLogger.writeToFile("Set DB Pssword Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            #### replace urls
            LitePagesLogger.SendStatus('Replacing Urls', self.JobID, 1, 0, 0, 80)

            lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --path=%s "%s" "%s"' % (
                VHuser, wpsitepath, oldfurl, newurl)
            ReturnCode, stdout, stderr = lpe.Execute()

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Return code simple recplce url ---%s:" % ReturnCode, LitePagesLogger.ERROR,
                                            0)
                LitePagesLogger.writeToFile("STderr code simple recplce url ---%s:" % stderr, LitePagesLogger.ERROR, 0)

            lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --allow-root --path=%s "https://www.%s" "http://%s"' % (
                VHuser, wpsitepath, newurl, newurl)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            shutil.rmtree(TempPath)

            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1, None

        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [ExRestoreLocalBackup]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('Failed to restore. Error: %s' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def NRestoreLocalBackup(self):
        try:
            VHuser = self.data['VHuser']
            PHPversion = self.data['PHPversion']
            serverIP = self.data['serverIP']
            serverID = self.data['serverID']
            Rwebpath = self.data['Rwebpath']
            servrIP = self.data['servrIP']
            servrID = self.data['servrID']
            filename = self.data['filename']
            Scrtkey = self.data['srctkey']
            Endurl = self.data['endpoint']
            KeyID = self.data['KeyID']
            olddomainpath = self.data['olddomainpath']
            OldDomain = self.data['olddomain']
            Mysqlfilename = self.data['Mysqlfilename']
            DB_Name = self.data['DB_Name']
            DB_User = self.data['DB_User']
            DB_Password = self.data['DB_Password']
            newwppath = self.data['newwppath']
            oldFinalurl = self.data['oldFinalurl']
            newurl = self.data['newurl']
            self.JobID = self.data['JobID']
            Tempfoldername = self.data['Tempfoldername']

            Wm = WPManager(None)
            Wm.CheckAndInstallWPCli()

            lpe = LitePagesExecutioner('')
            php = VirtualHost.getPHPString(PHPversion)
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("FinalPHPPath---%s:" % FinalPHPPath, LitePagesLogger.ERROR, 0)

            SIP = servrIP.replace(".", "")
            ID = str(servrID)
            IP = ID + 'z' + SIP

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("JobID---%s:" % self.JobID, LitePagesLogger.ERROR, 0)

            wpm = WPManager({})
            wpm.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(Rwebpath).st_uid
            websiteVHUserGID = os.stat(Rwebpath).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)

            LitePagesLogger.SendStatus('Downloading file..', self.JobID, 1, 0, 0, 10)

            ################Make directory for backup

            session = boto3.session.Session()

            LitePagesLogger.SendStatus('Downloading %s' % filename, self.JobID, 1, 0, 0, 10)

            client = session.client(
                's3',
                endpoint_url=Endurl,
                aws_access_key_id=KeyID,
                aws_secret_access_key=Scrtkey,
                verify=False
            )

            FinalZipPath = '%s/%s.zip' % (TempPath, filename)

            try:
                client.download_file(IP, filename, '%s/%s.zip' % (TempPath, filename))
                os.chown(FinalZipPath, websiteVHUserUID, websiteVHUserGID)
                os.chmod(FinalZipPath, 0o600)
            except BaseException as msg:
                LitePagesLogger.writeToFile("downloadfile error---%s:" % msg, LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus('Unable to Download Backup File error: %s' % msg, self.JobID, 0, 1, 0, 0)
                return 0, str(msg)

            # mak new dir

            lpe.command = "sudo -u %s mkdir %s/ab" % (VHuser, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("created new dir", LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Extracting data', self.JobID, 1, 0, 0, 30)

            ########## UNzip file

            lpe.command = "sudo -u %s tar -xvf  %s/%s.zip -C %s/ab" % (VHuser, TempPath, filename, TempPath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr
            # LitePagesLogger.writeToFile("UNzip file cmd:sudo -u %s tar -xvf  %s/backup/%s.zip -C %s/backup/ab" % (VHuser, Rwebpath, filename, Rwebpath), LitePagesLogger.ERROR, 0)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("UNzip file---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Restoring website..', self.JobID, 1, 0, 0, 50)

            ###### Web site path copy to new website path
            lpe.shell = True
            lpe.command = "sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (
                VHuser, TempPath, Tempfoldername, newwppath)
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("CMD: sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (
                    VHuser, TempPath, Tempfoldername, newwppath), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Copy old path to new web site path---%s:" % ReturnCode,
                                            LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            lpe.shell = False

            lpe.shell = True
            lpe.command = " sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s" % (
                VHuser, TempPath, Tempfoldername, newwppath)
            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(
                    "CMD:sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s" % (
                        VHuser, TempPath, Tempfoldername, newwppath), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 0, 0, 0)

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Copy old path to new web site path---%s:" % ReturnCode,
                                            LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(" err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            lpe.shell = False

            ##### SQLDUMP database
            lpe.shell = True
            lpe.command = "mysql -u root %s < %s/ab/var/litepages/secure/%s/%s" % (
                DB_Name, TempPath, Tempfoldername, Mysqlfilename)
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("restore mysqldump Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('Restoring database..', self.JobID, 1, 0, 0, 70)

            ### Set DB name

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_NAME %s --skip-plugins --skip-themes --path=%s" % (
                VHuser, FinalPHPPath, DB_Name, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB Name Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ### Set DB_USER

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_USER %s --skip-plugins --skip-themes --path=%s" % (
                VHuser, FinalPHPPath, DB_User, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB USer Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ### Set DB password

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_PASSWORD %s --skip-plugins --skip-themes --path=%s" % (
                VHuser, FinalPHPPath, DB_Password, newwppath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB Pssword Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            #### replace urls
            LitePagesLogger.SendStatus('Replacing Urls', self.JobID, 1, 0, 0, 80)

            lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --path=%s "%s" "%s"' % (
                VHuser, newwppath, oldFinalurl, newurl)
            ReturnCode, stdout, stderr = lpe.Execute()

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Return code simple recplce url ---%s:" % ReturnCode, LitePagesLogger.ERROR,
                                            0)
                LitePagesLogger.writeToFile("STderr code simple recplce url ---%s:" % stderr, LitePagesLogger.ERROR, 0)

            lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php -d error_reporting=0 /usr/bin/wp search-replace --skip-plugins --skip-themes --allow-root --path=%s "https://www.%s" "http://%s"' % (
                VHuser, newwppath, newurl, newurl)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Return code www recplce url ---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile("STderr code www recplce url ---%s:" % stderr, LitePagesLogger.ERROR, 0)

            shutil.rmtree(TempPath)
            LitePagesExecutioner.RestartServer()
            LitePagesLogger.SendStatus('Compeleted', self.JobID, 1, 1, 1, 100)

            return 1, None

        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [NRestoreLocalBackup]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus(str(msg), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def RestartLSWS(self):
        try:
            LitePagesExecutioner.RestartServer()
            LitePagesLogger.writeToFile("Restart LiteSpeed done:", LitePagesLogger.ERROR, 0)
            return 1, None
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [RestartLSWS]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)

    def WPSiteMove(self):
        try:
            redata = {}
            VHusr = self.data["Vhost"]
            Webpath = self.data["websitepath"]
            PHPversion = self.data['PHPversion']
            WPsitepath=self.data['WPsitepath']
            wpsitepath = self.data['Oldwpsitepath']
            DatabaseName = self.data['DatabaseName']
            websitedomain = self.data['websitedomain']
            self.JobID = self.data['JobID']
            LitePagesLogger.writeToFile("WPSiteMoveStart:", LitePagesLogger.ERROR, 0)

            status, retData = self.CreateBackup()


            if status == 0 or status == '0':
                LitePagesLogger.SendStatus('%s' % (str(retData)), self.JobID, 1, 1, 0, 0)
                LitePagesLogger.writeToFile("MOvesite(CreateBackup)  retData%s" % retData, LitePagesLogger.ERROR, 0)

            LitePagesLogger.writeToFile("MOvesite(CreateBackup) status %s" % status, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("MOvesite(CreateBackup)  retData%s" % retData, LitePagesLogger.ERROR, 0)

            if status:
                FinalZipPath = retData['FinalZipPath']
                RandomPath = retData['RandomPath']
                json_object = retData['json_object']
                TempPath = retData['TempPath']
            else:
                return 0, retData

            Wm = WPManager(None)
            Wm.CheckAndInstallWPCli()

            lpe = LitePagesExecutioner('')

            php = VirtualHost.getPHPString(PHPversion)
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)
            LitePagesLogger.writeToFile("FinalPHPPath---%s:" % FinalPHPPath, LitePagesLogger.ERROR, 0)

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_NAME  --skip-plugins --skip-themes --path=%s' % (
                VHusr, FinalPHPPath, wpsitepath)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode DataBaseName---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            lpe.shell = False
            Finaldbname = stdout.rstrip("\n")
            LitePagesLogger.writeToFile("fetch DataBaseName---%s:" % Finaldbname, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("fetch err---%s:" % stderr, LitePagesLogger.ERROR, 0)

            redata["DBName"] = Finaldbname

            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_USER  --skip-plugins --skip-themes --path=%s' % (
                VHusr, FinalPHPPath, wpsitepath)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode DataBaseUser---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            lpe.shell = False
            Finaldbuser = stdout.rstrip("\n")
            LitePagesLogger.writeToFile("fetch DataBaseUser---%s:" % Finaldbuser, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("fetch err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            redata['DB_user'] = Finaldbuser
            #
            lpe.command = 'sudo -u %s %s -d error_reporting=0 /usr/bin/wp config get DB_PASSWORD  --skip-plugins --skip-themes --path=%s' % (
                VHusr, FinalPHPPath, wpsitepath)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode Finaldbpwd---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)
            lpe.shell = False
            Finaldbpwd = stdout.rstrip("\n")
            LitePagesLogger.writeToFile("fetch Finaldbpwd---%s:" % Finaldbpwd, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("fetch err---%s:" % stderr, LitePagesLogger.ERROR, 0)
            redata["DB_Passwd"] = Finaldbpwd

            redata["RandomPath"] = RandomPath
            redata["TempPath"] = TempPath

            lpe = LitePagesExecutioner('')

            LitePagesLogger.SendStatus('Getting SSH key..', self.JobID, 1, 0, 0, 55)


            lpe.command = "sudo -u %s ssh-keygen -f %s/.ssh/id_rsa -t rsa -N ''" % (VHusr, Webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of KEYGEN ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            lpe.command = "cat %s/.ssh/id_rsa.pub" % Webpath
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode read Key ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            stdout = stdout.rstrip("\n")
            LitePagesLogger.writeToFile("Out read Key ...:%s" % stdout, LitePagesLogger.ERROR, 0)
            if ReturnCode == 0:
                return 0, stderr
            else:
                keyval = stdout

            redata["Keyval"] = keyval

            LitePagesLogger.writeToFile("retun dats is ----%s:" % redata, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("WPSiteMoveEnd..:", LitePagesLogger.ERROR, 0)



            LitePagesLogger.SendStatus('Move completed from server A:%s' % (json.dumps(redata)), self.JobID, 1, 0, 0, 55)



        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [WPSiteMove]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('%s' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def CopyFile(self):
        try:
            VHusr = self.data["VHUser"]
            Webpath = self.data["websitepath"]
            Filename = self.data["Filename"]
            TempPath = self.data["TempPath"]
            Newserveruser = self.data["Newserveruser"]
            NewserverIP = self.data["NewserverIP"]
            self.JobID = self.data['JobID']

            LitePagesLogger.SendStatus('Copying  data', self.JobID, 1, 0, 0, 60)
            lpe = LitePagesExecutioner('')
            lpe.command = "sudo -u %s scp -o StrictHostKeyChecking=no -P 22 -i %s/.ssh/id_rsa %s/%s.zip %s@%s:~" % (
                VHusr, Webpath, Webpath, Filename, Newserveruser, NewserverIP)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of copying file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            try:
                shutil.rmtree(TempPath)
            except:
                pass

            return 1, None
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [CopyFile]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('Failed, Error %s.' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def RestoreCopy(self):
        try:
            VHusr = self.data["VHUser"]
            Webpath = self.data["webpth"]
            Filename = self.data["filename"]
            domain = self.data["domain"]
            wpsitepath = self.data["wpsitepath"]
            Mysqlfilename = self.data["DumpFilename"]
            Newdatabasename = self.data["Newdatabasename"]
            NewdatabaseUser = self.data["NewdatabaseUser"]
            PHPversion = self.data["PHPVersion"]
            self.JobID = self.data['JobID']

            Wm = WPManager(None)
            Wm.CheckAndInstallWPCli()

            LitePagesLogger.writeToFile("Job ID...:%s" % self.JobID, LitePagesLogger.ERROR, 0)


            Mysqlfilename = Mysqlfilename + ".sql"
            lpe = LitePagesExecutioner('')


            php = VirtualHost.getPHPString(PHPversion)
            FinalPHPPath = '/usr/local/lsws/lsphp%s/bin/php' % (php)

            ## Mack dir
            lpe.command = "sudo -u %s mkdir %s/ab" % (VHusr, Webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of Make Dir ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Extracting  data', self.JobID, 1, 0, 0, 70)

            # unzip file
            lpe.command = "sudo -u %s tar -xvf  %s/%s.zip -C %s/ab" % (VHusr, Webpath, Filename, Webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of unzip ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("stderr of unzip ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Copying  data', self.JobID, 1, 0, 0, 80)

            ###Copy file
            lpe.shell = True
            lpe.command = "sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (
                VHusr, Webpath, Filename, wpsitepath)
            LitePagesLogger.writeToFile("sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (
                VHusr, Webpath, Filename, wpsitepath), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            LitePagesLogger.writeToFile("ReturnCode of copy ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("stderr of copy ...:%s" % stderr, LitePagesLogger.ERROR, 0)



            #copy SSL
            lpe.command = "sudo -u %s mkdir /etc/letsencrypt/live/%s" % (VHusr, domain)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of Make Dir ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            lpe.shell = True
            lpe.command = "cp -R %s/ab/var/litepages/secure/%s/letsencrypt/* /etc/letsencrypt/live/%s/" % (Webpath, Filename, domain)
            LitePagesLogger.writeToFile("cp -R %s/ab/var/litepages/secure/letsencrypt/* /etc/letsencrypt/live/%s/" % (Webpath, domain), LitePagesLogger.ERROR, 0)
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            LitePagesLogger.writeToFile("ReturnCode of copy SSL...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("stderr of copy SSL...:%s" % stderr, LitePagesLogger.ERROR, 0)


            # lpe.command = f"chown"



            lpe.shell = True

            lpe.command = " sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/.[^.]* %s" % (
                VHusr, Webpath, Filename, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of copy[] ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("stderr of copy[] ...:%s" % stderr, LitePagesLogger.ERROR, 0)
            lpe.shell = False

            LitePagesLogger.SendStatus('Restoring  DataBase', self.JobID, 1, 0, 0, 90)

            ### Set up a secure path for dump file

            wpm = WPManager({})
            wpm.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(wpsitepath).st_uid
            websiteVHUserGID = os.stat(wpsitepath).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)

            ###copy dum to secur folder
            lpe.command = " sudo -u %s cp -R %s/ab/var/litepages/secure/%s/%s %s" % (
                VHusr, Webpath, Filename, Mysqlfilename, TempPath)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            LitePagesLogger.writeToFile("ReturnCode of copy dump ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("stderr of copy dump ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            ####dump
            lpe.command = "mysql -u root %s < %s/%s" % (Newdatabasename, TempPath, Mysqlfilename)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            LitePagesLogger.writeToFile("ReturnCode of dump ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("stderr of dump ...:%s" % stderr, LitePagesLogger.ERROR, 0)



            ### Set DB name

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_NAME %s --skip-plugins --skip-themes --path=%s" % (
                VHusr, FinalPHPPath, Newdatabasename, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB Name Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)

            ### Set DB_USER

            lpe.command = "sudo -u %s %s /usr/bin/wp config set DB_USER %s --skip-plugins --skip-themes --path=%s" % (
                VHusr, FinalPHPPath, NewdatabaseUser, wpsitepath)
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile("Set DB USer Returncode---%s:" % ReturnCode, LitePagesLogger.ERROR, 0)




            LitePagesLogger.SendStatus('Restored  Database', self.JobID, 1, 0, 0, 95)

            lpe.command = "sudo -u %s rm -r %s/ab" % (VHusr, Webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of delete ab Dir ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            lpe.command = "sudo -u %s rm -r %s/%s.zip" % (VHusr, Webpath, Filename)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of delete Zip file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            shutil.rmtree(TempPath)
            LitePagesExecutioner.RestartServer()

            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1, None

        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [RestoreCopy]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('Failed, Error %s.' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def DeployPhpmyadmin(self):
        try:
            VHuser = self.data["VHUser"]
            webpath = self.data["path"]
            self.JobID = self.data['JobID']

            LitePagesLogger.SendStatus('Deploying PhPmyAdmin..', self.JobID, 1, 0, 0, 50)

            LitePagesLogger.writeToFile("phpmyadmin started:", LitePagesLogger.ERROR, 0)

            lpe = LitePagesExecutioner('')

            # lpe.command =" wget https://files.phpmyadmin.net/phpMyAdmin/5.1.3/phpMyAdmin-5.1.3-all-languages.zip -P /home/phpmyadmin1/public_html"
            lpe.command = "sudo -u %s wget https://files.phpmyadmin.net/phpMyAdmin/5.1.3/phpMyAdmin-5.1.3-all-languages.zip -P %s/public_html" % (
                VHuser, webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of download file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            lpe.command = "sudo -u %s ls %s/public_html/" % (VHuser, webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of ls public_html dir ...:%s" % ReturnCode, LitePagesLogger.ERROR,
                                        0)
            LitePagesLogger.writeToFile("error of ls public_html dir ...:%s" % stderr, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("output of ls public_html dir ...:%s" % stdout, LitePagesLogger.ERROR, 0)
            Donloadfilename = stdout.rstrip("\n")

            # sudo -u phpmyadmin1 unzip /home/phpmyadmin1/public_html/phpMyAdmin-5.1.3-all-languages.zip -d /home/phpmyadmin1/public_html/
            lpe.command = "sudo -u %s unzip %s/public_html/%s -d %s/public_html/" % (
                VHuser, webpath, Donloadfilename, webpath)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of unzip file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("error of unzip file ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            zipfilename = Donloadfilename.rstrip(".zip")
            LitePagesLogger.writeToFile("zipfilename ...:%s" % zipfilename, LitePagesLogger.ERROR, 0)

            # lpe.command="sudo -u %s cp -R %s/ab/var/litepages/secure/%s/public_html/* %s" % (VHuser, TempPath, Tempfoldername, newwppath)
            lpe.command = "sudo -u %s cp -R %s/public_html/%s/* cp -R %s/public_html/" % (
                VHuser, webpath, zipfilename, webpath)
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            lpe.shell = False
            LitePagesLogger.writeToFile("ReturnCode of copy zip data ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("error of copy zip data ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            lpe.command = "sudo -u %s rm -r %s/public_html/%s" % (VHuser, webpath, zipfilename)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of  delete unzip file ...:%s" % ReturnCode, LitePagesLogger.ERROR,
                                        0)
            LitePagesLogger.writeToFile("error of copy delete unzip file ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            lpe.command = "sudo -u %s rm -r %s/public_html/%s" % (VHuser, webpath, Donloadfilename)
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("ReturnCode of  delete zip file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            LitePagesLogger.writeToFile("error of copy delete zip file ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            return 1, None
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [DeployPhpmyadmin]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)


    def setupComposer(self):

        if os.path.exists('composer.sh'):
            os.remove('composer.sh')

        lpe = LitePagesExecutioner('')

        lpe.command = "wget https://cloudpages.cloud/composer.sh"
        lpe.Execute()

        lpe.command = "chmod +x composer.sh"
        lpe.Execute()

        lpe.command = "./composer.sh"
        lpe.Execute()

    def DeployMautic(self):
        try:
            VHuser = self.data["VHUser"]
            webpath = self.data["path"]
            self.JobID = self.data['JobID']
            MDBname = self.data['MDBname']
            MDBUser = self.data['MDBUser']
            MDBPW = self.data['MDBPW']
            MauticPassword = self.data['MauticPassword']
            MauticEmail = self.data['MauticEmail']
            Domain = self.data['Domain']

            if not os.path.exists('/usr/bin/composer'):
                self.setupComposer()

            lpe = LitePagesExecutioner('')

            ### Set up a secure path for backups

            wpm = WPManager({})
            wpm.SecurePath()

            RandomPath = str(random.randint(1000, 9999))

            TempPath = '%s/%s' % (WPManager.SecureTempPath, RandomPath)
            self.TempPath = TempPath

            websiteVHUserUID = os.stat(webpath).st_uid
            websiteVHUserGID = os.stat(webpath).st_uid

            os.mkdir(TempPath)
            os.chown(TempPath, websiteVHUserUID, websiteVHUserGID)

            LitePagesLogger.SendStatus('Downloading Mautic..', self.JobID, 1, 0, 0, 50)
            lpe = LitePagesExecutioner('')

            #lpe.command = "sudo -u %s wget -O %s/4.2.0.zip https://github.com/mautic/mautic/releases/download/4.2.0/4.2.0.zip" % (
            #    VHuser, webpath)
            #ReturnCode, stdout, stderr = lpe.Execute()

            ### replace command with composer install

            lpe.command = f'sudo -u {VHuser} /usr/local/lsws/lsphp80/bin/php /usr/bin/composer create-project mautic/recommended-project:^4 {webpath}/public_html'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr



            # lpe.command = "sudo -u %s unzip %s/4.2.0.zip -d %s/public_html/" % (
            #     VHuser, webpath, webpath)
            # ReturnCode, stdout, stderr = lpe.Execute()
            # LitePagesLogger.writeToFile("ReturnCode of unzip file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
            # LitePagesLogger.writeToFile("error of unzip file ...:%s" % stderr, LitePagesLogger.ERROR, 0)

            LitePagesLogger.SendStatus('Installing Mautic..', self.JobID, 1, 0, 0, 70)

            cwd = os.getcwd()

            os.chdir(f'{webpath}/public_html')

            lpe.command = f"sudo -u {VHuser} /usr/local/lsws/lsphp80/bin/php bin/console mautic:install --db_host='localhost' --db_name='{MDBname}' --db_user='{MDBUser}' --db_password='{MDBPW}' --admin_username='{MauticEmail}' --admin_email='{MauticEmail}' --admin_password='{MauticPassword}' --db_port='3306' http://{Domain} -f"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(stderr, self.JobID, 1, 1, 0, 0)
                return 0, stderr

            os.chdir(cwd)

#             lpe.command = 'touch %s/local.php' % TempPath
#             ReturnCode, stdout, stderr = lpe.Execute()
#             LitePagesLogger.writeToFile("ReturnCode of Create temp dir ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
#
#             json_object = """<?php
# // Example local.php to test install (to adapt of course)
# $parameters = array(
# 	// Do not set db_driver and mailer_from_name as they are used to assume Mautic is installed
# 	'db_host' => 'localhost',
# 	'db_table_prefix' => null,
# 	'db_port' => 3306,
# 	'db_name' => '%s',
# 	'db_user' => '%s',
# 	'db_password' => '%s',
# 	'db_backup_tables' => true,
# 	'db_backup_prefix' => 'bak_',
# 	'admin_email' => '%s',
# 	'admin_password' => '%s',
# 	'mailer_transport' => null,
# 	'mailer_host' => null,
# 	'mailer_port' => null,
# 	'mailer_user' => null,
# 	'mailer_password' => null,
# 	'mailer_api_key' => null,
# 	'mailer_encryption' => null,
# 	'mailer_auth_mode' => null,
# );""" % (MDBname, MDBUser, MDBPW, MauticEmail, MauticPassword)
#
#             configPath = "%s/local.php" % (TempPath)
#             file = open(configPath, "w")
#             file.write(json_object)
#             file.close()
#
#             lpe.command = "sudo -u %s cp %s/local.php %s/public_html/app/config" % (VHuser, TempPath, webpath)
#             ReturnCode, stdout, stderr = lpe.Execute()
#             LitePagesLogger.writeToFile("ReturnCode of Copy from temp to config ...:%s" % ReturnCode,
#                                         LitePagesLogger.ERROR, 0)
#
#             os.chdir('%s/public_html' % webpath)
#
#             lpe.command = 'sudo -u %s /usr/local/lsws/lsphp74/bin/php bin/console mautic:install http://%s -f' % (
#             VHuser, Domain)
#             ReturnCode, stdout, stderr = lpe.Execute()
#             LitePagesLogger.writeToFile("ReturnCode of installing cmd ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)
#
#             lpe.command = "sudo -u %s rm -r %s/4.2.0.zip"
#             ReturnCode, stdout, stderr = lpe.Execute()
#             LitePagesLogger.writeToFile("ReturnCode of remoove zip file ...:%s" % ReturnCode, LitePagesLogger.ERROR, 0)

            from vhost import vhost

            NewDocRoot = f'{webpath}/public_html/docroot'
            vhost.ReplaceDocRoot(None, Domain, NewDocRoot)

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:

                try:

                    ExistingDocRootApache = vhost.FindDocRootOfSiteApache(None, Domain)

                    if ExistingDocRootApache.find('docroot') == -1:
                        NewDocRootApache = f'{ExistingDocRootApache}docroot'
                    else:
                        NewDocRootApache = ExistingDocRootApache

                    if ExistingDocRootApache != None:
                        vhost.ReplaceDocRootApache(None, Domain, NewDocRootApache)
                except:
                    pass

            ### fix incorrect rules in .htaccess of mautic

            if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.ENT:
                htAccessPath = f'{webpath}/docroot/.htaccess'

                lpe.shell = True

                lpe.command = f"sudo -u {VHuser} sed -i '/# Fallback for Apache < 2.4/,/<\/IfModule>/d' {htAccessPath}"
                lpe.Execute()

                lpe.command = f"sudo -u {VHuser} sed -i '/# Apache 2.4+/,/<\/IfModule>/d' {htAccessPath}"
                lpe.Execute()

            from ApacheVhost import ApacheVhost

            lpe.command = f"systemctl restart {ApacheVhost.serviceName}"
            lpe.Execute()

            LitePagesExecutioner.RestartServer()


            shutil.rmtree(TempPath)
            LitePagesExecutioner.RestartServer()

            return 1, None
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [DeployMautic]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)

    def CheckServiceStaus(self):
        try:
            lpe = LitePagesExecutioner('')
            ServicesName = self.data["ServicesName"]
            if(ServicesName == 'Rides'):
                lpe.command = 'ps aux | grep redis'
                lpe.shell = True
                ReturnCode, stdout, stderr = lpe.Execute()
                lpe.shell = False
                ab = stdout.find('redis-server')
                LitePagesLogger.writeToFile('%s. find value [CheckServiceStaus]' % ab,
                                            LitePagesLogger.ERROR, 0)
                if(ab >= 0):
                    return 1, str(ab)
                else:
                    return 0, str(ab)
            return 1
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [CheckServiceStaus]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)

    def InstallService(self):
        try:
            lpe = LitePagesExecutioner('')
            ServicesName = self.data["ServicesName"]
            self.JobID = self.data['JobID']
            LitePagesLogger.SendStatus('Downloading %s..'%ServicesName, self.JobID, 1, 0, 0, 20)
            if (ServicesName == 'Redis'):
                lpe.command = 'apt-get install redis-server -y'
                ReturnCode, stdout, stderr = lpe.Execute()
                LitePagesLogger.writeToFile('%s. ReturnCode of apt get [CheckServiceStaus]' % ReturnCode, LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus('Installing %s..' % ServicesName, self.JobID, 1, 0, 0, 60)
                lpe.command = 'systemctl enable redis'
                ReturnCode, stdout, stderr = lpe.Execute()
                LitePagesLogger.writeToFile('%s. ReturnCode of enable [CheckServiceStaus]' % ReturnCode,
                                            LitePagesLogger.ERROR, 0)
                lpe.command = 'systemctl start redis'
                ReturnCode, stdout, stderr = lpe.Execute()
                LitePagesLogger.writeToFile('%s. ReturnCode of start [CheckServiceStaus]' % ReturnCode,
                                            LitePagesLogger.ERROR, 0)
                LitePagesLogger.SendStatus('Completed', self.JobID, 1, 1, 1, 100)

            return 1
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [InstallService]' % (str(msg)), LitePagesLogger.ERROR, 0)
            LitePagesLogger.SendStatus('Failed InstallService, Error %s.' % (str(msg)), self.JobID, 1, 1, 0, 0)
            return 0, str(msg)

    def FetchRedisdata(self):
        try:
            LitePagesLogger.writeToFile('%FetchRedisdata Function Stated', LitePagesLogger.ERROR, 0)
            lpe = LitePagesExecutioner('')
            passdata = {}
            lpe.command = 'redis-cli info memory'
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile(' ReturnCode of info memory...%s '%ReturnCode, LitePagesLogger.ERROR, 0)
            # LitePagesLogger.writeToFile(' stdout of info memory...%s  '%stdout, LitePagesLogger.ERROR, 0)

            memdata = stdout
            d = memdata.split()

            passdata['used_memory_human'] = d[3]
            passdata['total_system_memory_human'] = d[17]
            passdata['used_memory_peak_human'] = d[7]

            # # lpe.command = 'redis-cli info commandstats'
            # # ReturnCode, stdout, stderr = lpe.Execute()
            # # LitePagesLogger.writeToFile(' ReturnCode of info memory...%s ' % ReturnCode, LitePagesLogger.ERROR, 0)
            #
            # Performance= stdout
            # per = Performance.split()
            # passdata['perv'] = per

            lpe.command = 'redis-cli info stats'
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile(' ReturnCode of info memory...%s ' % ReturnCode, LitePagesLogger.ERROR, 0)

            cachehits= stdout
            cache = cachehits.split()

            passdata['total_connections_received']= cache[2]
            passdata['total_commands_processed']= cache[3]
            passdata['expired_keys']= cache[13]
            passdata['evicted_keys']= cache[16]



            return 1, json.dumps(passdata)
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [FetchRedisdata]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)

    def ClearRedisCache(self):
        try:
            LitePagesLogger.writeToFile('ClearRedisCache Function Stated', LitePagesLogger.ERROR, 0)
            lpe = LitePagesExecutioner('')
            lpe.command = 'redis-cli flushall async'
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile(' ReturnCode of ClearRedisCache...%s ' % ReturnCode, LitePagesLogger.ERROR, 0)
            if (ReturnCode == 1):
                return 1, None
            else:
                return 0, stderr

        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [ClearRedisCache]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)



    def RestartCurrentServer(self):
        try:
            lpe = LitePagesExecutioner('')
            lpe.command = 'reboot'
            ReturnCode, stdout, stderr = lpe.Execute()
            LitePagesLogger.writeToFile("Restart Server done:", LitePagesLogger.ERROR, 0)
            return 1, None
        except BaseException as msg:
            LitePagesLogger.writeToFile('%s. [RestartLSWS]' % (str(msg)), LitePagesLogger.ERROR, 0)
            return 0, str(msg)



