#!/usr/local/CyberCP/bin/python
import os
import os.path
from os import path
from datetime import datetime
import OpenSSL
from Logger import LitePagesLogger
from VirtualHost import VirtualHost

class Renew:


    def SSLObtainer(self):
        try:
            LitePagesLogger.writeToFile('Running SSL Renew Utility', LitePagesLogger.ERROR, 0)

            ## For websites

            for website in os.listdir(VirtualHost.VhostConfPath):

                if website.endswith('conf'):

                    DomainName = website.rstrip('.conf')
                    LitePagesLogger.writeToFile('Checking SSL for %s.' % (DomainName), 0)
                    filePath = '/etc/letsencrypt/live/%s/fullchain.pem' % (DomainName)
                    DataToSend = {'JobID': 0, 'domain': DomainName, 'path': '/home/%s' % DomainName, 'VHUser': 'root',
                                  'PHPVersion': 'PHP 7.4', 'email': 'support@cloudpages.cloud'}

                    if path.exists(filePath):
                        LitePagesLogger.writeToFile('SSL exists for %s. Checking if SSL will expire in 15 days..' % (DomainName), 0)
                        x509 = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM,
                                                               open(filePath, 'r').read())
                        expireData = x509.get_notAfter().decode('ascii')
                        finalDate = datetime.strptime(expireData, '%Y%m%d%H%M%SZ')
                        now = datetime.now()
                        diff = finalDate - now

                        print(x509.get_issuer().get_components()[1][1].decode('utf-8'))

                        if x509.get_issuer().get_components()[1][1].decode('utf-8') == 'Denial':
                            LitePagesLogger.writeToFile(
                                'SSL exists for %s and ready to renew..' % (DomainName), 0)
                            LitePagesLogger.writeToFile(
                                'Renewing SSL for %s..' % (DomainName), 0)

                            vh = VirtualHost(DataToSend)
                            vh.IssueFreeSSL()
                        elif int(diff.days) >= 15:
                            LitePagesLogger.writeToFile(
                                'SSL exists for %s and is not ready to renew, skipping..' % (DomainName), 0)
                        elif x509.get_issuer().get_components()[1][1].decode('utf-8') != "Let's Encrypt":
                            LitePagesLogger.writeToFile(
                                'Custom SSL exists for %s and ready to renew..' % (DomainName), 1)
                        else:
                            LitePagesLogger.writeToFile(
                                'SSL exists for %s and ready to renew..' % (DomainName), 0)
                            LitePagesLogger.writeToFile(
                                'Renewing SSL for %s..' % (DomainName), 0)

                            vh = VirtualHost(DataToSend)
                            vh.IssueFreeSSL()

                    else:
                        LitePagesLogger.writeToFile(
                            'SSL does not exist for %s. Obtaining now..' % (DomainName), 0)
                        vh = VirtualHost(DataToSend)
                        vh.IssueFreeSSL()

        except BaseException as msg:
           LitePagesLogger.writeToFile(str(msg) + '. Renew.SSLObtainer', 0)


if __name__ == "__main__":
    sslOB = Renew()
    sslOB.SSLObtainer()