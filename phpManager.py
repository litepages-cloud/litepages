import json
import re

from random import randint
# from .models import *
from xml.etree import ElementTree

from LitePagesExecutioner import LitePagesExecutioner


class PHPManager:

    def __init__(self, data):
        self.data = data

    @staticmethod
    def findPHPVersions():
        #distro = LitePagesExecutioner.DecideWebServer()

        #return ['PHP 7.0', 'PHP 7.1', 'PHP 7.2', 'PHP 7.3', 'PHP 7.4', 'PHP 8.0', 'PHP 8.1']
        try:

            lpe = LitePagesExecutioner('')
            lpe.shell = True

            lpe.command = f'ls -la /usr/local/lsws'
            ReturnCode, stdout, stderr = lpe.Execute()

            # Get the lines containing 'lsphp' in the output
            lsphp_lines = [line for line in stdout.split('\n') if 'lsphp' in line]

            # Extract the version from the lines and format it as 'PHP x.y'
            php_versions = ['PHP ' + line.split()[8][5] + '.' + line.split()[8][6:] for line in lsphp_lines]

            # Now php_versions contains the formatted PHP versions
            return ReturnCode, php_versions
        except BaseException as msg:
            return 1, ['PHP 7.0', 'PHP 7.1', 'PHP 7.2', 'PHP 7.3', 'PHP 7.4', 'PHP 8.0', 'PHP 8.1', 'PHP 8.2']

    @staticmethod
    def getPHPString(phpVersion):
        # Ex: "PHP 5.3" type string, return Ex: "53" type string
        phpVersion = phpVersion.split()
        php = phpVersion[1].replace(".", "")

        return php

    @staticmethod
    def FindPHPFPMPath(phpVersion):
        if phpVersion == "PHP 5.3":
            return "/opt/remi/php54/root/etc/php.ini"
        elif phpVersion == "PHP 5.4":
            return "/opt/remi/php54/root/etc/php.ini"
        elif phpVersion == "PHP 5.5":
            return "/opt/remi/php55/root/etc/php.ini"
        elif phpVersion == "PHP 5.6":
            return "/etc/opt/remi/php56/php.ini"
        elif phpVersion == "PHP 7.0":
            return "/etc/opt/remi/php70/php.ini"
        elif phpVersion == "PHP 7.1":
            return "/etc/opt/remi/php71/php.ini"
        elif phpVersion == "PHP 7.2":
            return "/etc/opt/remi/php72/php.ini"
        elif phpVersion == "PHP 7.3":
            return "/etc/opt/remi/php73/php.ini"

    @staticmethod
    def getCurrentPHPConfig(phpVersion):
        allow_url_fopen = "0"
        display_errors = "0"
        file_uploads = "0"
        allow_url_include = "0"
        memory_limit = ""
        max_execution_time = ""
        upload_max_filesize = ""
        max_input_time = ""

        lpe = LitePagesExecutioner('')

        lpe.command = "cat " + PHPManager.FindPHPFPMPath(phpVersion)
        ReturnCode, stdout, stderr = lpe.Execute()

        data = stdout.split('\n')

        for items in data:
            if items.find("allow_url_fopen") > -1 and items.find("=") > -1:
                if items.find("On") > -1:
                    allow_url_fopen = "1"
            if items.find("display_errors") > -1 and items.find("=") > -1:
                if items.find("On") > -1:
                    display_errors = "1"
            if items.find("file_uploads") > -1 and items.find("=") > -1:
                if items.find("On") > -1:
                    file_uploads = "1"
            if items.find("allow_url_include") > -1 and items.find("=") > -1:
                if items.find("On") > -1:
                    allow_url_include = "1"
            if items.find("memory_limit") > -1 and items.find("=") > -1:
                memory_limit = re.findall(r"[A-Za-z0-9_]+", items)[1]
            if items.find("max_execution_time") > -1 and items.find("=") > -1:
                max_execution_time = re.findall(r"[A-Za-z0-9_]+", items)[1]
            if items.find("upload_max_filesize") > -1 and items.find("=") > -1:
                upload_max_filesize = re.findall(r"[A-Za-z0-9_]+", items)[1]
            if items.find("max_input_time") > -1 and items.find("=") > -1:
                max_input_time = re.findall(r"[A-Za-z0-9_]+", items)[1]
            if items.find("post_max_size") > -1 and items.find("=") > -1:
                post_max_size = re.findall(r"[A-Za-z0-9_]+", items)[1]

        final_dic = {'fetchStatus': 1,
                     'allow_url_fopen': allow_url_fopen,
                     'display_errors': display_errors,
                     'file_uploads': file_uploads,
                     'allow_url_include': allow_url_include,
                     'memory_limit': memory_limit,
                     'max_execution_time': max_execution_time,
                     'upload_max_filesize': upload_max_filesize,
                     'max_input_time': max_input_time,
                     'post_max_size': post_max_size,
                     'status': 1}

        final_json = json.dumps(final_dic)

        return final_json

    def getExtensionsInformation(self):

        phpVers = self.data['phpSelection']

        if self.data['apache'] == None:
            phpVers = f"lsphp{PHPManager.getPHPString(phpVers)}"
        else:
            # if ProcessUtilities.decideDistro() == ProcessUtilities.centos or ProcessUtilities.decideDistro() == ProcessUtilities.cent8:
            #     phpVers = f"php{PHPManager.getPHPString(phpVers)}"
            # else:
            #     phpVers = phpVers.replace(' ', '').lower()

            phpVers = phpVers.replace(' ', '').lower()
            # phpVers = phpVers.replace('.', '').lower()

        # php = PHP.objects.get(phpVers=phpVers)

        # if os.path.exists('/etc/lsb-release'):
        #     command = f'apt list | grep {phpVers}'
        # else:
        #     command = 'yum list installed'
        #     resultInstalled = ProcessUtilities.outputExecutioner(command)
        #
        #     command = f'yum list | grep ^{phpVers} | xargs -n3 | column -t'

        lpe = LitePagesExecutioner('')
        lpe.shell = True

        lpe.command = f'apt list | grep {phpVers}'
        ReturnCode, stdout, stderr = lpe.Execute()
        lpe.shell = False
        result = stdout.split('\n')

        try:
            json_data = "["
            checker = 0
            counter = 1

            for items in result:
                if items.find(phpVers) > -1:
                    if items.find('installed') == -1:
                        status = "Not-Installed"
                    else:
                        status = "Installed"

                    dic = {'id': counter,
                           'phpVers': phpVers,
                           'extensionName': items.split('/')[0],
                           'description': items,
                           'status': status
                           }

                    if checker == 0:
                        json_data = json_data + json.dumps(dic)
                        checker = 1
                    else:
                        json_data = json_data + ',' + json.dumps(dic)
                    counter += 1

            json_data = json_data + ']'

            return 1, json_data
        except BaseException as msg:
            return 0, msg

    def installPHPExtension(self):
        # mailUtilities.checkHome()
        #
        # if ProcessUtilities.decideDistro() == ProcessUtilities.centos or ProcessUtilities.decideDistro() == ProcessUtilities.cent8:
        #     command = 'sudo yum install ' + extension + ' -y'
        # else:
        #     command = 'sudo apt-get install ' + extension + ' -y'

        lpe = LitePagesExecutioner('')

        lpe.command = command = 'sudo apt-get install ' + self.data['extension'] + ' -y'
        ReturnCode, stdout, stderr = lpe.Execute()

        return ReturnCode, stdout

    def unInstallPHPExtension(self):
        # mailUtilities.checkHome()
        #
        # if ProcessUtilities.decideDistro() == ProcessUtilities.centos or ProcessUtilities.decideDistro() == ProcessUtilities.cent8:
        #     command = 'sudo yum install ' + extension + ' -y'
        # else:
        #     command = 'sudo apt-get install ' + extension + ' -y'

        lpe = LitePagesExecutioner('')

        lpe.command = command = 'sudo apt-get remove ' + self.data['extension'] + ' -y'
        ReturnCode, stdout, stderr = lpe.Execute()

        return ReturnCode, stdout







    # @staticmethod
    # def savePHPConfigBasic(data):
    #     phpVersion = data['phpVersion']
    #     allow_url_fopen = data['allow_url_fopen']
    #     display_errors = data['display_errors']
    #     file_uploads = data['file_uploads']
    #     allow_url_include = data['allow_url_include']
    #     memory_limit = data['memory_limit']
    #     max_execution_time = data['max_execution_time']
    #     upload_max_filesize = data['upload_max_filesize']
    #     max_input_time = data['max_input_time']
    #     post_max_size = data['post_max_size']
    #
    #     if allow_url_fopen:
    #         allow_url_fopen = "allow_url_fopen = On"
    #     else:
    #         allow_url_fopen = "allow_url_fopen = Off"
    #
    #     if display_errors:
    #         display_errors = "display_errors = On"
    #     else:
    #         display_errors = "display_errors = Off"
    #
    #     if file_uploads:
    #         file_uploads = "file_uploads = On"
    #     else:
    #         file_uploads = "file_uploads = Off"
    #
    #     if allow_url_include:
    #         allow_url_include = "allow_url_include = On"
    #     else:
    #         allow_url_include = "allow_url_include = Off"
    #
    #     path = PHPManager.FindPHPFPMPath(phpVersion)
    #     command = "cat " + path
    #     data = ProcessUtilities.outputExecutioner(command).splitlines()
    #
    #     tempStatusPath = "/home/cyberpanel/" + str(randint(1000, 9999))
    #
    #     writeToFile = open(tempStatusPath, 'w')
    #
    #     for items in data:
    #         if items.find("allow_url_fopen") > -1 and items.find("=") > -1:
    #             writeToFile.writelines(allow_url_fopen + "\n")
    #         elif items.find("display_errors") > -1 and items.find("=") > -1:
    #             writeToFile.writelines(display_errors + "\n")
    #         elif items.find("file_uploads") > -1 and items.find("=") > -1 and not items.find(
    #                 "max_file_uploads") > -1:
    #             writeToFile.writelines(file_uploads + "\n")
    #         elif items.find("allow_url_include") > -1 and items.find("=") > -1:
    #             writeToFile.writelines(allow_url_include + "\n")
    #
    #         elif items.find("memory_limit") > -1 and items.find("=") > -1:
    #             writeToFile.writelines("memory_limit = " + memory_limit + "\n")
    #
    #         elif items.find("max_execution_time") > -1 and items.find("=") > -1:
    #             writeToFile.writelines("max_execution_time = " + max_execution_time + "\n")
    #
    #         elif items.find("upload_max_filesize") > -1 and items.find("=") > -1:
    #             writeToFile.writelines("upload_max_filesize = " + upload_max_filesize + "\n")
    #
    #         elif items.find("max_input_time") > -1 and items.find("=") > -1:
    #             writeToFile.writelines("max_input_time = " + max_input_time + "\n")
    #         elif items.find("post_max_size") > -1 and items.find("=") > -1:
    #             writeToFile.writelines("post_max_size = " + post_max_size + "\n")
    #         else:
    #             writeToFile.writelines(items + '\n')
    #
    #     writeToFile.close()
    #
    #     command = "mv %s %s" % (tempStatusPath, path)
    #     ProcessUtilities.executioner(command)
    #
    #     php = PHPManager.getPHPString(phpVersion)
    #
    #     command = "systemctl stop php%s-php-fpm" % (php)
    #     ProcessUtilities.executioner(command)
    #
    #     command = "systemctl start php%s-php-fpm" % (php)
    #     ProcessUtilities.executioner(command)
    #
    #     final_dic = {'status': 1}
    #     final_json = json.dumps(final_dic)
    #     return HttpResponse(final_json)

    # @staticmethod
    # def fetchPHPSettingsAdvance(phpVersion):
    #     command = "cat " + PHPManager.FindPHPFPMPath(phpVersion)
    #     data = ProcessUtilities.outputExecutioner(command)
    #     final_dic = {'fetchStatus': 1,
    #                  'configData': data,
    #                  'status': 1}
    #
    #     final_json = json.dumps(final_dic)
    #
    #     return HttpResponse(final_json)

    # @staticmethod
    # def savePHPConfigAdvance(data):
    #     phpVersion = data['phpVersion']
    #     configData = data['configData']
    #
    #     path = PHPManager.FindPHPFPMPath(phpVersion)
    #
    #     tempStatusPath = "/home/cyberpanel/" + str(randint(1000, 9999))
    #
    #     writeToFile = open(tempStatusPath, 'w')
    #     writeToFile.write(configData)
    #     writeToFile.close()
    #
    #     command = "mv %s %s" % (tempStatusPath, path)
    #     ProcessUtilities.executioner(command)
    #
    #     php = PHPManager.getPHPString(phpVersion)
    #
    #     command = "systemctl stop php%s-php-fpm" % (php)
    #     ProcessUtilities.executioner(command)
    #
    #     command = "systemctl start php%s-php-fpm" % (php)
    #     ProcessUtilities.executioner(command)
    #
    #     final_dic = {'status': 1}
    #     final_json = json.dumps(final_dic)
    #     return HttpResponse(final_json)

    # @staticmethod
    # def fetchPHPExtensions(data):
    #
    #     if ApachePHP.objects.all().count() == 0:
    #         phpfilePath = '/usr/local/CyberCP/ApachController/phpApache.xml'
    #
    #         for items in ['54', '55', '56', '70', '71', '72', '73']:
    #             phpvers = ApachePHP(phpVers='php' + items)
    #             phpvers.save()
    #
    #             php = ElementTree.parse(phpfilePath)
    #             phpExtensions = php.findall('extension')
    #
    #             for extension in phpExtensions:
    #                 extensionName = extension.find('extensionName').text % (items)
    #                 extensionDescription = extension.find('extensionDescription').text
    #                 status = int(extension.find('status').text)
    #
    #                 phpExtension = installedPackagesApache(phpVers=phpvers,
    #                                                  extensionName=extensionName,
    #                                                  description=extensionDescription,
    #                                                  status=status)
    #
    #                 phpExtension.save()
    #
    #     phpVers = "php" + PHPManager.getPHPString(data['phpVersion'])
    #
    #     phpVersion = ApachePHP.objects.get(phpVers=phpVers)
    #
    #     records = phpVersion.installedpackagesapache_set.all()
    #
    #     json_data = "["
    #     checker = 0
    #
    #     for items in records:
    #
    #         if items.status == 0:
    #             status = "Not-Installed"
    #         else:
    #             status = "Installed"
    #
    #         dic = {'id': items.id,
    #                'phpVers': items.phpVers.phpVers,
    #                'extensionName': items.extensionName,
    #                'description': items.description,
    #                'status': status
    #                }
    #
    #         if checker == 0:
    #             json_data = json_data + json.dumps(dic)
    #             checker = 1
    #         else:
    #             json_data = json_data + ',' + json.dumps(dic)
    #
    #     json_data = json_data + ']'
    #     final_json = json.dumps({'status': 1, 'error_message': "None", "data": json_data})
    #     return HttpResponse(final_json)
