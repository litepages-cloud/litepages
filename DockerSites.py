import json
import time
import argparse
import threading as multi

from CommonUtils import randomPassword
from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger

class Docker_Sites(multi.Thread):
    Wordpress = 1
    Joomla = 2

    def __init__(self, function_run, data):
        multi.Thread.__init__(self)
        self.function_run = function_run
        self.data = data
        try:
            self.JobID = self.data['JobID']  ##JOBID will be file path where status is being written
        except:
            pass

        self.DockerAppName = f"{self.data['name'].replace(' ', '')}_{self.data['name'].replace(' ', '-')}"

    def run(self):
        try:
            if self.function_run == 'DeployWPContainer':
                self.DeployWPContainer()
            elif self.function_run == 'DeployN8NContainer':
                self.DeployN8NContainer()

        except BaseException as msg:
            LitePagesLogger.writeToFile(str(msg) + ' [Docker_Sites.run]', LitePagesLogger.ERROR, 0)

    def InstallDocker(self):

        lpe = LitePagesExecutioner('')

        lpe.command = 'apt install docker-compose -y'
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode:
            return 1, None
        else:
            return 0, stderr

    @staticmethod
    def SetupProxy(port):
        ConfPath = '/usr/local/lsws/conf/httpd_config.conf'
        data = open(ConfPath, 'r').read()
        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            StringCheck = f"127.0.0.1:{port}"
            if data.find(StringCheck) == -1:
                ProxyContent = f"""
extprocessor docker{port} {{
  type                    proxy
  address                 127.0.0.1:{port}
  maxConns                100
  pcKeepAliveTimeout      60
  initTimeout             60
  retryTimeout            0
  respBuffer              0
}}    
"""

                WriteToFile = open(ConfPath, 'a')
                WriteToFile.write(ProxyContent)
                WriteToFile.close()

    @staticmethod
    def SetupHTAccess(port, htaccess):
        ### Update htaccess

        StringCheck = f'docker{port}'

        try:
            Content = open(htaccess, 'r').read()
        except:
            Content = ''

        print(f'value of content {Content}')

        if Content.find(StringCheck) == -1:
            HTAccessContent = f'''
RewriteEngine On
REWRITERULE ^(.*)$ HTTP://docker{port}/$1 [P]
'''
            WriteToFile = open(htaccess, 'a')
            WriteToFile.write(HTAccessContent)
            WriteToFile.close()

    # Takes
    # ComposePath, MySQLPath, MySQLRootPass, MySQLDBName, MySQLDBNUser, MySQLPassword, CPUsMySQL, MemoryMySQL,
    # port, SitePath, CPUsSite, MemorySite, ComposePath, SiteName
    # finalURL, blogTitle, adminUser, adminPassword, adminEmail, htaccessPath, externalApp

    def DeployWPContainer(self):

        try:
            LitePagesLogger.SendStatus('Checking if Docker is installed..', self.JobID, 1, 0, 0, 0)

            lpe = LitePagesExecutioner('')
            lpe.shell = True


            lpe.command = 'docker --help'
            ReturnCode, stdout, stderr = lpe.Execute()

            print(f'return code of docker install {stdout}')

            if ReturnCode == 0:
                status, message = self.InstallDocker()
                if status == 0:
                    LitePagesLogger.SendStatus(f'Failed to install Docker {message}', self.JobID, 1, 1, 0, 0)
                    return 0, message


            LitePagesLogger.SendStatus('Docker is ready to use..', self.JobID, 1, 0, 0, 10)

            self.data['ServiceName'] = self.data["SiteName"].replace(' ', '-')

            WPSite = f'''
version: '3.8'

services:
  '{self.data['ServiceName']}':
    user: root
    image: cyberpanel/openlitespeed:latest
    ports:
      - "{self.data['port']}:8088"
#      - "443:443"
    environment:
      DB_NAME: "{self.data['MySQLDBName']}"
      DB_USER: "{self.data['MySQLDBNUser']}"
      DB_PASSWORD: "{self.data['MySQLPassword']}"
      WP_ADMIN_EMAIL: "{self.data['adminEmail']}"
      WP_ADMIN_USER: "{self.data['adminUser']}"
      WP_ADMIN_PASSWORD: "{self.data['adminPassword']}"
      WP_URL: {self.data['finalURL']}
      DB_Host: '{self.data['ServiceName']}-db:3306'
      SITE_NAME: '{self.data['SiteName']}'
    volumes:
#      - "/home/docker/{self.data['finalURL']}:/usr/local/lsws/Example/html"
      - "/home/docker/{self.data['finalURL']}/data:/usr/local/lsws/Example/html"
    depends_on:
      - '{self.data['ServiceName']}-db'
    deploy:
      resources:
        limits:
          cpus: '{self.data['CPUsSite']}'  # Use 50% of one CPU core
          memory: {self.data['MemorySite']}M  # Limit memory to 512 megabytes
  '{self.data['ServiceName']}-db':
    image: mariadb
    restart: always
    environment:
#      ALLOW_EMPTY_PASSWORD=no
      MYSQL_DATABASE: '{self.data['MySQLDBName']}'
      MYSQL_USER: '{self.data['MySQLDBNUser']}'
      MYSQL_PASSWORD: '{self.data['MySQLPassword']}'
      MYSQL_ROOT_PASSWORD: '{self.data['MySQLPassword']}'
    volumes:
      - "/home/docker/{self.data['finalURL']}/db:/var/lib/mysql"
    deploy:
      resources:
        limits:
          cpus: '{self.data['CPUsMySQL']}'  # Use 50% of one CPU core
          memory: {self.data['MemoryMySQL']}M  # Limit memory to 512 megabytes            
'''

            ### WriteConfig to compose-file

            lpe.command = f"mkdir -p /home/docker/{self.data['finalURL']}"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(f'Error {str(stderr)}', self.JobID, 1, 1, 0, 0)
                return 0

            TempCompose = f'/home/docker/{self.data["finalURL"]}-docker-compose.yml'

            WriteToFile = open(TempCompose, 'w')
            WriteToFile.write(WPSite)
            WriteToFile.close()

            lpe.command = f"mv {TempCompose} {self.data['ComposePath']}"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(f'Error {str(stderr)}', self.JobID, 1, 1, 0, 0)
                return 0

            lpe.command = f"chmod 600 {self.data['ComposePath']} && chown root:root {self.data['ComposePath']}"
            lpe.Execute()

            ####

            lpe.command = f"docker-compose -f {self.data['ComposePath']} -p '{self.data['SiteName']}' up -d"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(f'Error {str(stderr)}', self.JobID, 1, 1, 0, 0)
                return 0

            LitePagesLogger.SendStatus('Bringing containers online..', self.JobID, 1, 0, 0, 50)

            time.sleep(25)

            ### Set up Proxy

            execPath = "python3 /usr/local/LitePages/DockerSites.py"
            lpe.command = execPath + f" SetupProxy --port {self.data['port']}"
            lpe.Execute()

            ### Set up ht access

            execPath = "python3 /usr/local/LitePages/DockerSites.py"
            lpe.command = execPath + f" SetupHTAccess --port {self.data['port']} --htaccess {self.data['htaccessPath']}"
            lpe.Execute()


            lpe.command = f"chown -R nobody:nogroup /home/docker/{self.data['finalURL']}/data"
            lpe.Execute()

            ### just restart ls for htaccess

            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Compeleted', self.data['JobID'], 1, 1, 1, 100)

        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [Docker_Sites.DeployWPContainer:348]' % (str(msg)), self.JobID, 1, 1,
                                       0, 0)
            return 0

    def DeleteDockerApp(self):
        try:

            lpe = LitePagesExecutioner('')

            lpe.command = f'docker-compose -f {self.data["path"]}/docker-compose.yml down'
            lpe.Execute()

            lpe.command = f'rm -rf /home/docker/{self.data["domain"]}'
            lpe.Execute()

            lpe.command = f'rm -f {self.data["path"]}/public_html/.htaccess'
            lpe.Execute()


            ### forcefully delete containers

            import docker

            # Create a Docker client
            client = docker.from_env()

            # Define the label to filter containers
            label_filter = {'name': self.DockerAppName}

            # List containers matching the label filter
            containers = client.containers.list(filters=label_filter)

            LitePagesLogger.writeToFile(f'List of containers {str(containers)}', LitePagesLogger.ERROR, 0)


            for container in containers:
                lpe.command = f'docker stop {container.short_id}'
                lpe.Execute()

                lpe.command = f'docker rm {container.short_id}'
                lpe.Execute()

            LitePagesExecutioner.RestartServer()

        except BaseException as msg:
            LitePagesLogger.writeToFile(f"Error Delete Docker APP ....... %s" % str(msg), LitePagesLogger.ERROR, 0)
            return 0

    ## This function need site name which was passed while creating the app
    def ListContainers(self):
        try:

            import docker

            # Create a Docker client
            client = docker.from_env()

            # Define the label to filter containers
            label_filter = {'name': self.DockerAppName}

            # List containers matching the label filter
            containers = client.containers.list(filters=label_filter)

            json_data = "["
            checker = 0

            for container in containers:

                dic = {
                    'id': container.short_id,
                    'name': container.name,
                    'status': container.status,
                    'volumes': container.attrs['HostConfig']['Binds'] if 'HostConfig' in container.attrs else [],
                    'logs_50': container.logs(tail=50).decode('utf-8'),
                    'ports': container.attrs['HostConfig']['PortBindings'] if 'HostConfig' in container.attrs else {}
                }

                if checker == 0:
                    json_data = json_data + json.dumps(dic)
                    checker = 1
                else:
                    json_data = json_data + ',' + json.dumps(dic)

            json_data = json_data + ']'

            return 1, json_data


        except BaseException as msg:
            LitePagesLogger.writeToFile(f"ListContainers ....... %s" % str(msg), LitePagesLogger.ERROR, 0)
            return 0

    ### pass container id and number of lines to fetch from logs
    def ContainerLogs(self):
        try:
            import docker
            # Create a Docker client
            client = docker.from_env()

            # Get the container by ID
            container = client.containers.get(self.data['containerID'])

            # Fetch last 'tail' logs for the container
            logs = container.logs(tail=self.data['numberOfLines']).decode('utf-8')

            return 1, logs
        except BaseException as msg:
            LitePagesLogger.writeToFile(f"ContainerLogs ....... %s" % str(msg), LitePagesLogger.ERROR, 0)
            return 0

        ### pass container id and number of lines to fetch from logs

    def ContainerInfo(self):
        try:
            import docker
            # Create a Docker client
            client = docker.from_env()

            # Get the container by ID
            container = client.containers.get(self.data['containerID'])

            # Fetch container stats
            stats = container.stats(stream=False)

            dic = {
                'id': container.short_id,
                'name': container.name,
                'status': container.status,
                'volumes': container.attrs['HostConfig']['Binds'] if 'HostConfig' in container.attrs else [],
                'logs_50': container.logs(tail=50).decode('utf-8'),
                'ports': container.attrs['HostConfig']['PortBindings'] if 'HostConfig' in container.attrs else {},
                'memory': stats['memory_stats']['usage'],
                'cpu' : stats['cpu_stats']['cpu_usage']['total_usage']
            }

            return 1, dic
        except BaseException as msg:
            LitePagesLogger.writeToFile(f"ContainerInfo ....... %s" % str(msg), LitePagesLogger.ERROR, 0)
            return 0

    ##### N8N Container

    def DeployN8NContainer(self):
        try:

            LitePagesLogger.SendStatus('Checking if Docker is installed..', self.JobID, 1, 0, 0, 0)

            lpe = LitePagesExecutioner('')
            lpe.shell = True

            lpe.command = 'docker --help'
            ReturnCode, stdout, stderr = lpe.Execute()

            print(f'return code of docker install {stdout}')

            if ReturnCode == 0:
                status, message = self.InstallDocker()
                if status == 0:
                    LitePagesLogger.SendStatus(f'Failed to install Docker {message}', self.JobID, 1, 1, 0, 0)
                    return 0, message

            LitePagesLogger.SendStatus('Docker is ready to use..', self.JobID, 1, 0, 0, 10)

            self.data['ServiceName'] = self.data["SiteName"].replace(' ', '-')

            WPSite = f'''
version: '3.8'

volumes:
  db_storage:
  n8n_storage:

services:
  '{self.data['ServiceName']}-db':
    image: docker.io/bitnami/postgresql:16
    user: root
    restart: always
    environment:
#      - POSTGRES_USER:root
      - POSTGRESQL_USERNAME={self.data['MySQLDBNUser']}
      - POSTGRESQL_DATABASE={self.data['MySQLDBName']}
      - POSTGRESQL_POSTGRES_PASSWORD={self.data['MySQLPassword']}
      - POSTGRESQL_PASSWORD={self.data['MySQLPassword']}
    volumes:
#      - "/home/docker/{self.data['finalURL']}/db:/var/lib/postgresql/data"
      - "/home/docker/{self.data['finalURL']}/db:/bitnami/postgresql"

  '{self.data['ServiceName']}':
    image: docker.n8n.io/n8nio/n8n
    user: root
    restart: always
    environment:
      - DB_TYPE=postgresdb
      - DB_POSTGRESDB_HOST={self.data['ServiceName']}-db
      - DB_POSTGRESDB_PORT=5432
      - DB_POSTGRESDB_DATABASE={self.data['MySQLDBName']}
      - DB_POSTGRESDB_USER={self.data['MySQLDBNUser']}
      - DB_POSTGRESDB_PASSWORD={self.data['MySQLPassword']}
      - N8N_HOST={self.data['finalURL']}
      - NODE_ENV=production
      - WEBHOOK_URL=https://{self.data['finalURL']}
    ports:
      - "{self.data['port']}:5678"
    links:
      - {self.data['ServiceName']}-db
    volumes:
      - "/home/docker/{self.data['finalURL']}/data:/home/node/.n8n"
    depends_on:
      - '{self.data['ServiceName']}-db'
'''

            ### WriteConfig to compose-file

            lpe.command = f"mkdir -p /home/docker/{self.data['finalURL']}"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(f'Error {str(stderr)}', self.JobID, 1, 1, 0, 0)
                return 0

            TempCompose = f'/home/docker/{self.data["finalURL"]}-docker-compose.yml'

            WriteToFile = open(TempCompose, 'w')
            WriteToFile.write(WPSite)
            WriteToFile.close()

            lpe.command = f"mv {TempCompose} {self.data['ComposePath']}"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(f'Error {str(stderr)}', self.JobID, 1, 1, 0, 0)
                return 0

            lpe.command = f"chmod 600 {self.data['ComposePath']} && chown root:root {self.data['ComposePath']}"
            lpe.Execute()

            ####

            lpe.command = f"docker-compose -f {self.data['ComposePath']} -p '{self.data['SiteName']}' up -d"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode == 0:
                LitePagesLogger.SendStatus(f'Error {str(stderr)}', self.JobID, 1, 1, 0, 0)
                return 0

            LitePagesLogger.SendStatus('Bringing containers online..', self.JobID, 1, 0, 0, 50)

            time.sleep(25)

            ### Set up Proxy

            execPath = "python3 /usr/local/LitePages/DockerSites.py"
            lpe.command = execPath + f" SetupProxy --port {self.data['port']}"
            lpe.Execute()

            ### Set up ht access

            execPath = "python3 /usr/local/LitePages/DockerSites.py"
            lpe.command = execPath + f" SetupHTAccess --port {self.data['port']} --htaccess {self.data['htaccessPath']}"
            lpe.Execute()

            lpe.command = f"chown -R nobody:nogroup /home/docker/{self.data['finalURL']}/data"
            lpe.Execute()

            ### just restart ls for htaccess

            LitePagesExecutioner.RestartServer()

            LitePagesLogger.SendStatus('Compeleted', self.data['JobID'], 1, 1, 1, 100)


        except BaseException as msg:
            LitePagesLogger.SendStatus('%s [Docker_Sites.DeployN8NContainer:348]' % (str(msg)), self.JobID, 1, 1,

                                       0, 0)
            return 0


def Main():
    try:

        parser = argparse.ArgumentParser(description='CyberPanel Docker Sites')
        parser.add_argument('function', help='Specify a function to call!')
        parser.add_argument('--port', help='')
        parser.add_argument('--htaccess', help='')
        parser.add_argument('--externalApp', help='')
        parser.add_argument('--domain', help='')

        args = parser.parse_args()

        if args.function == "SetupProxy":
            Docker_Sites.SetupProxy(args.port)
        elif args.function == 'SetupHTAccess':
            Docker_Sites.SetupHTAccess(args.port, args.htaccess)
        elif args.function == 'DeployWPDocker':
            # Takes
            # ComposePath, MySQLPath, MySQLRootPass, MySQLDBName, MySQLDBNUser, MySQLPassword, CPUsMySQL, MemoryMySQL,
            # port, SitePath, CPUsSite, MemorySite, SiteName
            # finalURL, blogTitle, adminUser, adminPassword, adminEmail, htaccessPath, externalApp
            data = {
                "JobID": '/home/cyberpanel/error-logs.txt',
                "ComposePath": "/home/docker.cyberpanel.net/docker-compose.yml",
                "MySQLPath": '/home/docker.cyberpanel.net/public_html/sqldocker',
                "MySQLRootPass": 'testdbwp12345',
                "MySQLDBName": 'testdbwp',
                "MySQLDBNUser": 'testdbwp',
                "MySQLPassword": 'testdbwp12345',
                "CPUsMySQL": '2',
                "MemoryMySQL": '512',
                "port": '8000',
                "SitePath": '/home/docker.cyberpanel.net/public_html/wpdocker',
                "CPUsSite": '2',
                "MemorySite": '512',
                "SiteName": 'wp docker test',
                "finalURL": 'docker.cyberpanel.net',
                "blogTitle": 'docker site',
                "adminUser": 'testdbwp',
                "adminPassword": 'testdbwp',
                "adminEmail": 'usman@cyberpersons.com',
                "htaccessPath": '/home/docker.cyberpanel.net/public_html/.htaccess',
                "externalApp": 'docke8463',
                "docRoot": "/home/docker.cyberpanel.net"
            }
            ds = Docker_Sites('', data)
            ds.DeployWPContainer()

        elif args.function == 'DeleteDockerApp':
            data = {
                "domain": args.domain}
            ds = Docker_Sites('', data)
            ds.DeleteDockerApp()


    except BaseException as msg:
        print(str(msg))
        pass


if __name__ == "__main__":
    Main()