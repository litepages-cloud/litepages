import json
import os

from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
class MySQLUtils:

    def __init__(self, data=None):
        self.data = data



    @staticmethod
    def CheckMySQLAccessFile():

        lpe = LitePagesExecutioner('')

        if lpe.GetOSVersion == 'Ubuntu 22':
            cnfPath = '/etc/mysql/mariadb.conf.d/cloudpages.cnf'
        else:

            cnfPath = '/etc/mysql/conf.d/cloudpages.cnf'

        MySQLRootPass = json.loads(open('/etc/LitePages/config', 'r').read())['MySQLRootPass']

        if not os.path.exists(cnfPath):
            cnfContent = """[mysqldump]
user=root
password=%s
max_allowed_packet=1024M
[mysql]
user=root
password=%s
[client]
port            = 3306
socket          = /var/run/mysqld/mysqld.sock
user=root
password=%s
""" % (MySQLRootPass, MySQLRootPass, MySQLRootPass)
            writeToFile = open(cnfPath, 'w')
            writeToFile.write(cnfContent)
            writeToFile.close()

            os.chmod(cnfPath, 0o600)

    @staticmethod
    def CreateDatabase(name):
        MySQLUtils.CheckMySQLAccessFile()
        command = "mysql -e 'create database %s'" % (name)
        lpe = LitePagesExecutioner(command)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        return 1, None

    @staticmethod
    def CreateUser(user, password, host='localhost'):
        MySQLUtils.CheckMySQLAccessFile()

        command = "mysql -e 'CREATE USER '%s'@%s IDENTIFIED BY \"%s\"'" % (user, host, password)
        lpe = LitePagesExecutioner(command)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        return 1, None

    @staticmethod
    def GrantPriviliges(database, user, host='localhost'):
        MySQLUtils.CheckMySQLAccessFile()

        command = "mysql -e 'GRANT ALL PRIVILEGES ON '%s'.* TO '%s'@%s'" % (database, user, host)
        lpe = LitePagesExecutioner(command)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        return 1, None

    @staticmethod
    def CreateDatabaseFull(database, user, password, host='localhost'):
        MySQLUtils.CheckMySQLAccessFile()

        status, message = MySQLUtils.CreateDatabase(database)

        if status == 0:
            return 0, message

        status, message = MySQLUtils.CreateUser(user, password, host)

        if status == 0:
            return 0, message

        status, message = MySQLUtils.GrantPriviliges(database, user, host)

        if status == 0:
            return 0, message

        return 1,None

    ## Deletion functions

    @staticmethod
    def DeleteUser(user, host='localhost'):
        MySQLUtils.CheckMySQLAccessFile()
        command = "mysql -e 'DROP USER IF EXISTS \"%s\"@\"%s\"'" % (user, host)
        lpe = LitePagesExecutioner(command)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        return 1, None

    @staticmethod
    def DeleteDatabase(database):
        command = "mysql -e 'DROP DATABASE IF EXISTS %s'" % (database)
        lpe = LitePagesExecutioner(command)
        ReturnCode, stdout, stderr = lpe.Execute()

        if ReturnCode == 0:
            return 0, stderr

        return 1, None

    @staticmethod
    def DeleteDatabaseFull(data):
        ### Deleting users

        for user in data['Users']:
            status, message = MySQLUtils.DeleteUser(user['user'], user['host'])
            if status == 0:
                LitePagesLogger.SendStatus(message, data['JobID'], 1, 0, 0, 0)

        status, message = MySQLUtils.DeleteDatabase(data['database'])

        if status == 0:
            LitePagesLogger.SendStatus(message, data['JobID'], 1, 1, 0, 0)
            return 0, message
        else:
            LitePagesLogger.SendStatus('Completed', data['JobID'], 1, 1, 1, 100)
            return 1, None

    ## Assign Database user

    @staticmethod
    def CreateDatabaseUser(database, user, password, host='localhost'):

        status, message = MySQLUtils.CreateUser(user, password, host)

        if status == 0:
            return 0, message

        status, message = MySQLUtils.GrantPriviliges(database, user, host)

        if status == 0:
            return 0, message

        return 1, None

    ##

    @staticmethod
    def GetTime(seconds):
        time = float(seconds)
        day = time // (24 * 3600)
        time = time % (24 * 3600)
        hour = time // 3600
        time %= 3600
        minutes = time // 60
        time %= 60
        seconds = time
        return ("%d:%d:%d:%d" % (day, hour, minutes, seconds))

    def showStatus(self):
        MySQLUtils.CheckMySQLAccessFile()
        try:

            data = {}

            command = "mysql -e 'SHOW GLOBAL STATUS'"
            lpe = LitePagesExecutioner(command)
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                for items in stdout.split('\n'):
                    cLine = items.split('\t')
                    if cLine[0] == 'Uptime':
                        data['uptime'] = MySQLUtils.GetTime(cLine[1])
                    elif cLine[0] == 'Connections':
                        data['connections'] = cLine[1]
                    elif cLine[0] == 'Slow_queries':
                        data['Slow_queries'] = cLine[1]
            else:
                return 0, stderr

            ## Process list

            lpe.command = "mysql -e 'show processlist'"
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                json_data = "["
                checker = 0

                for items in stdout.split('\n'):
                    cLine = items.split('\t')

                    if len(cLine) == 1:
                        continue
                    elif cLine[1] == 'User':
                        continue

                    if len(str(cLine[3])) == 0:
                        database = 'NULL'
                    else:
                        database = cLine[3]

                    if len(str(cLine[6])) == 0:
                        state = 'NULL'
                    else:
                        state = cLine[6]

                    if len(str(cLine[7])) == '':
                        info = 'NULL'
                    else:
                        info = cLine[7]

                    dic = {
                        'id': cLine[0],
                        'user': cLine[1],
                        'database': database,
                        'command': cLine[4],
                        'time': cLine[5],
                        'state': state,
                        'info': info,
                        'progress': cLine[8],
                    }

                    if checker == 0:
                        json_data = json_data + json.dumps(dic)
                        checker = 1
                    else:
                        json_data = json_data + ',' + json.dumps(dic)

                json_data = json_data + ']'

                data['processes'] = json_data
            else:
                return 0, stderr

            return 1, data

        except BaseException as msg:
            return 0, str(msg)

    def FetchMySQLConfigs(self):
        MySQLUtils.CheckMySQLAccessFile()
        try:

            data = {}

            lpe = LitePagesExecutioner('cat /etc/mysql/my.cnf')
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                data['conf'] = stdout
            else:
                return 0, stderr

            lpe.shell = True
            lpe.command = 'echo $(($(getconf _PHYS_PAGES) * $(getconf PAGE_SIZE) / (1024 * 1024)))'
            ReturnCode, stdout, stderr = lpe.Execute()

            if ReturnCode:
                data['ram'] = float(stdout.rstrip('\n')) / float(1024)
            else:
                return 0, stderr

            return 1, data
        except BaseException as msg:
            return 0, str(msg)

    def ApplyChanges(self, MySQLRootPass):
        try:
            RecommendedConfigurations = self.data['RecommendedConfigurations'].replace('{RootPassword}', MySQLRootPass)

            writeToFile = open('/etc/mysql/my.cnf', 'w')
            writeToFile.write(RecommendedConfigurations)
            writeToFile.close()

            lpe = LitePagesExecutioner('systemctl restart mysql')
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode == 0:
                return 0, stderr

            return 1, None
        except BaseException as msg:
            return 0, str(msg)


if __name__ == "__main__":
    msu = MySQLUtils()
    msu.showStatus()

