import json
import os

from Logger import LitePagesLogger
import threading as multi
import subprocess
import shlex

class LitePagesExecutioner(multi.Thread):
    ENT = 1
    OLS = 0

    """
    command (str) = command to execute
    OutputNeeded (bool) = If output is needed, executioner will return output with command status ex 1, output
    WaitForFinish (int)  = Either run this command with popen (meaning run in background or not)
    """

    def __init__(self, command, capture_output = True, WaitForFinish = 1, shell=False):

        multi.Thread.__init__(self)

        OSVersion = open('/etc/os-release').read()

        if OSVersion.find('Ubuntu 22.04') > -1:
            self.OSVersion = 'Ubuntu 22'
        else:
            self.OSVersion = 'Ubuntu 20'


        self.command = command
        self.capture_output = capture_output
        self.WaitForFinish = WaitForFinish

        ## These two variables will be populated later when command is actually executed

        self.stdout = ''
        self.stderr = ''
        self.ReturnCode = 0
        self.shell = shell

    """
        returns result code, command output (if any) and command error if any
        
        e.g
        
        1, 'success', 'error'
    """
    def Execute(self):
        try:

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile(f'The command to be executed {self.command}.', LitePagesLogger.ERROR, 0)

            if self.shell:
                result = subprocess.run(self.command, capture_output=self.capture_output, text=True, shell=self.shell)
            else:
                result = subprocess.run(shlex.split(self.command), capture_output=self.capture_output, text=True)
    
            self.stdout = result.stdout
            self.stderr = result.stderr
    
            if result.returncode == 0:
                self.ReturnCode = 1
            else:
                self.ReturnCode = 0
    
            ### Output success, error and return code

            if os.path.exists('/var/log/LPDebug'):
                LitePagesLogger.writeToFile('Value of shell %s' % (str(self.shell)), LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(self.stdout, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile(self.stderr, LitePagesLogger.ERROR, 0)
                LitePagesLogger.writeToFile('Return code for command "%s" is %s' % (self.command, str(self.ReturnCode)), LitePagesLogger.ERROR, 0)
    
            return self.ReturnCode, self.stdout, self.stderr
        except BaseException as msg:
            return 0, '', str(msg)
    @staticmethod
    def remove_suffix(input_string, suffix):
        if suffix and input_string.endswith(suffix):
            return input_string[:-len(suffix)]
        return input_string

    @staticmethod
    def DecideWebServer():
        import os
        if os.path.exists('/usr/local/lsws/bin/openlitespeed'):
            return LitePagesExecutioner.OLS
        else:
            return LitePagesExecutioner.ENT


    @staticmethod
    def RestartServer():
        if LitePagesExecutioner.DecideWebServer() == LitePagesExecutioner.OLS:
            command = "systemctl restart lsws"
        else:
            command = "/usr/local/lsws/bin/lswsctrl restart"

        lpe = LitePagesExecutioner(command)
        lpe.Execute()

    @staticmethod
    def RestartServerLSPHP():
        command = 'killall lsphp'
        lpe = LitePagesExecutioner(command)
        lpe.Execute()

    @staticmethod
    def FetchIPAddress():
        config = json.loads(open(LitePagesLogger.LitePagesConfigPathFile, 'r').read())
        return config['ip']

    def GetOSVersion(self):
        return self.OSVersion

