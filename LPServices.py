import os
import random
import socket
import string

from LitePagesExecutioner import LitePagesExecutioner
from Logger import LitePagesLogger
import os
import shutil
import subprocess, shlex

from VirtualHost import VirtualHost

class LPServices:

    def __init__(self, data):
        self.data = data

    def FetchServicesStatus(self):
        try:

            lpe = LitePagesExecutioner('')

            finalDic = {}

            ### LS Status

            finalDic['LSProcesses'] = []
            finalDic['LSStatus'] = 'stopped'
            finalDic['TotalLSMemory'] = 0.0
            finalDic['TotalMySQLMemory'] = 0.0

            lpe.shell = True
            lpe.command = "ps -A | grep litespeed"
            ReturnCode, stdout, stderr = lpe.Execute()

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = s.connect_ex(('127.0.0.1', 80))

            if result == 0:
                finalDic['LSStatus'] = 'running'
                for line in stdout.splitlines():
                    CurrentLine = line.split(' ')
                    CurrentLine = [x for x in CurrentLine if x]
                    finalDic['LSProcesses'].append(CurrentLine[0])

                for process in finalDic['LSProcesses']:
                    lpe.command = "pmap %s | tail -n 1 | awk '/[0-9]K/{print $2}'" % (process)
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode:
                        finalDic['TotalLSMemory'] = finalDic['TotalLSMemory'] + float(stdout.rstrip('K\n'))
                    else:
                        return 0, stderr

                finalDic['TotalLSMemory'] = int(finalDic['TotalLSMemory'] / 1024)

            else:
                finalDic['LSStatus'] = 'stopped'


            ### MySQL Status

            finalDic['MySQLProcesses'] = []
            finalDic['MySQLStatus'] = 'stopped'

            lpe.shell = True
            lpe.command = "ps -A | grep mysql"
            ReturnCode, stdout, stderr = lpe.Execute()

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            result = s.connect_ex(('127.0.0.1', 3306))

            if result == 0:
                finalDic['MySQLStatus'] = 'running'
                for line in stdout.splitlines():
                    CurrentLine = line.split(' ')
                    CurrentLine = [x for x in CurrentLine if x]
                    finalDic['MySQLProcesses'].append(CurrentLine[0])

                finalDic['TotalMySQLMemory'] = 0.0

                for process in finalDic['MySQLProcesses']:
                    lpe.command = "pmap %s | tail -n 1 | awk '/[0-9]K/{print $2}'" % (process)
                    ReturnCode, stdout, stderr = lpe.Execute()
                    if ReturnCode:
                        finalDic['TotalMySQLMemory'] = finalDic['TotalMySQLMemory'] + float(stdout.rstrip('K\n'))
                    else:
                        return 0, stderr
                finalDic['TotalMySQLMemory'] = int(finalDic['TotalMySQLMemory']/1024)
            else:
                finalDic['MySQLStatus'] = 'stopped'

            return 1, finalDic
        except BaseException as msg:
            return 0, str(msg)

    @staticmethod
    def FetchServerDetails(data):
        try:
            RetData = {}
            import shutil
            BytesPerGB = 1024 * 1024 * 1024
            (total, used, free) = shutil.disk_usage("/")
            RetData['TotalDisk'] = "%.2fGB" % (float(total) / BytesPerGB)
            RetData['UsedDisk'] = "%.2fGB" % (float(used)/BytesPerGB)

            RetData['UsedDiskPercent'] = float(100) / (float(total) / BytesPerGB)
            RetData['UsedDiskPercent'] = '%.2f' % (float(RetData['UsedDiskPercent']) * float((float(used)/BytesPerGB)))

            try:
                RetData['version'] = data['version']
            except:
                RetData['version'] = '0.0.1'

            lpe = LitePagesExecutioner('uname -r')
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['Kernel'] = stdout
            else:
                return 0, stderr

            ## Ram

            lpe.shell = True
            lpe.command = "free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 2"
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['TotalRam'] = "%.2fGB" % (float(stdout.rstrip('\n'))/1024)
            else:
                return 0, stderr

            lpe.shell = True
            lpe.command = "free -m  | grep ^Mem | tr -s ' ' | cut -d ' ' -f 3"
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['UsedRam'] = "%.2fGB" % (float(stdout.rstrip('\n')) / 1024)
            else:
                return 0, stderr

            RetData['UsedRamPercent'] = float(100) / (float(RetData['TotalRam'].rstrip('GB')))
            RetData['UsedRamPercent'] = '%.2f' % (float(RetData['UsedRamPercent']) * float((float(RetData['UsedRam'].rstrip('GB')))))

            ## CPU Usage

            lpe.command = "top -n 1 -b | awk '/^%Cpu/{print $2}'"
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['CPUUsage'] = "%s" % (stdout.rstrip('\n'))
            else:
                return 0, stderr

            ### Get CPU Model
            lpe.shell = True
            lpe.command = "lscpu | grep 'Model name' | cut -f 2 -d ':' | awk '{$1=$1}1'"
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['CPUModel'] = stdout.rstrip('\n')
            else:
                return 0, stderr

            ## GET CPU Cores

            lpe.command = "nproc"
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['CPUCores'] = "%s" % (stdout.rstrip('\n'))
            else:
                return 0, stderr


            ## Get Timezone

            lpe.command = "date +'%Z %z'"
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                RetData['Timezone'] = "%s" % (stdout.rstrip('\n'))
            else:
                return 0, stderr

            OSVersion = open('/etc/os-release').read()

            if OSVersion.find('Ubuntu 22.04') > -1:
                RetData['Operating_System'] = 'Ubuntu 22'
            else:
                RetData['Operating_System'] = 'Ubuntu 20'



            return 1, RetData
        except BaseException as msg:
            return 0, str(msg)

    def FetchLiteSpeedDetails(self):
        try:
            RetData = []

            lpe = LitePagesExecutioner("cat /tmp/lshttpd/.rtreport | awk '/REQ_RATE/{print $2,$4, $6,$8}'")
            lpe.shell = True
            ReturnCode, stdout, stderr = lpe.Execute()
            if ReturnCode:
                for vhost in stdout.split('\n'):
                    try:
                        CurrentVhost = vhost.split(' ')
                        LitePagesLogger.writeforCLI(str(CurrentVhost),
                                                    LitePagesLogger.DEBUG, 'FetchLiteSpeedDetails')

                        domain = CurrentVhost[0].lstrip('[').rstrip(':]')

                        if domain == '':
                            domain = 'All'

                        RetData.append({'domain': domain, 'REQ_PROCESSING': CurrentVhost[1].rstrip(','), 'REQ_PER_SEC': CurrentVhost[2].rstrip(','), 'TOT_REQS': CurrentVhost[3].rstrip(',')})
                    except:
                        pass
            else:
                return 0, stderr

            return 1, RetData
        except BaseException as msg:
            return 0, str(msg)

if __name__ == "__main__":
    lps = LPServices({})

